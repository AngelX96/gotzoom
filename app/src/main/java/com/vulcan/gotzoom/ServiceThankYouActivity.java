package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import java.util.HashMap;

public class ServiceThankYouActivity extends AppCompatActivity {

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_thank_you);

        WebView webView = (WebView) findViewById(R.id.webView);

        intent = getIntent();

        String thank_you_String = (String) intent.getSerializableExtra("thank_you_String");

        webView.loadData(thank_you_String,  "text/html", null);
    }

    public void onClickNextButton(View id){
        Intent intent = new Intent(getApplicationContext(), ProfileDataActivity.class);
        intent.putExtra("isSendingMode", true);
        startActivity(intent);
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
