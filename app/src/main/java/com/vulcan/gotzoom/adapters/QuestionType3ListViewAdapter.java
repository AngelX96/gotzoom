package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.vulcan.gotzoom.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 4/19/16.
 */
public class QuestionType3ListViewAdapter extends BaseAdapter {
    static String TITLE_KEY      = "title";
    static String TYPE_KEY       = "type";
    static String VALUE_KEY      = "value";
    static String NEXT_ID_KEY    = "next_id";
    static String CHILD_KEY      = "child";
    static String VALIDATION_KEY = "validation";
    static String CHECK_KEY      = "check_key";
    static String CONTENT_KEY    = "content";

    static String DESCRIPTION_CHILD_TYPE = "description";
    static String DESCRIPTION_WEB_CHILD_TYPE = "description_web";
    static String INPUT_CHILD_TYPE       = "input";
    static String INPUT_EMPTY_CHILD_TYPE = "input_empty";
    static String INPUT_FSA_CHILD_TYPE   = "input_fsa";
    static String EMPTY_CHILD_TYPE       = "empty";

    static String CHECK_TYPE_CELL       = "CheckItemCell";
    static String INPUT_FSA_TYPE_CELL   = "InputFsaIdItemCell";
    static String INPUT_TYPE_CELL       = "InputItemCell";
    static String DESCRIPTION_TYPE_CELL = "DescriptionItemCell";
    static String DESCRIPTION_WEB_TYPE_CELL = "DescriptionWebItemCell";

    static int VALUE_NO  = 0;
    static int VALUE_YES = 1;
    static int VALUE_IDN = 2;

    public final static String TITLE            = "title";
    public final static String COUNT_OF_NESTING = "count_of_nesting";

    public String input_type;

    private ArrayList contentListArray;
    private Context context;
    private int[]                          resources;
    private int[]                          views;
    private int[]                          controls;
    // private Intent intent;
    private View.OnClickListener[]         listners;

    public QuestionType3ListViewAdapter(Context context, ArrayList contentListArray, int[] resources, int[] views, int[] controls,
                                        View.OnClickListener[] listners) {
        super();
        this.contentListArray = contentListArray;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.listners = listners;

        // this.intent = intent;
    }

    @Override
    public int getCount() {
        return contentListArray.size();
    }

    @Override
    public Object getItem(int position) {
        return contentListArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateContentList(ArrayList contentListArray){
        this.contentListArray = contentListArray;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String inflater = Context.LAYOUT_INFLATER_SERVICE;

        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        convertView = li.inflate(this.resources[0], parent, false);


        if(((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)){

            convertView = li.inflate(this.resources[0], parent, false);

        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(INPUT_FSA_TYPE_CELL)){

            convertView = li.inflate(this.resources[1], parent, false);

        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(DESCRIPTION_TYPE_CELL)){

            convertView = li.inflate(this.resources[3], parent, false);

        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(DESCRIPTION_WEB_TYPE_CELL)){

            convertView = li.inflate(this.resources[4], parent, false);

        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(INPUT_TYPE_CELL)){

            convertView = li.inflate(this.resources[2], parent, false);

        }

        convertView.setOnClickListener(this.listners[0]);


//        new int[]{
//                R.layout.item_question_yes_no_idn,
//                R.layout.item_question_fsa,
//                R.layout.item_question_input,
//                R.layout.item_question_description,
//                R.layout.item_question_description_web
//        },
//                new int[]{
//                        R.id.radioButton,
//                        R.id.inputTypeLabel,
//                        R.id.textView,
//                        R.id.editText,
//                        R.id.webView
//                },

        if(((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)){

            ((RadioButton) convertView.findViewById(this.views[0])).setText((String)((HashMap) contentListArray.get(position)).get(TITLE_KEY));

            if((int)((HashMap) contentListArray.get(position)).get(CHECK_KEY) == 0){
                // cell.accessoryType = UITableViewCellAccessoryNone;
                ((RadioButton) convertView.findViewById(this.views[0])).setChecked(false);
            }else{
                // cell.accessoryType = UITableViewCellAccessoryCheckmark;
                ((RadioButton) convertView.findViewById(this.views[0])).setChecked(true);
            }
        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(INPUT_FSA_TYPE_CELL)){
//        Spikael33
//        Deer3373
            // ((InputFsaIdItemTableViewCell*)cell).inputIdTextField.text = @"Spikael33";
            //((InputFsaIdItemTableViewCell*)cell).inputPasswordTextField.text = @"Deer3373";

        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(DESCRIPTION_TYPE_CELL)){

            ((TextView) convertView.findViewById(this.views[2])).setText((String) ((HashMap) contentListArray.get(position)).get(DESCRIPTION_CHILD_TYPE));

        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(DESCRIPTION_WEB_TYPE_CELL)){

            ((WebView) convertView.findViewById(this.views[4])).loadData((String) ((HashMap) contentListArray.get(position)).get(DESCRIPTION_WEB_CHILD_TYPE),"text/html",null);

        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(INPUT_TYPE_CELL)){

            ((TextView) convertView.findViewById(this.views[1])).setText("#");

            if(((HashMap) ((HashMap)contentListArray.get(position)).get(VALIDATION_KEY)).get(TYPE_KEY).equals("money")){
                ((TextView) convertView.findViewById(this.views[1])).setText("$");
                ((EditText) convertView.findViewById(R.id.editText)).setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
                input_type = "decimal";
            }else if(((HashMap) ((HashMap)contentListArray.get(position)).get(VALIDATION_KEY)).get(TYPE_KEY).equals("percent")){
                ((EditText) convertView.findViewById(R.id.editText)).setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
                ((TextView) convertView.findViewById(this.views[1])).setText("%");
                input_type = "decimal";
            }else if(((HashMap) ((HashMap)contentListArray.get(position)).get(VALIDATION_KEY)).get(TYPE_KEY).equals("months")){
                ((EditText) convertView.findViewById(R.id.editText)).setInputType(InputType.TYPE_CLASS_NUMBER);
                ((TextView) convertView.findViewById(this.views[1])).setText("m");
                input_type = "decimal";
            }else if(((HashMap) ((HashMap)contentListArray.get(position)).get(VALIDATION_KEY)).get(TYPE_KEY).equals("number")){
                ((EditText) convertView.findViewById(R.id.editText)).setInputType(InputType.TYPE_CLASS_NUMBER);
                ((TextView) convertView.findViewById(this.views[1])).setText("#");
                input_type = "decimal";
            }else if(((HashMap) ((HashMap)contentListArray.get(position)).get(VALIDATION_KEY)).get(TYPE_KEY).equals("string")){
                ((EditText) convertView.findViewById(R.id.editText)).setInputType(InputType.TYPE_CLASS_TEXT);
                ((TextView) convertView.findViewById(this.views[1])).setText("st");
                input_type = "string";
            }

            ((TextView) convertView.findViewById(R.id.editText)).setText(((HashMap) contentListArray.get(position)).get(CONTENT_KEY).toString());
        }

        convertView.setId(position);
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return convertView;

    }
}
