package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.vulcan.gotzoom.R;
import com.vulcan.gotzoom.ServicesActivity;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by andiil on 9/12/16.
 */
public class ServicesListViewAdapter extends BaseAdapter {

    public final static String ITEM_TYPE = "item_type";

    final String name_green_cell             = "name_green_cell";
    final String name_description_green_cell = "name_description_green_cell";
    final String previouses_cell             = "previouses_cell";
    final String item_cell                   = "item_cell";
    final String button_cell                 = "button_cell";
    final String button_free_cell            = "button_free_cell";

//    enum GZServiceListButtonState {
//        GZServiceListButtonStateSelectEnable,
//        GZServiceListButtonStateSelectDisable,
//        GZServiceListButtonStateContinueEnable,
//        GZServiceListButtonStateUpgradeEnable,
//        GZServiceListButtonStateUpgradeDisable }

    public String input_type;

    private ArrayList contentListArray;
    private ArrayList itemspreviewListArray;
    private ViewGroup parent;
    private Context context;
    private int[] resources;
    private int[] views;
    private int[] controls;
    // private Intent intent;
    private Object[] listeners;

    public ServicesListViewAdapter(Context context, ArrayList contentListArray, int[] resources, int[] views, int[] controls,
                                  Object[] listeners, ViewGroup parent) {

        // this.intent = intent;
    }

    public ServicesListViewAdapter(Context context, ArrayList contentArray, int[] resources, int[] views, int[] controls, View.OnClickListener[] onClickListeners, ViewGroup parent) {
        super();
        this.contentListArray = contentArray;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.listeners = onClickListeners;

        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        itemspreviewListArray = new ArrayList();

        for (int i=0; i<this.resources.length ; i++){
            itemspreviewListArray.add(li.inflate(this.resources[i], parent, false));
        }
    }

    @Override
    public int getCount() {
        return contentListArray.size();
    }

    @Override
    public Object getItem(int position) {
        return contentListArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String type = ((HashMap) contentListArray.get(position)).get(ITEM_TYPE).toString();
        Integer error_i = 1;

        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        if(type.equals(button_free_cell)){

            convertView = li.inflate(this.resources[0], parent, false);

        } else if(type.equals(button_cell) ){

            convertView = li.inflate(this.resources[1], parent, false);

        } else if(type.equals(item_cell)){

            convertView = li.inflate(this.resources[2], parent, false);

        } else if(type.equals(previouses_cell)){

            convertView = li.inflate(this.resources[3], parent, false);

        } else if(type.equals(name_green_cell)){

            convertView = li.inflate(this.resources[4], parent, false);

        } else if(type.equals(name_description_green_cell)){

            convertView = li.inflate(this.resources[5], parent, false);
        }

//        R.layout.item_services_button_free,
//                R.layout.item_services_list_button,
//                R.layout.item_services_list_item,
//                R.layout.item_services_previus,
//                R.layout.item_services_name_green,
//                R.layout.item_services_name_description_green

//        R.id.name,
//        R.id.description,
//        R.id.text,
//        R.id.price,
//        R.id.price_month,
//        R.id.button

        if (convertView != null) {
            if(type.equals(name_green_cell)){

                ((TextView) convertView.findViewById(this.views[0])).setText(((HashMap) contentListArray.get(position)).get("name").toString());

            } else if(type.equals(name_description_green_cell) ){

                ((TextView) convertView.findViewById(this.views[0])).setText(((HashMap) contentListArray.get(position)).get("name").toString());
                ((TextView) convertView.findViewById(this.views[1])).setText(((HashMap) contentListArray.get(position)).get("name_description").toString());

            } else if(type.equals(previouses_cell)){

                ((TextView) convertView.findViewById(this.views[0])).setText(((HashMap) contentListArray.get(position)).get("name").toString());

            } else if(type.equals(item_cell)){

                ((TextView) convertView.findViewById(this.views[2])).setText(((HashMap) contentListArray.get(position)).get("text").toString());

            } else if(type.equals(button_cell)){
                // convertView.findViewById(this.views[5]).setOnClickListener((View.OnClickListener) this.listeners[0]);

                ServicesActivity.GZServiceListButtonState status = (ServicesActivity.GZServiceListButtonState)((HashMap) contentListArray.get(position)).get("status");

                switch (status) {
                    case GZServiceListButtonStateSelectEnable:
                        ((Button) convertView.findViewById(this.views[5])).setText("Select");
                        ((Button) convertView.findViewById(this.views[5])).setEnabled(true);
                        convertView.findViewById(this.views[5]).setBackgroundResource(R.drawable.next_button_shape);

                        convertView.findViewById(this.views[5]).setOnClickListener((View.OnClickListener) this.listeners[0]);

                        break;
                    case GZServiceListButtonStateSelectDisable:
                        ((Button) convertView.findViewById(this.views[5])).setText("Select");
                        ((Button) convertView.findViewById(this.views[5])).setEnabled(false);
                        convertView.findViewById(this.views[5]).setBackgroundResource(R.drawable.button_disable_shape);

                        break;
                    case GZServiceListButtonStateContinueEnable:
                        ((Button) convertView.findViewById(this.views[5])).setText("Continue");
                        ((Button) convertView.findViewById(this.views[5])).setEnabled(true);
                        convertView.findViewById(this.views[5]).setBackgroundResource(R.drawable.next_button_shape);

                        convertView.findViewById(this.views[5]).setOnClickListener((View.OnClickListener) this.listeners[1]);

                        break;
                    case GZServiceListButtonStateUpgradeEnable:
                        ((Button) convertView.findViewById(this.views[5])).setText("Upgrade");
                        ((Button) convertView.findViewById(this.views[5])).setEnabled(true);
                        convertView.findViewById(this.views[5]).setBackgroundResource(R.drawable.next_button_shape);

                        convertView.findViewById(this.views[5]).setOnClickListener((View.OnClickListener) this.listeners[0]);

                        break;
                    case GZServiceListButtonStateUpgradeDisable:
                        ((Button) convertView.findViewById(this.views[5])).setText("Upgrade");
                        ((Button) convertView.findViewById(this.views[5])).setEnabled(false);
                        convertView.findViewById(this.views[5]).setBackgroundResource(R.drawable.button_disable_shape);

                        break;
                    default:
                        break;
                }

                ((TextView) convertView.findViewById(this.views[3])).setText(String.format("$%(.0f", ((HashMap) contentListArray.get(position)).get("price")));
                ((TextView) convertView.findViewById(this.views[4])).setText(String.format("$%(.2f", ((HashMap) contentListArray.get(position)).get("price_month")));

            } else if(type.equals(button_free_cell)) {
                ((Button) convertView.findViewById(this.views[5])).setEnabled(false);
                convertView.findViewById(this.views[5]).setBackgroundResource(R.drawable.button_disable_shape);
                convertView.findViewById(this.views[5]).setOnClickListener((View.OnClickListener) this.listeners[0]);

            }
        }

        convertView.setId(position);

        //cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return convertView;

    }
}
