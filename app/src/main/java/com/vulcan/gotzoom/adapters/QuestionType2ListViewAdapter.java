package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by andiil on 4/19/16.
 */
public class QuestionType2ListViewAdapter extends BaseAdapter {

    public final static String TITLE            = "title";
    public final static String COUNT_OF_NESTING = "count_of_nesting";

    private String[] states_full;
    private String[] states_short;
    private String[] data;
    private String state;
    private Context context;
    private int[]                          resources;
    private int[]                          views;
    private int[]                          controls;
    // private Intent intent;
    private View.OnClickListener[]         listners;

    public QuestionType2ListViewAdapter(Context context, String[] states_full, String[] states_short, String state, int[] resources, int[] views, int[] controls,
                                        View.OnClickListener[] listners) {
        super();
        this.states_full = states_full;
        this.states_short = states_short;
        this.state = state;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.listners = listners;

        // this.intent = intent;
    }

    @Override
    public int getCount() {
        return states_full.length;
    }

    @Override
    public Object getItem(int position) {
        return states_short[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateState(String state){
        this.state = state;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String inflater = Context.LAYOUT_INFLATER_SERVICE;

        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        convertView = li.inflate(this.resources[0], parent, false);
        convertView.setId(position);

        RadioButton mCheckBox;

        mCheckBox = (RadioButton) convertView.findViewById(this.views[0]);
        mCheckBox.setText(states_full[position]);

        if (states_short[position].equals(state)){
            mCheckBox.setChecked(true);
        }

        convertView.findViewById(this.controls[0]).setOnClickListener(this.listners[0]);

        return convertView;
    }
}
