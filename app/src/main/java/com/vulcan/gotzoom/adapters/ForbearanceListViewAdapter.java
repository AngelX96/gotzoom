package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.vulcan.gotzoom.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 9/12/16.
 */
public class ForbearanceListViewAdapter extends BaseAdapter {

    private ViewGroup parent;
    private Context context;
    private int[] resources;
    private int[] views;
    private int[] controls;

    // private Intent intent;
    private Object[] listeners;

    private int state;

    private HashMap download_status_Dictionary;

    private ArrayList state_1_Array;
    private ArrayList state_2_Array;
    private ArrayList state_3_Array;

    final String FORBEARANCE_BIG_DESCRIPTION_CELL_TYPE   = "forbearance_big_description";
    final String FORBEARANCE_SMALL_DESCRIPTION_CELL_TYPE = "forbearance_small_description";
    final String FORBEARANCE_ITEM_CELL_TYPE              = "forbearance_item";
    final String FORBEARANCE_BUTTON_ITEM_CELL_TYPE       = "forbearance_button_item";
    final String FORBEARANCE_DOWNLOAD_CELL_TYPE          = "forbearance_download";

    public ForbearanceListViewAdapter(Context context, int state, HashMap download_status_Dictionary, ArrayList state_1_Array, ArrayList state_2_Array, ArrayList state_3_Array, int[] resources, int[] views, int[] controls,
                                   Object[] listeners, ViewGroup parent) {

        super();

        this.download_status_Dictionary = download_status_Dictionary;
        this.state         = state;
        this.state_1_Array = state_1_Array;
        this.state_2_Array = state_2_Array;
        this.state_3_Array = state_3_Array;
        this.context       = context;
        this.resources     = resources;
        this.views         = views;
        this.controls      = controls;
        this.listeners     = listeners;
    }

    public int getSteate(){
        return this.state;
    }

    public void setState(int state){

        this.state = state > 3?3:state;
    }

    public void setDownloadStatusDictionary(HashMap download_status_Dictionary){

        this.download_status_Dictionary = download_status_Dictionary;
    }

    @Override
    public int getCount() {
        int size = 0;
        switch (state){
            case 1:
                size = state_1_Array.size();
                break;
            case 2:
                size = state_2_Array.size();
                break;
            case 3:
                size = state_3_Array.size();
                break;
            default:
                break;
        }
        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        String type = "";

        switch (state) {
            case 1:
                type = ((HashMap) state_1_Array.get(position)).get("type").toString();
                break;
            case 2:
                type = ((HashMap) state_2_Array.get(position)).get("type").toString();
                break;
            case 3:
                type = ((HashMap) state_3_Array.get(position)).get("type").toString();
                break;

            default:
                break;
        }

        //        R.layout.item_forbearance_big_description,
        //        R.layout.item_forbearance_button,
        //        R.layout.item_forbearance_download,
        //        R.layout.item_forbearance_item,
        //        R.layout.item_forbearance_small_description,
        //        R.layout.item_forbearance_open
        //
        //

//                R.id.text,
//                R.id.openButton,
//                R.id.downloadButton,
//                R.id.open_container,
//                R.id.download_container

        if (type.equals(FORBEARANCE_BIG_DESCRIPTION_CELL_TYPE)) {

            convertView = li.inflate(this.resources[0], parent, false);
        } else if (type.equals(FORBEARANCE_SMALL_DESCRIPTION_CELL_TYPE)) {

            convertView = li.inflate(this.resources[4], parent, false);
        } else if (type.equals(FORBEARANCE_ITEM_CELL_TYPE)) {

            convertView = li.inflate(this.resources[3], parent, false);
        } else if (type.equals(FORBEARANCE_BUTTON_ITEM_CELL_TYPE)) {

            convertView = li.inflate(this.resources[1], parent, false);
        } else if (type.equals(FORBEARANCE_DOWNLOAD_CELL_TYPE)) {
            int download_status = 0;
            switch (state) {
                case 3:
                    download_status = (int) download_status_Dictionary.get("forbearance-form");
                    switch (download_status){
                        case 0:
                            convertView = li.inflate(this.resources[2], parent, false);
                            break;
                        case 1:
                            break;
                        case 2:
                            convertView = li.inflate(this.resources[5], parent, false);
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }

        }

        String content = "";

        if (type.equals(FORBEARANCE_BIG_DESCRIPTION_CELL_TYPE)      ||
                type.equals(FORBEARANCE_SMALL_DESCRIPTION_CELL_TYPE)||
                type.equals(FORBEARANCE_ITEM_CELL_TYPE)             ||
                type.equals(FORBEARANCE_BUTTON_ITEM_CELL_TYPE)      ||
                type.equals(FORBEARANCE_DOWNLOAD_CELL_TYPE)) {

            switch (state) {
                case 1:
                    content = ((HashMap) state_1_Array.get(position)).get("content").toString();
                    break;
                case 2:
                    content = ((HashMap) state_2_Array.get(position)).get("content").toString();
                    break;
                case 3:
                    content = ((HashMap) state_3_Array.get(position)).get("content").toString();
                    break;

                default:
                    break;
            }

            ((TextView) convertView.findViewById(this.views[0])).setText(content);
        }

        convertView.setId(position);

        return convertView;
    }
}
