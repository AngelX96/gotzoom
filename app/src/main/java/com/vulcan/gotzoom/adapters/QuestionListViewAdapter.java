package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.vulcan.gotzoom.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 4/19/16.
 */
public class QuestionListViewAdapter extends BaseAdapter {
    static String TITLE_KEY = "title";
    static String TYPE_KEY = "type";
    static String VALUE_KEY = "value";
    static String NEXT_ID_KEY = "next_id";
    static String CHILD_KEY = "child";
    static String VALIDATION_KEY = "validation";
    static String CHECK_KEY = "check_key";
    static String CONTENT_KEY = "content";

    static String DESCRIPTION_CHILD_TYPE = "description";
    static String INPUT_CHILD_TYPE = "input";
    static String INPUT_EMPTY_CHILD_TYPE = "input_empty";
    static String INPUT_FSA_CHILD_TYPE = "input_fsa";
    static String EMPTY_CHILD_TYPE = "empty";

    static String CHECK_TYPE_CELL = "CheckItemCell";
    static String INPUT_FSA_TYPE_CELL = "InputFsaIdItemCell";
    static String INPUT_TYPE_CELL = "InputItemCell";
    static String DESCRIPTION_TYPE_CELL = "DescriptionItemCell";

    static int VALUE_NO = 0;
    static int VALUE_YES = 1;
    static int VALUE_IDN = 2;

    public final static String TITLE = "title";
    public final static String COUNT_OF_NESTING = "count_of_nesting";

    public String input_type;

    private ArrayList contentListArray;
    private Context context;
    private int[] resources;
    private int[] views;
    private int[] controls;
    // private Intent intent;
    private View.OnClickListener[] listners;

    public QuestionListViewAdapter(Context context, ArrayList contentListArray, int[] resources, int[] views, int[] controls,
                                   View.OnClickListener[] listners) {
        super();
        this.contentListArray = contentListArray;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.listners = listners;

        // this.intent = intent;
    }

    @Override
    public int getCount() {
        return contentListArray.size();
    }

    @Override
    public Object getItem(int position) {
        return contentListArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateContentList(ArrayList contentListArray) {
        this.contentListArray = contentListArray;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String inflater = Context.LAYOUT_INFLATER_SERVICE;

        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        convertView = li.inflate(this.resources[0], parent, false);

        convertView.setOnClickListener(this.listners[0]);


        HashMap response_dict  = new HashMap();
        String input_type      = "";
        String input_text      = "";
        String answer_text     = "";
        String content_text    = "";
        String validation_type = "";

        if (convertView != null) {

            if (!((HashMap) contentListArray.get(position)).get("answer").equals("")) {
                ((TextView) convertView.findViewById(this.views[0])).setText(((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("title").toString());


                //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

                switch (((Double) ((HashMap) ((HashMap) ((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("response_dict")).get("question")).get("q_type")).intValue()) {
                    case 1:
                        ArrayList answer_names_arr = (ArrayList) ((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("answer_names");
                        String answer_names = "";

                        for (int i = 0; i < answer_names_arr.size(); i++) {
                            if (i < answer_names_arr.size() - 1) {
                                answer_names += answer_names_arr.get(i) + "\n";
                            } else {
                                answer_names += answer_names_arr.get(i);
                            }
                        }

                        ((TextView) convertView.findViewById(this.views[1])).setText(answer_names);
                        break;
                    case 2:
                        ((TextView) convertView.findViewById(this.views[1])).setText(((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("answer_name").toString());
                        break;
                    case 3:
                    case 6:
                    case 7:
                        answer_text = ((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("answer").toString();


                        if (answer_text.equals("no")) {
                            input_text = "No";
                        } else if (answer_text.equals("yes")) {
                            input_text = "Yes";
                        } else if (answer_text.equals("idn")) {
                            input_text = "I don't now";
                        }

                        ((TextView) convertView.findViewById(this.views[1])).setText(input_text);
                        break;
                    case 5:
                        response_dict = (HashMap) ((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("response_dict");

                        answer_text = ((HashMap) ((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("answer")).get("value").toString();

                        content_text = ((HashMap) ((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("answer")).get("content").toString();

                        if (answer_text.equals("no")) {
                            input_text = "No";
                        } else if (answer_text.equals("yes")) {
                            input_text = "Yes";
                        } else if (answer_text.equals("idn")) {
                            input_text = "I don't now";
                        }

                        HashMap m = (HashMap) ((HashMap) ((HashMap) response_dict.get("question")).get("check_type")).get(answer_text);

                        validation_type = ((HashMap) ((HashMap) ((HashMap) response_dict.get("question")).get("check_type")).get(answer_text)).get("type").toString();
                        //validation_type = ((HashMap) ((HashMap) ((HashMap) ((HashMap) response_dict.get("question")).get("check_type")).get(answer_text)).get("validation")).get("type").toString();

                        if (validation_type.equals("money")) {
                            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("%s, $%s", input_text, content_text));
                        } else if (validation_type.equals("percent")) {
                            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("%s, %s%s", input_text, content_text, "%"));
                        } else if (validation_type.equals("months")) {
                            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("%s, %s months", input_text, content_text));
                        } else {
                            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("%s, %s", input_text, content_text));
                        }
                        break;
                    case 4:
                        response_dict = (HashMap) ((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("response_dict");

                        input_text = ((HashMap) ((HashMap) contentListArray.get(position)).get("answer")).get("answer").toString();

                        validation_type = ((HashMap) ((HashMap) response_dict.get("question")).get("validation")).get("type").toString();

                        if (validation_type.equals("money")) {
                            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("$%s", input_text));
                        } else if (validation_type.equals("percent")) {
                            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("%s%s", input_text, "%"));
                        } else if (validation_type.equals("months")) {
                            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("%s months", input_text));
                        } else {
                            //((TextView) convertView.findViewById(this.views[1])).setText(String.format("%s, %s", input_text));
                            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("%s", input_text));
                        }

                        break;

                    default:
                        break;
                }
            }
        }


//        if(((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)){
//
//            ((RadioButton) convertView.findViewById(this.views[0])).setText((String)((HashMap) contentListArray.get(position)).get(TITLE_KEY));
//
//            if(((HashMap) contentListArray.get(position)).get(CHECK_KEY) == 0){
//                // cell.accessoryType = UITableViewCellAccessoryNone;
//                ((RadioButton) convertView.findViewById(this.views[0])).setChecked(false);
//            }else{
//                // cell.accessoryType = UITableViewCellAccessoryCheckmark;
//                ((RadioButton) convertView.findViewById(this.views[0])).setChecked(true);
//            }
//        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(INPUT_FSA_TYPE_CELL)){
////        Spikael33
////        Deer3373
//            // ((InputFsaIdItemTableViewCell*)cell).inputIdTextField.text = @"Spikael33";
//            //((InputFsaIdItemTableViewCell*)cell).inputPasswordTextField.text = @"Deer3373";
//
//        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(DESCRIPTION_TYPE_CELL)){
//
//            ((TextView) convertView.findViewById(this.views[2])).setText((String) ((HashMap) contentListArray.get(position)).get(DESCRIPTION_CHILD_TYPE));
//
//        }else if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(INPUT_TYPE_CELL)){
//
//            ((TextView) convertView.findViewById(this.views[1])).setText("#");
//
//
//
//            ((TextView) convertView.findViewById(R.id.editText)).setText(((HashMap) contentListArray.get(position)).get(CONTENT_KEY).toString());
//        }

        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        convertView.setOnClickListener(this.listners[0]);
        convertView.setId(position);
        return convertView;

//
//        QuestionListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuestionListTableViewCell"];
//
//        if (cell == nil) {
//            cell = [[QuestionListTableViewCell alloc]
//            initWithStyle:UITableViewCellStyleDefault
//            reuseIdentifier:@"QuestionListTableViewCell"];
//        }
//

//        // cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//
//        return cell;

    }
}
