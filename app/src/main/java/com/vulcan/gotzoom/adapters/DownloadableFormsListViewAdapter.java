package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 9/12/16.
 */
public class DownloadableFormsListViewAdapter  extends BaseAdapter {

    private ArrayList itemspreviewListArray;
    private ViewGroup parent;
    private Context context;
    private int[] resources;
    private int[] views;
    private int[] controls;
    // private Intent intent;
    private ArrayList features_Array;
    private HashMap download_status_Dictionary;
    private ArrayList contentArray;

    final String title_cell       = "title_cell";
    final String description_cell = "description_cell";
    final String button_cell      = "button_cell";

    public DownloadableFormsListViewAdapter(Context context, ArrayList contentArray, HashMap download_status_Dictionary, int[] resources, int[] views, int[] controls, ViewGroup parent) {
        super();

        this.contentArray = contentArray;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.download_status_Dictionary = download_status_Dictionary;
    }

    public void setDownloadStatusDictionary(HashMap download_status_Dictionary){

        this.download_status_Dictionary = download_status_Dictionary;
    }

    @Override
    public int getCount() {
        return contentArray.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        String type = "";

        type = (String) ((HashMap) contentArray.get(position)).get("type");

//        new int[]{
//                R.layout.item_form_downloadable_button_download,
//                R.layout.item_form_downloadable_button_open,
//                R.layout.item_form_downloadable_description,
//                R.layout.item_form_downloadable_title
//        },
//                new int[]{
//                        R.id.openButton,
//                        R.id.downloadButton,
//                        R.id.text
//                },

        if (type.equals(title_cell)) {

            convertView = li.inflate(this.resources[3], parent, false);
        } else if (type.equals(description_cell)) {

            convertView = li.inflate(this.resources[2], parent, false);
        } else if (type.equals(button_cell)) {
            int download_status = 0;

            download_status = (int) download_status_Dictionary.get(((HashMap)contentArray.get(position)).get("slug"));
            switch (download_status) {
                case 0:
                    convertView = li.inflate(this.resources[0], parent, false);
                    break;
                case 1:
                    break;
                case 2:
                    convertView = li.inflate(this.resources[1], parent, false);
                    break;
                default:
                    break;
            }
        }

        String content = "";

        if (type.equals(title_cell)      ||
                type.equals(description_cell)) {

            content = ((HashMap) contentArray.get(position)).get("content").toString();

            ((TextView) convertView.findViewById(this.views[2])).setText(content);
        }

        convertView.setId(position);

        return convertView;
    }
}
