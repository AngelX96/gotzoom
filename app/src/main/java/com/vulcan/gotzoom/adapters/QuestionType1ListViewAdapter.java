package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by andiil on 4/19/16.
 */
public class QuestionType1ListViewAdapter extends BaseAdapter {

    public final static String TITLE            = "title";
    public final static String COUNT_OF_NESTING = "count_of_nesting";

    private ArrayList data;
    ArrayList<Boolean> checkArray;
    private Context context;
    private int[]                          resources;
    private int[]                          views;
    private int[]                          controls;
    // private Intent intent;
    private View.OnClickListener[]         listners;

    public QuestionType1ListViewAdapter(Context context, ArrayList data, ArrayList<Boolean> checkArray, int[] resources, int[] views, int[] controls,
                                        View.OnClickListener[] listners) {
        super();
        this.data = data;
        this.checkArray = checkArray;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.listners = listners;

        // this.intent = intent;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return checkArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Integer.parseInt((String) ((HashMap) data.get(position)).keySet().toArray()[0]);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // convertView =
        // inflater.inflate(R.layout.three_line_list_item,
        // container, false);
        String inflater = Context.LAYOUT_INFLATER_SERVICE;

        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        convertView = li.inflate(this.resources[0], parent, false);
        convertView.setId(position);

        Map<String, String> map = (Map<String, String>) this.data.get(position);

        CheckBox mCheckBox;

        mCheckBox = (CheckBox) convertView.findViewById(this.views[0]);
        mCheckBox.setText((String)((HashMap) data.get(position)).get(((HashMap) data.get(position)).keySet().toArray()[0]));
        mCheckBox.setChecked(checkArray.get(position));

        convertView.findViewById(this.controls[0]).setOnClickListener(this.listners[0]);

        return convertView;
    }
}
