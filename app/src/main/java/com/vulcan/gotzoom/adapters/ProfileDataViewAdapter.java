package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by andiil on 4/19/16.
 */
public class ProfileDataViewAdapter extends BaseAdapter {

    static String TYPE_ROW_KEY   = "type_row";
    static String ID_KEY         = "id";
    static String TYPE_INPUT_KEY = "type_input";
    static String NAME_KEY       = "name";
    static String REQUIRED_KEY   = "required";
    static String ITEMS_KEY      = "items";
    static String CONTENT_KEY    = "content";
    static String ERROR_KEY      = "error";
    static String ERROR_TEXT_KEY = "error_text";

    static String INPUT_TYPE       = "input";
    static String INPUT_NUM_TYPE   = "input_num";
    static String INPUT_PHONE_TYPE = "input_phone";

    static String INPUT_EMAIL_TYPE = "input_email";

    static String INPUT_STATE_TYPE          = "input_state";
    static String INPUT_DATE_TYPE           = "input_date";
    static String INPUT_DISABLE_PROGRAMS    = "input_disable_programs";
    static String INPUT_PROGRAMS_TYPE       = "input_programs";
    static String INPUT_STATUS_TYPE         = "input_status";
    static String INPUT_DESCRIPTION         = "description";
    static String INPUT_DESCRIPTION_SECTION = "description_section";
    static String INPUT_SSN_TYPE            = "input_ssn";

    static String SERVICE_WILL_UPGRADE_TYPE = "service_will_upgrade_type";
    static String SERVICE_DID_UPGRADE_TYPE  = "service_did_upgrade_type";

    static String ROW     = "row";
    static String SECTION = "section";
    
    public final static String TITLE = "title";
    public final static String COUNT_OF_NESTING = "count_of_nesting";

    public String input_type;

    private ArrayList contentListArray;
    private ArrayList itemspreviewListArray;
    private ViewGroup parent;
    private Context context;
    private int[] resources;
    private int[] views;
    private int[] controls;
    // private Intent intent;
    private Object[] listeners;

    public ProfileDataViewAdapter(Context context, ArrayList contentListArray, int[] resources, int[] views, int[] controls,
                                  Object[] listeners, ViewGroup parent) {
        super();
        this.contentListArray = contentListArray;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.listeners = listeners;
        this.parent = parent;

        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        itemspreviewListArray = new ArrayList();

        for (int i=0; i<this.resources.length ; i++){
            itemspreviewListArray.add(li.inflate(this.resources[i], parent, false));
        }

        // this.intent = intent;
    }

    @Override
    public int getCount() {
        return contentListArray.size();
    }

    @Override
    public Object getItem(int position) {
        return contentListArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateContentList(ArrayList contentListArray) {
        this.contentListArray = contentListArray;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String type = ((HashMap) contentListArray.get(position)).get(TYPE_INPUT_KEY).toString();
        Integer error_i = 1;

        if(!type.equals(INPUT_DESCRIPTION) && !type.equals(INPUT_DESCRIPTION_SECTION)) {
            Object o = ((HashMap) contentListArray.get(position)).get(ERROR_KEY);
            if((int) o == 0){
                error_i = 0;
            }else {
                error_i = (int) o;
            }
        }

        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        convertView = li.inflate(this.resources[0], parent, false);

        if(type.equals(INPUT_TYPE)    ||
        type.equals(INPUT_NUM_TYPE)   ||
        type.equals(INPUT_PHONE_TYPE) ||
        type.equals(INPUT_EMAIL_TYPE) ||
        type.equals(INPUT_SSN_TYPE)){

            convertView = li.inflate(this.resources[0], parent, false);

        }else if(type.equals(INPUT_STATE_TYPE)      ||
                type.equals(INPUT_DATE_TYPE)        ||
                type.equals(INPUT_PROGRAMS_TYPE)    ||
                type.equals(INPUT_DISABLE_PROGRAMS) ||
                type.equals(INPUT_STATUS_TYPE) ){

            convertView = li.inflate(this.resources[1], parent, false);

        }else if(type.equals(INPUT_DESCRIPTION)||
                type.equals(INPUT_DESCRIPTION_SECTION) ){

            convertView = li.inflate(this.resources[2], parent, false);

        } else if(type.equals(SERVICE_WILL_UPGRADE_TYPE)){

            convertView = li.inflate(this.resources[3], parent, false);

        } else if(type.equals(SERVICE_DID_UPGRADE_TYPE)){

            convertView = li.inflate(this.resources[4], parent, false);
        }

//        new int[]{
//                R.layout.item_profile_data_input,
//                R.layout.item_profile_data_select,
//                R.layout.item_profile_data_description,
//                R.layout.item_profile_data_will_upgrade,
//                R.layout.item_profile_data_did_upgrade
//        },
//                new int[]{
//                        R.id.nameTextView,
//                        R.id.editText,
//                        R.id.labelText,
//                        R.id.addButton,
//                        R.id.deleteButton,
//                        R.id.errorLabel,
//                        R.id.description_Label,
//                        R.id.name,
//                        R.id.payment,
//                        R.id.date
//                },

        if (convertView != null) {

            ///////////////////////
            if (type.equals(SERVICE_WILL_UPGRADE_TYPE)||
                    type.equals(SERVICE_DID_UPGRADE_TYPE)) {

                    ((TextView) convertView.findViewById(this.views[7])).setText(((HashMap)((HashMap) contentListArray.get(position)).get("Services")).get("name").toString());
                    ((TextView) convertView.findViewById(this.views[8])).setText(((HashMap)((HashMap) contentListArray.get(position)).get("Payments")).get("created").toString());
                    ((TextView) convertView.findViewById(this.views[9])).setText(String.format("$ %s", ((HashMap)((HashMap) contentListArray.get(position)).get("Services")).get("price").toString()));
            }

            ///////////////////////
            if(!type.equals(INPUT_DESCRIPTION) &&
                    !type.equals(INPUT_DESCRIPTION_SECTION) &&
                    !type.equals(SERVICE_WILL_UPGRADE_TYPE) &&
                    !type.equals(SERVICE_DID_UPGRADE_TYPE)) {
                if (error_i == 1) {
                    convertView.findViewById(this.views[5]).setVisibility(View.VISIBLE);
                    ((TextView) convertView.findViewById(this.views[5])).setText(((HashMap) contentListArray.get(position)).get(ERROR_TEXT_KEY).toString());
                } else {
                    convertView.findViewById(this.views[5]).setVisibility(View.INVISIBLE);
                }
            }

            if(type.equals(INPUT_TYPE) ||
            type.equals(INPUT_NUM_TYPE)  ||
            type.equals(INPUT_PHONE_TYPE) ||
            type.equals(INPUT_EMAIL_TYPE) ||
            type.equals(INPUT_SSN_TYPE) ){

                //convertView.findViewById(this.views[1]).setOnClickListener(this.listeners[2]);.setOnFocusChangeListener(
                //convertView.findViewById(this.views[1]).setOnTouchListener((View.OnTouchListener) this.listeners[2]);
                convertView.findViewById(this.views[1]).setOnFocusChangeListener((View.OnFocusChangeListener) this.listeners[2]);

                String content = ((HashMap) contentListArray.get(position)).get(CONTENT_KEY).toString();

                ((TextView) convertView.findViewById(this.views[0])).setText(((HashMap) contentListArray.get(position)).get(NAME_KEY).toString());
                ((TextView) convertView.findViewById(this.views[1])).setText(content);

                if(type.equals(INPUT_TYPE)){

                } else if (type.equals(INPUT_NUM_TYPE)){
                    ((EditText) convertView.findViewById(this.views[1])).setInputType(InputType.TYPE_CLASS_NUMBER);
                }else if (type.equals(INPUT_PHONE_TYPE)){
                    ((EditText) convertView.findViewById(this.views[1])).setInputType(InputType.TYPE_CLASS_PHONE);
                }else if (type.equals(INPUT_EMAIL_TYPE)){
                    ((EditText) convertView.findViewById(this.views[1])).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                }else if (type.equals(INPUT_SSN_TYPE)){
                    ((EditText) convertView.findViewById(this.views[1])).setInputType(InputType.TYPE_CLASS_DATETIME);
                }

            } else if(type.equals(INPUT_STATE_TYPE)      ||
                        type.equals(INPUT_DATE_TYPE)     ||
                        type.equals(INPUT_PROGRAMS_TYPE) ||
                        type.equals(INPUT_STATUS_TYPE) ){

                convertView.findViewById(this.views[3]).setOnClickListener((View.OnClickListener) this.listeners[0]);
                convertView.findViewById(this.views[4]).setOnClickListener((View.OnClickListener) this.listeners[1]);

                ((TextView) convertView.findViewById(this.views[0])).setText(((HashMap) contentListArray.get(position)).get(NAME_KEY).toString());

                String content = ((HashMap) contentListArray.get(position)).get(CONTENT_KEY).toString();


                if(type.equals(INPUT_DATE_TYPE) ){
                    if(content.equals("")){
                        content = "0";
                    }
                    long date_content = Long.parseLong(content);
                    if (date_content == 0) {
                        ((TextView) convertView.findViewById(this.views[2])).setText("- DATE -");
                    } else {
//                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//                        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//                        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
//
//                        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[((NSNumber *)content) doubleValue]];
//
//                        NSLog(@"stringFromDate: %@", [dateFormatter stringFromDate:date]);

                        Calendar date = Calendar.getInstance();
                        date.setTimeInMillis(Long.parseLong(content));
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                        // ((HashMap) content_key_list_Array.get(position)).put (CONTENT_KEY, (c.getTimeInMillis() / 1000L));


                        //Calendar date = Calendar.getInstance();
                        //date.setTimeInMillis(c.getTimeInMillis());

                        //long l = (date.getTimeInMillis());

                        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        //String format = formatter.format(c.getTime());

                        String format = formatter.format(date.getTime());

                        ((TextView) convertView.findViewById(this.views[2])).setText(format);
                    }
                }else{
                    if (content.equals("")) {
                        ((TextView) convertView.findViewById(this.views[2])).setText("- SELECT -");
                    } else {
                        ((TextView) convertView.findViewById(this.views[2])).setText(content);
                    }
                }
            } else if(type.equals(INPUT_DESCRIPTION)){
                String content = ((HashMap) contentListArray.get(position)).get(CONTENT_KEY).toString();
                ((TextView) convertView.findViewById(this.views[6])).setText(content);
            } else if(type.equals(INPUT_DESCRIPTION_SECTION)){
                String content = ((HashMap) contentListArray.get(position)).get(NAME_KEY).toString();
                ((TextView) convertView.findViewById(this.views[6])).setText(content);
            } else if(type.equals(INPUT_DISABLE_PROGRAMS)){
                String content = ((HashMap) contentListArray.get(position)).get(CONTENT_KEY).toString();

                ((TextView) convertView.findViewById(this.views[0])).setText(((HashMap) contentListArray.get(position)).get(NAME_KEY).toString());
                ((TextView) convertView.findViewById(this.views[2])).setText(content);
                convertView.findViewById(this.views[2]).setEnabled(false);
            }
        }

        convertView.setId(position);

        //cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return convertView;
    }
}
