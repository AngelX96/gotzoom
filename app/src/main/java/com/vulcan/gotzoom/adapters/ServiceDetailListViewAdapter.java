package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.vulcan.gotzoom.R;
import com.vulcan.gotzoom.ServicesActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 9/12/16.
 */
public class ServiceDetailListViewAdapter extends BaseAdapter {

    HashMap service_Dictionary;
    private ArrayList itemspreviewListArray;
    private ViewGroup parent;
    private Context context;
    private int[] resources;
    private int[] views;
    private int[] controls;
    // private Intent intent;
    private Object[] listeners;
    private ArrayList features_Array;

    public ServiceDetailListViewAdapter(Context context, HashMap service_Dictionary, int[] resources, int[] views, int[] controls, View.OnClickListener[] onClickListeners, ViewGroup parent) {
        super();
        this.service_Dictionary = service_Dictionary;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.listeners = onClickListeners;

        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        itemspreviewListArray = new ArrayList();

        for (int i=0; i<this.resources.length ; i++){
            itemspreviewListArray.add(li.inflate(this.resources[i], parent, false));
        }

        features_Array = (ArrayList) service_Dictionary.get("features");
    }

    @Override
    public int getCount() {
        return features_Array.size() + 3;
    }

    @Override
    public Object getItem(int position) {
        if (position<3) {
            return features_Array.get(position);
        }else{
            return ((int)features_Array.get(position) + 3);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        if ( position == 0 ) {
            convertView = li.inflate(this.resources[0], parent, false);
        } else if ( position == 1 ) {
            convertView = li.inflate(this.resources[1], parent, false);
        } else if ( position == 2 ) {
            convertView = li.inflate(this.resources[2], parent, false);
        } else if ( position >= 3 ) {
            convertView = li.inflate(this.resources[3], parent, false);
        }

//        R.layout.item_service_card,
//        R.layout.item_service_detail_name,
//        R.layout.item_service_detail_price,
//        R.layout.item_service_item
//
//        R.id.button,
//        R.id.name,
//        R.id.price,
//        R.id.price_month,
//        R.id.text

        if ( convertView != null ) {
            if ( position == 0 ) {
                convertView.findViewById(this.views[0]).setOnClickListener((View.OnClickListener) this.listeners[0]);
                ((Button) convertView.findViewById(this.views[0])).setText(String.format("Pay $ %(.0f", ((Double)service_Dictionary.get("price")).floatValue()));
            } else if ( position == 1 ) {

                ((TextView) convertView.findViewById(this.views[1])).setText((String)service_Dictionary.get("name"));
            } else if ( position == 2 ) {

                ((TextView) convertView.findViewById(this.views[2])).setText(String.format("Price $ %(.0f", ((Double)service_Dictionary.get("price")).floatValue()));
                ((TextView) convertView.findViewById(this.views[3])).setText(String.format("Plus $%(.2f a month", service_Dictionary.get("price_month")));
            } else if ( position >= 3 ) {

                ((TextView) convertView.findViewById(this.views[4])).setText( features_Array.get(position-3).toString());
            }
        }

        convertView.setId(position);

        //cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return convertView;

    }
}
