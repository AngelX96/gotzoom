package com.vulcan.gotzoom.adapters;

import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.vulcan.gotzoom.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 4/19/16.
 */
public class QuestionResultViewAdapter extends BaseAdapter {

    public final static String TITLE = "title";
    public final static String COUNT_OF_NESTING = "count_of_nesting";

    public String input_type;

    private ArrayList contentListArray;
    private Context context;
    private int[] resources;
    private int[] views;
    private int[] controls;
    // private Intent intent;
    private View.OnClickListener[] listners;

    public QuestionResultViewAdapter(Context context, ArrayList contentListArray, int[] resources, int[] views, int[] controls,
                                     View.OnClickListener[] listners) {
        super();
        this.contentListArray = contentListArray;
        this.context = context;
        this.resources = resources;
        this.views = views;
        this.controls = controls;
        this.listners = listners;

        // this.intent = intent;
    }

    @Override
    public int getCount() {
        return contentListArray.size();
    }

    @Override
    public Object getItem(int position) {
        return contentListArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateContentList(ArrayList contentListArray) {
        this.contentListArray = contentListArray;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String inflater = Context.LAYOUT_INFLATER_SERVICE;

        LayoutInflater li = (LayoutInflater) context.getSystemService(inflater);

        convertView = li.inflate(this.resources[0], parent, false);

        if (((HashMap) contentListArray.get(position)).get("type").equals("available")) {

            convertView = li.inflate(this.resources[0], parent, false);

        } else if (((HashMap) contentListArray.get(position)).get("type").equals("unavailable")) {

            convertView = li.inflate(this.resources[1], parent, false);

        } else if (((HashMap) contentListArray.get(position)).get("type").equals("description")) {

            convertView = li.inflate(this.resources[2], parent, false);
        }

        if (!((HashMap) contentListArray.get(position)).get("type").equals("description")) {
            convertView.setOnClickListener(this.listners[0]);

            ((TextView) convertView.findViewById(this.views[0])).setText(((HashMap) contentListArray.get(position)).get("name").toString());
            ((TextView) convertView.findViewById(this.views[1])).setText(String.format("$%(.2f", ((HashMap) contentListArray.get(position)).get("loanPeriod")));

            ((TextView) convertView.findViewById(this.views[2])).setText(String.format("$%(.2f", ((HashMap) contentListArray.get(position)).get("initialMonthlyPayment")));
        } else {

            ((TextView) convertView.findViewById(this.views[3])).setText(((HashMap) contentListArray.get(position)).get("text").toString());
        }

        //cell.selectionStyle = UITableViewCellSelectionStyleBlue;

        convertView.setId(position);
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return convertView;


    }
}
