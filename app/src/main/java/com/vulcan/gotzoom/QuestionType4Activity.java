package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.util.NavigationUtil;
import com.vulcan.gotzoom.util.PayUtil;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuestionType4Activity extends AppCompatActivity {

    private Intent intent;
    private Boolean _isEdit;
    private HashMap _answerDict;
    private HashMap responseDict;
    private Integer _Id;
    private Integer _nextId;

    private String input_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_type4);

        PayUtil.payAlert(this);

        intent = getIntent();

        _isEdit = intent.getBooleanExtra("isEdit", false);

        if (_isEdit) {
            ((Button) findViewById(R.id.buttonNext)).setText("Save");
        }

        _answerDict = (HashMap) intent.getSerializableExtra("answerDict");
        responseDict = (HashMap) intent.getSerializableExtra("responseDict");

        _nextId = ((Double) ((HashMap) responseDict.get("question")).get("next")).intValue();
        _Id = Integer.parseInt(((HashMap) responseDict.get("question")).get("id").toString());

        if (_Id.equals("104")) {
            ((EditText) findViewById(R.id.editText)).setText((String) ((HashMap) FileUtil.getAnswerById(this, "103").get("answer")).get("result"));
        } else if (_Id.equals("322")) {
            ((EditText) findViewById(R.id.editText)).setText((String) ((HashMap) FileUtil.getAnswerById(this, "321").get("answer")).get("result"));
        }

        ((TextView) findViewById(R.id.inputTypeLabel)).setText("#");

        if((((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get("type")).equals("money")){
            ((EditText) findViewById(R.id.editText)).setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            ((TextView) findViewById(R.id.inputTypeLabel)).setText("$");
            input_type = "decimal";
        }else if((((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get("type")).equals("percent")){
            ((EditText) findViewById(R.id.editText)).setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            ((TextView) findViewById(R.id.inputTypeLabel)).setText("%");
            input_type = "decimal";
        }else if((((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get("type")).equals("months")){
            ((EditText) findViewById(R.id.editText)).setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            ((TextView) findViewById(R.id.inputTypeLabel)).setText("m");
            input_type = "decimal";
        }else if((((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get("type")).equals("number")){
            ((EditText) findViewById(R.id.editText)).setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            ((TextView) findViewById(R.id.inputTypeLabel)).setText("#");
            input_type = "decimal";
        }else if((((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get("type")).equals("string")){
            ((EditText) findViewById(R.id.editText)).setInputType(InputType.TYPE_CLASS_TEXT);
            ((TextView) findViewById(R.id.inputTypeLabel)).setText("\" \"");
            input_type = "string";
        }

        if (_answerDict != null) {
            ((TextView) findViewById(R.id.questionLabel)).setText((String) _answerDict.get("title"));
            ((EditText) findViewById(R.id.editText)).setText((String) _answerDict.get("answer"));
        } else {
            ((TextView) findViewById(R.id.questionLabel)).setText((String) ((HashMap) responseDict.get("question")).get("q"));
        }
    }

    private String validateString(String checkString, String type) {

//        String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
//        CharSequence inputStr = phoneNumber;
//        Pattern pattern = Pattern.compile(expression);
//        Matcher matcher = pattern.matcher(inputStr);
//        if(matcher.matches()){
//            isValid = true;
//        }

        if (type.equals("ssn")) { // SSN is an outlet
            String expression = "^\\d{3}[- ]?\\d{2}[- ]?\\d{4}$";
            CharSequence inputStr = checkString;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if (!matcher.matches()) {
                return "Enter social security number in '000-00-0000' format";
            }

        } else if (type.equals("email")) {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(checkString).matches()) {
                return "Enter email 'email@example.com' format";
            }
        } else if (type.equals("number")||
                type.equals( "money")||
                type.equals( "months")){
            String expression = "^[0-9]*\\.?[0-9]+$";
            CharSequence inputStr = checkString;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if (!matcher.matches()) {
                return "Enter correct number";
            }
        }else if (type.equals("percent")){
            String expression = "^[0-9]*\\.?[0-9]+$";
            CharSequence inputStr = checkString;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if (!matcher.matches()) {
                return "Enter correct number";
            }
        }

        return null;
    }

    private Boolean isValidating() {
        int max, min;

        max = -1;
        min = -1;

        String item = null;
        Object[] key_arr =  ((HashMap) ((HashMap) responseDict.get("question")).get("validation")).keySet().toArray();

        for (int i = 0; i < key_arr.length; i++) {
            item = key_arr[i].toString();

            if (item.equals("max")) {
                max = ((Double) ((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get(item)).intValue();
            } else if (item.equals("min")) {
                min = ((Double) ((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get(item)).intValue();
            }
        }

        String validate_content = validateString(((EditText) findViewById(R.id.editText)).getText().toString(),
                ((String) ((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get("type")));

        if(!(((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get("type")).equals("string")) {
            if (validate_content == null) {
                Integer editint = Integer.parseInt(((EditText) findViewById(R.id.editText)).getText().toString());

                if (min > -1) {
                    Integer val = Integer.parseInt(((EditText) findViewById(R.id.editText)).getText().toString());
                    if (min <= val && max >= val) {

                    } else {

                        ((EditText) findViewById(R.id.editText)).setText("");
                        ((TextView) findViewById(R.id.errortextView)).setText((String.format("%d - %d", min, max)));
                        findViewById(R.id.errortextView).setVisibility(View.VISIBLE);
                        return false;
                    }
                } else if (((EditText) findViewById(R.id.editText)).getText().toString().equals("")) {
                    ((EditText) findViewById(R.id.editText)).setText("");
                    ((TextView) findViewById(R.id.errortextView)).setText("Field cannot be empty.");
                    findViewById(R.id.errortextView).setVisibility(View.VISIBLE);

                    return false;
                } else if (((HashMap) ((HashMap) responseDict.get("question")).get("validation")).get("type").equals("percent") &&
                        (editint < 0 || editint > 100)) {
                    ((EditText) findViewById(R.id.editText)).setText("");
                    ((TextView) findViewById(R.id.errortextView)).setText("1 - 100");
                    findViewById(R.id.errortextView).setVisibility(View.VISIBLE);

                    return false;
                }
            } else {
                ((EditText) findViewById(R.id.editText)).setText("");
                ((TextView) findViewById(R.id.errortextView)).setText(validate_content);
                findViewById(R.id.errortextView).setVisibility(View.VISIBLE);

                return false;
            }
        }
        return true;
    }

    public void onClickNextButton(View id){
       // NavigationUtil.navigateToQuestion(this,"-1",NavigationUtil.getViewControlsQuestionIdentiesDict(),findViewById(R.id.container_progress_view), findViewById(R.id.buttonNext));
        if(this.isValidating()){
            findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
            findViewById(R.id.buttonNext).setVisibility(View.GONE);

            String inputText = ((EditText) findViewById(R.id.editText)).getText().toString();
            String titleText = ((TextView) findViewById(R.id.questionLabel)).getText().toString();

            if (_Id.equals("105") || _Id.equals("104")) {
                FileUtil.writeAmountJsonToFileCalcCalendarJson(this, inputText);
            } else if(_Id.equals("107")){
                FileUtil.writeAmountJsonToFileCalcCalendarJson(this, inputText);
            }

            HashMap t_dictionary = new HashMap();
            t_dictionary.put("response_dict", responseDict);
            t_dictionary.put("title", titleText);
            t_dictionary.put("answer", inputText);

            HashMap answ_dictionary = new HashMap();
            answ_dictionary.put("answer", t_dictionary);

            FileUtil.addAnswerById(this, _Id.toString(), _nextId.toString(), answ_dictionary);
            if(_isEdit){
                onBackPressed();
            } else{

                NavigationUtil.navigateToQuestion(
                        this,
                        _nextId.toString(),
                        NavigationUtil.getViewControlsQuestionIdentiesDict(),
                        findViewById(R.id.container_progress_view),
                        findViewById(R.id.buttonNext));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}
