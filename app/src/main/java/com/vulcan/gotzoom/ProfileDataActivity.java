package com.vulcan.gotzoom;

import android.net.Uri;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.vulcan.gotzoom.adapters.ProfileDataViewAdapter;
import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.ConnectUtilListener;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.util.PayUtil;
import com.vulcan.gotzoom.volley.VolleyError;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ProfileDataActivity extends AppCompatActivity {

    static String TYPE_ROW_KEY = "type_row";
    static String ID_KEY = "id";
    static String TYPE_INPUT_KEY = "type_input";
    static String NAME_KEY = "name";
    static String REQUIRED_KEY = "required";
    static String ITEMS_KEY = "items";
    static String CONTENT_KEY = "content";
    static String ERROR_KEY = "error";
    static String ERROR_TEXT_KEY = "error_text";

    static String INPUT_TYPE = "input";
    static String INPUT_NUM_TYPE = "input_num";
    static String INPUT_PHONE_TYPE = "input_phone";

    static String INPUT_EMAIL_TYPE = "input_email";

    static String INPUT_STATE_TYPE = "input_state";
    static String INPUT_DATE_TYPE = "input_date";
    static String INPUT_PROGRAMS_TYPE = "input_programs";
    static String INPUT_STATUS_TYPE = "input_status";
    static String INPUT_DESCRIPTION = "description";
    static String INPUT_DESCRIPTION_SECTION = "description_section";

    static String SERVICE_WILL_UPGRADE_TYPE = "service_will_upgrade_type";
    static String SERVICE_DID_UPGRADE_TYPE  = "service_did_upgrade_type";

    static String INPUT_SSN_TYPE = "input_ssn";

    static String ROW = "row";
    static String SECTION = "section";

    private String potential_forgiveness_program;

    ArrayList content_key_list_Array;

    HashMap activeServiceDict;

    String[] state_listArray;
    String[] programsArray;
    String[] statusArray;

    int position;

    float height_show_button_constrain;
    float height_hide_button_constrain;
    float height_picker_container_constrain;
    float height_keyboard;
    float height_toolbar;

    Integer selected_row;

    EditText textfield;

    Boolean _isSendingMode;

    String _plan_name;

    float _initial_monthly_payment;
    float _loan_period;
    float _current_payment;
    float _currrent_term;

    int full_listview_height;
    int previousHeightDiffrence;
    Boolean isKeyBoardVisible;

    ListView list_view;

    private ProfileDataViewAdapter mListAdapterS;

    private Boolean isSendingMode;

    private EditText currentEditext;

    private Intent intent;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_data);

        PayUtil.payAlert(this);

        intent = getIntent();

        isSendingMode = intent.getBooleanExtra("isSendingMode",false);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();


    }

    private void  getActiveService(){
        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {
                HashMap responseDict = ParseConnectionMessages.parseQuestionFirst(str);

                if ((Boolean) responseDict.get("success")) {
                    HashMap dict = (HashMap) responseDict.get("data");
                    activeServiceDict = (HashMap) responseDict.get("data");

                    FileUtil.writeActiveServiceFileCalcCalendarJson(ProfileDataActivity.this, activeServiceDict);
                } else {
                    activeServiceDict = (HashMap) FileUtil.readJsonFromFileCalcCalendarJson(ProfileDataActivity.this).get("active_service");
                }


                ProfileDataActivity.this.listBuild();
            }

            @Override
            public void failResponse(VolleyError error) {
                activeServiceDict = (HashMap) FileUtil.readJsonFromFileCalcCalendarJson(ProfileDataActivity.this).get("active_service");

                ProfileDataActivity.this.listBuild();
            }
        };

        ConnectUtil.getActiveServiceByToken(this, listener, FileUtil.readTokenFromFile(this));
    }

    private ArrayList addServiceItem(ArrayList content_key_list_Array, HashMap activeServiceDict){
        //////////////////




        //        {
        //            data =     {
        //                Payments =         {
        //                    amount = "149.00";
        //                    "amount_sub" = "9.95";
        //                    created = "2016-08-15 13:11:19";
        //                };
        //                Services =         {
        //                    "for_married" = "<null>";
        //                    level = 2;
        //                    name = "WE DO IT  FOR YOU";
        //                    "name_description" = "(SINGLE PERSON  OR HEAD OF  HOUSEHOLD)";
        //                    price = "149.00";
        //                };
        //                id = 31;
        //                "service_id" = 3;
        //            };
        //            success = 1;
        //        }




//                           @"type_row" : @"row",
//                           @"error" : @"0",
//                           @"error_text" : @"This field cannot be left empty",
//                           @"id" : @"firstname",
//                           @"content":@"",
//                           @"type_input"   : @"input",
//                           @"name" : @"First name *",
//                           @"required": @"1"
//                           },
        if (activeServiceDict!=null) {
            String type = "";

            double service_id = Double.parseDouble(activeServiceDict.get("service_id").toString());

            switch ((int)service_id) {
                case 1:
                case 2:
                    type = SERVICE_WILL_UPGRADE_TYPE;
                    break;
                case 3:
                case 4:
                    type = SERVICE_DID_UPGRADE_TYPE;
                    break;

                default:
                    break;
            }

            HashMap item_service = new HashMap(activeServiceDict);
            item_service.put("type_input", type);

            if (type.equals(SERVICE_WILL_UPGRADE_TYPE) ||
                    type.equals(SERVICE_DID_UPGRADE_TYPE)) {
                content_key_list_Array.set(0, item_service);
            } else {
                content_key_list_Array.add(0, item_service);

            }
        }

        return content_key_list_Array;
    }

    @Override
    protected void onResume() {
        super.onResume();

        findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
        findViewById(R.id.main_container).setVisibility(View.GONE);

        intent = getIntent();

        this.getActiveService();

        list_view = (ListView) findViewById(R.id.listView);

        //height_picker_container_constrain = _height_container_pickerLayoutConstraint.constant;

//        height_show_button_constrain = _height_button_tableview_pickerLayoutConstraint.constant;
//        height_hide_button_constrain = 8;

//        _height_button_tableview_pickerLayoutConstraint.constant = height_hide_button_constrain;
//        _height_container_pickerLayoutConstraint.constant = 0;

        _isSendingMode = intent.getBooleanExtra("isSendingMode", true);

        if (_isSendingMode) {

            _plan_name = intent.getStringExtra("plan_name");
            _initial_monthly_payment = intent.getFloatExtra("initial_monthly_payment", 0);
            _loan_period = intent.getFloatExtra("loan_period", 0);
            _current_payment = intent.getFloatExtra("current_payment", 0);
            _currrent_term = intent.getFloatExtra("currrent_term", 0);

            findViewById(R.id.containerButtonView).setVisibility(FileUtil.isQuestionListFin(this) ? View.VISIBLE : View.VISIBLE);

        } else {

           // findViewById(R.id.containerButtonView).setVisibility(FileUtil.isQuestionListFin(this) ? View.VISIBLE : View.GONE);


            ((Button) findViewById(R.id.containerButtonView)).setText("Save");

            _plan_name = "";
            _initial_monthly_payment = 0;
            _loan_period = 0;
            _current_payment = 0;
            _currrent_term = 0;

        }

        //_loadActivityIndicatorView.hidden = YES;

        programsArray = new String[]{
                "N/A",
                "Public Service Loan Forgiveness",
                "Total and Permanent Disability Loan Discharge",
                "Income Based Forgiveness"
        };

        statusArray = new String[]{
                "Married Filing Jointly",
                "Married Filing Separately",
                "Single",
                "Head of Household",
        };

        state_listArray = new String[]{
                "Alabama",
                "Alaska",
                "Arizona",
                "Arkansas",
                "California",
                "Colorado",
                "Connecticut",
                "Delaware",
                "Florida",
                "Georgia",
                "Hawaii",
                "Idaho",
                "Illinois Indiana",
                "Iowa",
                "Kansas",
                "Kentucky",
                "Louisiana",
                "Maine",
                "Maryland",
                "Massachusetts",
                "Michigan",
                "Minnesota",
                "Mississippi",
                "Missouri",
                "Montana Nebraska",
                "Nevada",
                "New Hampshire",
                "New Jersey",
                "New Mexico",
                "New York",
                "North Carolina",
                "North Dakota",
                "Ohio",
                "Oklahoma",
                "Oregon",
                "Pennsylvania Rhode Island",
                "South Carolina",
                "South Dakota",
                "Tennessee",
                "Texas",
                "Utah",
                "Vermont",
                "Virginia",
                "Washington",
                "West Virginia",
                "Wisconsin",
                "Wyoming"
        };

        potential_forgiveness_program = FileUtil.readJsonFromFileCalcCalendarJson(this).get("potential_forgiveness_program").toString();

        if (potential_forgiveness_program == null) {
            potential_forgiveness_program = "N/A";
        }

        HashMap item = new HashMap();

        content_key_list_Array = new ArrayList();

        item = new HashMap();

        item.put("type_row", "section");
        item.put("name", "Personal information");
        item.put("type_input", "description_section");

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "firstname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "First name *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "middlename");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Middle name");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "lastname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Last name *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "former_name");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Former Name");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ssn");
        item.put("content", "");
        item.put("type_input", "input_ssn");
        item.put("name", "SSN *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "date_of_birth");
        item.put("content", "");
        item.put("type_input", "input_date");
        item.put("name", "Date of Birth");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "phone_home");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Home phone");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "phone_cell");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Cell phone");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "phone_work");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Work phone");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "address");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Address *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "address2");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Address2");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "city");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "City *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "state");
        item.put("content", "");
        item.put("type_input", "input_state");
        item.put("name", "State *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "zip");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Zip *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "fsa_username");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "FSA Username");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "fsa_password");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "FSA Password");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "dl_number");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "DL Number");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "dl_state");
        item.put("content", "");
        item.put("type_input", "input_state");
        item.put("name", "DL State");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "employer");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Employer");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "employer_phone");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Employer Phone");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "employer_address");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Employer Address");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "employer_address2");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Employer Address2");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "employer_city");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Employer City");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "employer_state");
        item.put("content", "");
        item.put("type_input", "input_state");
        item.put("name", "Employer State");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "employer_zip");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Employer Zip");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "annual_income");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Annual Income");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "filing_status");
        item.put("content", "");
        item.put("type_input", "input_status");
        item.put("name", "Filing Status *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "family_size");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Family Size");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "children_in_household");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Children in Household");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "adjusted_gross_income");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Adjusted Gross Income");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "adults_in_household");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Adults in Household");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "potential_forgiveness_programs");
        item.put("content", potential_forgiveness_program);
        item.put("type_input", "input_disable_programs");
        item.put("name", "Potential Forgiveness Programs");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "section");
        item.put("name", "Spouse information");
        item.put("type_input", "description_section");

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_firstname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "First name");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_middlname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Middle name");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_lastname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Last name");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_former_name");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Former Name");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_date_of_birth");
        item.put("content", "");
        item.put("type_input", "input_date");
        item.put("name", "Date of Birth");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_ssn");
        item.put("content", "");
        item.put("type_input", "input_ssn");
        item.put("name", "SSN");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_fsa_username");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "FSA Username");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_fsa_password");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "FSA Password");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_dl_number");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "DL Number");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_dl_state");
        item.put("content", "");
        item.put("type_input", "input_state");
        item.put("name", "DL State");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_phone_home");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Home phone");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_phome_cell");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Cell phone");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_phone_work");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Work phone");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_address");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Address");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_address2");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Address2");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_city");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "City");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_state");
        item.put("content", "");
        item.put("type_input", "input_state");
        item.put("name", "State");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_zip");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Zip");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_employer");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Employer");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_employer_phone");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Employer Phone");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_annual_income");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Annual Income");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "co_loan_balance");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Outstanding Federal Loan Balance");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "section");
        item.put("name", "Reference 1");
        item.put("type_input", "description_section");

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("content", "FULL References: 2 persons not residing in the same household");
        item.put("type_input", "description");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref1_firstname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "First name *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref1_lastname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Last name *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref1_address");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Address *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref1_city");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "City *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref1_state");
        item.put("content", "");
        item.put("type_input", "input_state");
        item.put("name", "State *");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref1_zip");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Zip *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref1_phone");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Phone *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref1_relationship");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Relationship *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "section");
        item.put("name", "Reference 2");
        item.put("type_input", "description_section");

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("content", "FULL References: 2 persons not residing in the same household");
        item.put("type_input", "description");
        item.put("required", 0);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref2_firstname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "First name *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref2_lastname");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Last name *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref2_address");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Address *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref2_city");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "City *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref2_state");
        item.put("content", "");
        item.put("type_input", "input_state");
        item.put("name", "State *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref2_zip");
        item.put("content", "");
        item.put("type_input", "input_num");
        item.put("name", "Zip *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref2_phone");
        item.put("content", "");
        item.put("type_input", "input_phone");
        item.put("name", "Phone *");
        item.put("required", 1);

        content_key_list_Array.add(item);

        item = new HashMap();

        item.put("type_row", "row");
        item.put("error", 0);
        item.put("error_text", "This field cannot be left empty");
        item.put("id", "ref2_relationship");
        item.put("content", "");
        item.put("type_input", "input");
        item.put("name", "Relationship *");
        item.put("required", 1);

        content_key_list_Array.add(item);


    }

    private void uncheckErrorLabels() {
        int section = -1;
        int item = -1;

        HashMap item_dict = new HashMap();

        int first_position = this.list_view.getFirstVisiblePosition();
        int last_position = list_view.getLastVisiblePosition();

        for (int i = 0; i < content_key_list_Array.size(); i++) {
            item_dict = (HashMap) content_key_list_Array.get(i);

            if (!item_dict.equals(INPUT_DESCRIPTION) && !item_dict.equals(INPUT_DESCRIPTION_SECTION)) {
                ((HashMap) content_key_list_Array.get(i)).put("error", 0);
            }
        }

        item_dict = new HashMap();

        mListAdapterS.updateContentList(content_key_list_Array);

        for (int i = 0; i <= (last_position - first_position); i++) {

            item_dict = (HashMap) content_key_list_Array.get(i);

            if (!item_dict.equals(INPUT_DESCRIPTION) && !item_dict.equals(INPUT_DESCRIPTION_SECTION)) {

                String type = item_dict.get(TYPE_INPUT_KEY).toString();

                //[self addContent:[NSNumber numberWithInt:0] key:ERROR_KEY indexPath:[NSIndexPath indexPathForItem:item inSection:section]];

                list_view.getChildAt(i).findViewById(R.id.errorLabel).setVisibility(View.INVISIBLE);
            }
        }
    }

    private void listBuild() {
        ArrayList tempArray = FileUtil.readProfileData(this);

        if (tempArray != null) {
            content_key_list_Array = tempArray;
        }

        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {

            }

            @Override
            public void failResponse(VolleyError error) {

            }
        };

        ConnectUtil.loadPersonaByToken(this, listener, FileUtil.readTokenFromFile(this));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Titile")
                .setPositiveButton("fire", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                    }
                })
                .setNegativeButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        View.OnClickListener addButtonListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = ((View) view.getParent().getParent().getParent()).getId();

                //checkingItemInPath(position);

                LayoutInflater inflater = ProfileDataActivity.this.getLayoutInflater();

                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileDataActivity.this);

                View alert_view = null;

                NumberPicker itemPicker;

                NumberPicker.OnValueChangeListener numberpicker_listener = new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        String type;

                        if (((HashMap) content_key_list_Array.get(position)).get(TYPE_ROW_KEY).equals(ROW)) {

                            type = ((HashMap) content_key_list_Array.get(position)).get(TYPE_INPUT_KEY).toString();

                            if (type.equals(INPUT_STATE_TYPE)) {
                                // ((TextView) list_view.getChildAt(position).findViewById(R.id.labetText)).setText(state_listArray[newVal]);
                                ((HashMap) content_key_list_Array.get(position)).put(CONTENT_KEY, state_listArray[newVal]);

                                int first_position = list_view.getFirstVisiblePosition();
                                int last_position = list_view.getLastVisiblePosition();

                                for (int i = 0; i <= (last_position - first_position); i++) {
                                    if ((position - first_position) == i) {
                                        //((RadioButton) list_view.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                                        ((TextView) list_view.getChildAt(i).findViewById(R.id.labelText)).setText(state_listArray[newVal]);
                                    }
                                }
                            } else if (type.equals(INPUT_DATE_TYPE)) {

                            } else if (type.equals(INPUT_PROGRAMS_TYPE)) {
                                //((TextView) list_view.getChildAt(position).findViewById(R.id.labetText)).setText(programsArray[newVal]);
                                ((HashMap) content_key_list_Array.get(position)).put(CONTENT_KEY, programsArray[newVal]);

                                int first_position = list_view.getFirstVisiblePosition();
                                int last_position = list_view.getLastVisiblePosition();

                                for (int i = 0; i <= (last_position - first_position); i++) {
                                    if ((position - first_position) == i) {
                                        //((RadioButton) list_view.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                                        ((TextView) list_view.getChildAt(i).findViewById(R.id.labelText)).setText(programsArray[newVal]);
                                    }
                                }
                            } else if (type.equals(INPUT_STATUS_TYPE)) {
                                // ((TextView) list_view.getChildAt(position).findViewById(R.id.labetText)).setText(statusArray[newVal]);
                                ((HashMap) content_key_list_Array.get(position)).put(CONTENT_KEY, statusArray[newVal]);

                                int first_position = list_view.getFirstVisiblePosition();
                                int last_position = list_view.getLastVisiblePosition();

                                for (int i = 0; i <= (last_position - first_position); i++) {
                                    if ((position - first_position) == i) {
                                        // ((RadioButton) list_view.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                                        ((TextView) list_view.getChildAt(i).findViewById(R.id.labelText)).setText(statusArray[newVal]);
                                    }
                                }
                            }
                        }
                    }
                };

                String type;
                if (((HashMap) content_key_list_Array.get(position)).get(TYPE_ROW_KEY).equals(ROW)) {

                    type = ((HashMap) content_key_list_Array.get(position)).get(TYPE_INPUT_KEY).toString();

                    if (type.equals(INPUT_STATE_TYPE)) {
                        alert_view = inflater.inflate(R.layout.alert_dialog_item_picket, null);

                        itemPicker = (NumberPicker) alert_view.findViewById(R.id.itemPicker);
                        itemPicker.setOnValueChangedListener(numberpicker_listener);
                        itemPicker.setMinValue(0);
                        itemPicker.setMaxValue(state_listArray.length - 1);
                        itemPicker.setDisplayedValues(state_listArray);
                        itemPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

                    } else if (type.equals(INPUT_DATE_TYPE)) {
                        alert_view = inflater.inflate(R.layout.alert_dialog_date_picket, null);

                        DatePicker.OnDateChangedListener datepicker_listener = new DatePicker.OnDateChangedListener() {
                            @Override
                            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar c = Calendar.getInstance();
                                c.set(Calendar.YEAR, year);
                                c.set(Calendar.MONTH, monthOfYear);
                                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                c.set(Calendar.HOUR, 0);
                                c.set(Calendar.MINUTE, 0);
                                c.set(Calendar.SECOND, 0);
                                c.set(Calendar.MILLISECOND, 0);

                                ((HashMap) content_key_list_Array.get(position)).put(CONTENT_KEY, (c.getTimeInMillis()));

                                //Calendar date = Calendar.getInstance();
                                //date.setTimeInMillis(c.getTimeInMillis());

                                //long l = (date.getTimeInMillis());

                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String format = formatter.format(c.getTime());

                                //((TextView) list_view.getChildAt(position).findViewById(R.id.labetText)).setText(format);

                                int first_position = list_view.getFirstVisiblePosition();
                                int last_position = list_view.getLastVisiblePosition();

                                for (int i = 0; i <= (last_position - first_position); i++) {
                                    if ((position - first_position) == i) {
                                        ((TextView) list_view.getChildAt(i).findViewById(R.id.labelText)).setText(format);
                                        //((RadioButton) list_view.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                                    }
                                }
                            }
                        };

                        Calendar today = Calendar.getInstance();

                        long unixTimeStamp = 0;

                        if (((HashMap) content_key_list_Array.get(position)).get(CONTENT_KEY).equals("") ||
                                ((HashMap) content_key_list_Array.get(position)).get(CONTENT_KEY).equals(0)) {
                            unixTimeStamp = 0;
                        } else {
                            unixTimeStamp = (long) ((HashMap) content_key_list_Array.get(position)).get(CONTENT_KEY);
                        }

                        today.setTimeInMillis(unixTimeStamp);

                        ((DatePicker) alert_view.findViewById(R.id.datePicker)).init(
                                today.get(Calendar.YEAR),
                                today.get(Calendar.MONTH),
                                today.get(Calendar.DAY_OF_MONTH),
                                datepicker_listener);

                    } else if (type.equals(INPUT_PROGRAMS_TYPE)) {
                        alert_view = inflater.inflate(R.layout.alert_dialog_item_picket, null);

                        itemPicker = (NumberPicker) alert_view.findViewById(R.id.itemPicker);
                        itemPicker.setOnValueChangedListener(numberpicker_listener);
                        itemPicker.setMinValue(0);
                        itemPicker.setMaxValue(programsArray.length - 1);
                        itemPicker.setDisplayedValues(programsArray);
                        itemPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

                    } else if (type.equals(INPUT_STATUS_TYPE)) {
                        alert_view = inflater.inflate(R.layout.alert_dialog_item_picket, null);

                        itemPicker = (NumberPicker) alert_view.findViewById(R.id.itemPicker);
                        itemPicker.setOnValueChangedListener(numberpicker_listener);
                        itemPicker.setMinValue(0);
                        itemPicker.setMaxValue(statusArray.length - 1);
                        itemPicker.setDisplayedValues(statusArray);
                        itemPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

                    }
                }

                TextView titleTextView = (TextView) alert_view.findViewById(R.id.title_textView);
                titleTextView.setText(((HashMap) content_key_list_Array.get(position)).get(NAME_KEY).toString());

                builder.setView(alert_view)
                        // Add action buttons
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

                builder.create();
            }
        };

        View.OnClickListener removeButtonListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer position = view.getId();

                //checkingItemInPath(position);
            }
        };

        View.OnFocusChangeListener focusEditText = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    position = ((View) v.getParent().getParent()).getId();

                    int first_position = list_view.getFirstVisiblePosition();
                    int last_position = list_view.getLastVisiblePosition();

                    for (int i = 0; i <= (last_position - first_position); i++) {
                        int index = (position - first_position);
                        if ((position - first_position) == i) {
                            //((RadioButton) list_view.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                            currentEditext = ((EditText) list_view.getChildAt(i).findViewById(R.id.editText));

                            //list_view.getChildAt(i).findViewById(R.id.labetText).setFocusable(true);
                        }
                    }

                    currentEditext.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
                            //i++;
                            //tv.setText(String.valueOf(i) + " / " + String.valueOf(charCounts));

                            String st = currentEditext.getText().toString();

                            ((HashMap) content_key_list_Array.get(position)).put(CONTENT_KEY, currentEditext.getText().toString());

                            mListAdapterS.updateContentList(content_key_list_Array);
                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }
                    });
                } else {

                }
            }
        };

        View.OnTouchListener touchEditText = new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                position = ((View) v.getParent().getParent()).getId();

                int first_position = list_view.getFirstVisiblePosition();
                int last_position = list_view.getLastVisiblePosition();

                for (int i = 0; i <= (last_position - first_position); i++) {
                    if ((position - first_position) == i) {
                        //((RadioButton) list_view.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                        currentEditext = ((EditText) list_view.getChildAt(i).findViewById(R.id.editText));

                        //list_view.getChildAt(i).findViewById(R.id.labetText).setFocusable(true);
                    }
                }

                currentEditext.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        //i++;
                        //tv.setText(String.valueOf(i) + " / " + String.valueOf(charCounts));

                        String st = currentEditext.getText().toString();

                        ((HashMap) content_key_list_Array.get(position)).put(CONTENT_KEY, currentEditext.getText().toString());

                        mListAdapterS.updateContentList(content_key_list_Array);
                    }

                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                });

                return false;
            }
        };

        View.OnClickListener selectEditText = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = ((View) view.getParent().getParent()).getId();

                //list_view.smoothScrollToPosition(position);
                //showSoftKeyboard(view);
                //checkingItemInPath(position);

                //list_view.setDividerHeight(100);

                int first_position = list_view.getFirstVisiblePosition();
                int last_position = list_view.getLastVisiblePosition();

                for (int i = 0; i <= (last_position - first_position); i++) {
                    if ((position - first_position) == i) {
                        //((RadioButton) list_view.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                        currentEditext = ((EditText) list_view.getChildAt(i).findViewById(R.id.editText));


                        //list_view.getChildAt(i).findViewById(R.id.labetText).setFocusable(true);
                    }
                }

//                currentEditext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                        boolean handled = false;
//
//                        String st = v.getText().toString();
//
//                        if (actionId == EditorInfo.IME_ACTION_SEND) {
//                            // sendMessage();
//                            handled = true;
//                        }
//                        return handled;
//                    }
//                });

                currentEditext.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        //i++;
                        //tv.setText(String.valueOf(i) + " / " + String.valueOf(charCounts));
                        String st = currentEditext.getText().toString();

                        ((HashMap) content_key_list_Array.get(position)).put(CONTENT_KEY, currentEditext.getText().toString());

                        mListAdapterS.updateContentList(content_key_list_Array);
                    }

                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                });
            }
        };

        content_key_list_Array = addServiceItem(content_key_list_Array, activeServiceDict);
        mListAdapterS = new ProfileDataViewAdapter(
                this,
                content_key_list_Array,
                new int[]{
                        R.layout.item_profile_data_input,
                        R.layout.item_profile_data_select,
                        R.layout.item_profile_data_description,
                        R.layout.item_profile_data_will_upgrade,
                        R.layout.item_profile_data_did_upgrade
                },
                new int[]{
                        R.id.nameTextView,
                        R.id.editText,
                        R.id.labelText,
                        R.id.addButton,
                        R.id.deleteButton,
                        R.id.errorLabel,
                        R.id.description_Label,
                        R.id.name,
                        R.id.payment,
                        R.id.date
                },
                new int[]{},
                new Object[]{addButtonListner, removeButtonListner, focusEditText},
                list_view);

        list_view = (ListView) this.findViewById(R.id.listView);
        list_view.setAdapter(mListAdapterS);
        list_view.setLongClickable(false);
        list_view.setClickable(false);

        //list_view.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS); //FOCUS_BLOCK_DESCENDANTS

        findViewById(R.id.container_progress_view).setVisibility(View.GONE);
        findViewById(R.id.main_container).setVisibility(View.VISIBLE);

        this.uncheckErrorLabels();
    }

    public void onClickSubmitButton(View id) {
        //    plan_name
//    initial_monthly_payment
//    loan_period
//    current_payment
//    currrent_term
        FileUtil.writeProfileData(this, content_key_list_Array);

        if (_isSendingMode) {

            Boolean isError = false;

            int section = -1;
            int item = -1;

            findViewById(R.id.container_progress_view).setVisibility(View.GONE);

            HashMap item_dict = new HashMap();

            int first_position = list_view.getFirstVisiblePosition();
            int last_position = list_view.getLastVisiblePosition();

            for (int i = first_position; i <= last_position; i++) {

                item_dict = (HashMap) content_key_list_Array.get(i);

                item++;

                if (!item_dict.equals(INPUT_DESCRIPTION) || !item_dict.equals(INPUT_DESCRIPTION_SECTION)) {

                    String type = item_dict.get(TYPE_INPUT_KEY).toString();

                    if (item_dict.get(REQUIRED_KEY) != null) {
                        if (((int) item_dict.get(REQUIRED_KEY) == 1) &&
                                (item_dict.get(CONTENT_KEY).equals("")||
                                        item_dict.get(CONTENT_KEY).equals("- SELECT -") ||
                                        item_dict.get(CONTENT_KEY).equals("- DATE -")
                                )) {

                            list_view.getChildAt(i - first_position).findViewById(R.id.errorLabel).setVisibility(View.VISIBLE);

                            ((TextView) list_view.getChildAt(i - first_position).findViewById(R.id.errorLabel)).setText("This field cannot be left empty");

                            isError = true;
                        } else {

                            if (type.equals(INPUT_TYPE) ||
                                    type.equals(INPUT_NUM_TYPE) ||
                                    type.equals(INPUT_PHONE_TYPE) ||
                                    type.equals(INPUT_EMAIL_TYPE) ||
                                    type.equals(INPUT_SSN_TYPE)) {
                                list_view.getChildAt(i - first_position).findViewById(R.id.errorLabel).setVisibility(View.INVISIBLE);

                            } else if (type.equals(INPUT_STATE_TYPE) ||
                                    type.equals(INPUT_DATE_TYPE) ||
                                    type.equals(INPUT_PROGRAMS_TYPE) ||
                                    type.equals(INPUT_STATUS_TYPE)) {
                                list_view.getChildAt(i - first_position).findViewById(R.id.errorLabel).setVisibility(View.INVISIBLE);

                            }
                        }
                    }
                }
            }

            for (int i = 0; i < content_key_list_Array.size(); i++) {
                item_dict = (HashMap) content_key_list_Array.get(i);
                item++;

                if (!item_dict.equals(INPUT_DESCRIPTION) &&
                        !item_dict.equals(INPUT_DESCRIPTION_SECTION) &&
                        !item_dict.equals(SERVICE_WILL_UPGRADE_TYPE) &&
                        !item_dict.equals(SERVICE_DID_UPGRADE_TYPE)) {
                    String type = item_dict.get(TYPE_INPUT_KEY).toString();


                    if (item_dict.get(REQUIRED_KEY) != null) {
                        Object content = item_dict.get(CONTENT_KEY);
                        Boolean isrequared = ((int) item_dict.get(REQUIRED_KEY)) == 1;
                        Object requared = item_dict.get(REQUIRED_KEY);
                        Boolean isnull = item_dict.get(CONTENT_KEY) == null;

                        if (((int) item_dict.get(REQUIRED_KEY) == 1) &&
                                (item_dict.get(CONTENT_KEY) != null)) {
                            Boolean isempty = (item_dict.get(CONTENT_KEY).equals("") ||
                                    item_dict.get(CONTENT_KEY).equals("- SELECT -") ||
                                    item_dict.get(CONTENT_KEY).equals("- DATE -"));
                        }
                    }
                    if (item_dict.get(REQUIRED_KEY) != null) {
                        if (((int) item_dict.get(REQUIRED_KEY) == 1) &&
                                (item_dict.get(CONTENT_KEY).equals(""))) {
                            ((HashMap) content_key_list_Array.get(i)).put(ERROR_KEY, 1);

                            ((HashMap) content_key_list_Array.get(i)).put(ERROR_TEXT_KEY, "This field cannot be left empty");

                            isError = true;

                        } else if (((int) item_dict.get(REQUIRED_KEY) == 1) &&
                                (item_dict.get(CONTENT_KEY).equals("") ||
                                        item_dict.get(CONTENT_KEY).equals("- SELECT -") ||
                                        item_dict.get(CONTENT_KEY).equals("- DATE -")
                                )) {
                            ((HashMap) content_key_list_Array.get(i)).put(ERROR_KEY, 1);

                            ((HashMap) content_key_list_Array.get(i)).put(ERROR_TEXT_KEY, "This field cannot be left empty");

                            isError = true;
                        } else {
                            ((HashMap) content_key_list_Array.get(i)).put(ERROR_KEY, 0);
                        }
                    }
                }
            }

            mListAdapterS.updateContentList(content_key_list_Array);

            if (isError) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Warning")
                        .setMessage("Please input all requared fields.")
                        .setCancelable(false)
                        .setNegativeButton("ОК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();

                builder.create();

                //[_profileTableView reloadData];

                //[self presentViewController:alert animated:YES completion:nil];

                //_submitButton.hidden = NO;
                //_loadActivityIndicatorView.hidden = YES;
            } else {
                HashMap send_dict = new HashMap();

                if (_isSendingMode) {
                    send_dict.put("plan_name", _plan_name);
                    send_dict.put("initial_monthly_payment", _initial_monthly_payment);
                    send_dict.put("loan_period", _loan_period);
                    send_dict.put("current_payment", _current_payment);
                    send_dict.put("currrent_term", _currrent_term);
                }

                for (int i = 0; i < content_key_list_Array.size(); i++) {
                    item_dict = (HashMap) content_key_list_Array.get(i);
                    if (!item_dict.get(TYPE_INPUT_KEY).equals(INPUT_DESCRIPTION) &&
                            !item_dict.get(TYPE_INPUT_KEY).equals(INPUT_DESCRIPTION_SECTION) &&
                            !item_dict.get(TYPE_INPUT_KEY).equals(SERVICE_WILL_UPGRADE_TYPE) &&
                            !item_dict.get(TYPE_INPUT_KEY).equals(SERVICE_DID_UPGRADE_TYPE)) {
                        if (item_dict.get(TYPE_INPUT_KEY).equals(INPUT_DATE_TYPE)) {
                            if ((int)item_dict.get(CONTENT_KEY) == 0 ||  item_dict.get(CONTENT_KEY).equals("")) {
                                send_dict.put(item_dict.get(ID_KEY), 0);
                            } else {
                                Calendar date = Calendar.getInstance();

                                date.setTimeInMillis(Long.parseLong(item_dict.get(CONTENT_KEY).toString()));
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String format = formatter.format(date.getTime());

                                send_dict.put(item_dict.get(ID_KEY), format);
                            }
                        } else {
                            send_dict.put(item_dict.get(ID_KEY), item_dict.get(CONTENT_KEY));
                        }
                    }
                }


                ConnectUtilListener listener = new ConnectUtilListener() {
                    @Override
                    public void successResponse(String str) {
                        findViewById(R.id.container_progress_view).setVisibility(View.GONE);

                        HashMap responseDict = ParseConnectionMessages.parseQuestionFirst(str);

                        if ((boolean) responseDict.get("success")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileDataActivity.this);
                            builder.setTitle("Success")
                                    .setMessage("Report sending success.")
                                    .setCancelable(false)
                                    .setNegativeButton("Next",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    Intent intent = new Intent(ProfileDataActivity.this, ForbearanceActivity.class);
                                                    ProfileDataActivity.this.startActivity(intent);
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileDataActivity.this);
                            builder.setTitle("Warning")
                                    .setMessage("Please input all requared fields.")
                                    .setCancelable(false)
                                    .setNegativeButton("OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
//                                                Intent intent = new Intent(ProfileDataActivity.this, MainActivity.class);
//                                                ProfileDataActivity.this.startActivity(intent);
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();

                            int section = -1;
                            int item = -1;

                            HashMap item_dict = new HashMap();

                            String error_key_item = "";

                            ArrayList response_errors = new ArrayList();
                            response_errors.addAll(((HashMap) responseDict.get("errors")).keySet());

                            int first_position = list_view.getFirstVisiblePosition();
                            int last_position = list_view.getLastVisiblePosition();

                            for (int i = 0; i <= (last_position - first_position); i++) {

                                item_dict = (HashMap) content_key_list_Array.get(first_position + i);

                                item++;

                                if (!item_dict.equals(INPUT_DESCRIPTION) &&
                                        !item_dict.equals(INPUT_DESCRIPTION_SECTION) &&
                                        !item_dict.equals(SERVICE_WILL_UPGRADE_TYPE) &&
                                        !item_dict.equals(SERVICE_DID_UPGRADE_TYPE)) {
                                    String type = item_dict.get(TYPE_INPUT_KEY).toString();

                                    for (int j = 0; j < response_errors.size(); j++) {
                                        error_key_item = response_errors.get(j).toString();

                                        if (error_key_item.equals(item_dict.get(ID_KEY))) {


                                            if (type.equals(INPUT_TYPE) ||
                                                    type.equals(INPUT_NUM_TYPE) ||
                                                    type.equals(INPUT_PHONE_TYPE) ||
                                                    type.equals(INPUT_EMAIL_TYPE) ||
                                                    type.equals(INPUT_SSN_TYPE)) {

                                                list_view.getChildAt(i).findViewById(R.id.errorLabel).setVisibility(View.INVISIBLE);

                                                if (!((HashMap) responseDict.get("errors")).get(error_key_item).getClass().equals(ArrayList.class)) {

                                                    HashMap obj1 = (HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item);
                                                    HashMap obj2 = (HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item);
                                                    String obj3 = ((HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item)).keySet().toArray()[0].toString();

                                                    View v = list_view.getChildAt(i).findViewById(R.id.errorLabel);

                                                    String st = obj2.get(obj3).toString();

                                                    ((TextView) list_view.getChildAt(i).findViewById(R.id.errorLabel)).setText(obj2.get(obj3).toString());
                                                } else {
                                                    ((TextView) list_view.getChildAt(i).findViewById(R.id.errorLabel)).setText(((ArrayList) ((HashMap) responseDict.get("errors")).get(error_key_item)).get(0).toString());
                                                }
                                            } else if (type.equals(INPUT_STATE_TYPE) ||
                                                    type.equals(INPUT_DATE_TYPE) ||
                                                    type.equals(INPUT_PROGRAMS_TYPE) ||
                                                    type.equals(INPUT_STATUS_TYPE)) {

                                                list_view.getChildAt(i).findViewById(R.id.errorLabel).setVisibility(View.INVISIBLE);

                                                HashMap obj1 = (HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item);
                                                HashMap obj2 = (HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item);
                                                String obj3 = ((ArrayList) ((HashMap) responseDict.get("errors")).get(error_key_item)).get(0).toString();

                                                obj1.get(obj2.get(obj3));

                                                ((TextView) list_view.getChildAt(i).findViewById(R.id.errorLabel)).setText(obj1.get(obj2.get(obj3)).toString());

                                                if (((HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item)).keySet().toArray().length > 0) {

                                                    obj1 = (HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item);
                                                    obj2 = (HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item);
                                                    obj3 = ((HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item)).keySet().toArray()[0].toString();

                                                    obj1.get(obj2.get(obj3));

                                                    ((TextView) list_view.getChildAt(i).findViewById(R.id.errorLabel)).setText(obj1.get(obj2.get(obj3)).toString());
                                                } else {
                                                    ((TextView) list_view.getChildAt(i).findViewById(R.id.errorLabel)).setText(((ArrayList) ((HashMap) responseDict.get("errors")).get(error_key_item)).get(0).toString());
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            for (int i = 0; i < content_key_list_Array.size(); i++) {

                                item_dict = (HashMap) content_key_list_Array.get(i);

                                ((HashMap) content_key_list_Array.get(i)).put(ERROR_KEY, 0);

                                item++;

                                if (!item_dict.equals(INPUT_DESCRIPTION) || !item_dict.equals(INPUT_DESCRIPTION_SECTION)) {
                                    String type = item_dict.get(TYPE_INPUT_KEY).toString();

                                    for (int j = 0; j < response_errors.size(); j++) {
                                        error_key_item = response_errors.get(j).toString();

                                        if (error_key_item.equals(item_dict.get(ID_KEY))) {

                                            if(((HashMap) responseDict.get("errors")).get(error_key_item).getClass() == ArrayList.class){
                                                ArrayList obj2 = (ArrayList) ((HashMap) responseDict.get("errors")).get(error_key_item);

                                                ((HashMap) content_key_list_Array.get(i)).put(ERROR_KEY, 1);
                                                ((HashMap) content_key_list_Array.get(i)).put(ERROR_TEXT_KEY, obj2.get(0).toString());
                                            } else {
                                                HashMap obj2 = (HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item);
                                                String obj3 = ((HashMap) ((HashMap) responseDict.get("errors")).get(error_key_item)).keySet().toArray()[0].toString();

                                                ((HashMap) content_key_list_Array.get(i)).put(ERROR_KEY, 1);
                                                ((HashMap) content_key_list_Array.get(i)).put(ERROR_TEXT_KEY, obj2.get(obj3).toString());
                                            }
                                        }
                                    }
                                }
                            }

                            mListAdapterS.updateContentList(content_key_list_Array);
                        }

                        findViewById(R.id.container_progress_view).setVisibility(View.GONE);
                        findViewById(R.id.main_container).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failResponse(VolleyError error) {
                        findViewById(R.id.container_progress_view).setVisibility(View.GONE);
                        findViewById(R.id.main_container).setVisibility(View.VISIBLE);
                    }
                };

                ConnectUtil.sendProfileToken(this, listener, FileUtil.readTokenFromFile(this), send_dict);

                findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
                findViewById(R.id.main_container).setVisibility(View.GONE);
            }

            //FileUtil.writeProfileData(this, content_key_list_Array);
        }else{
            onBackPressed();

            FileUtil.writeProfileData(this, content_key_list_Array);
        }
    }

    public void showSoftKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ProfileData Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.vulcan.gotzoom/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ProfileData Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.vulcan.gotzoom/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            FileUtil.writeProfileData(this, content_key_list_Array);
            onBackPressed();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClickupgradeButton(View id){
        Intent intent_services = new Intent(getApplicationContext(), ServicesActivity.class);
        startActivity(intent_services);
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}