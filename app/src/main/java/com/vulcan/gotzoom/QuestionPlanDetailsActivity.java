package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.ConnectUtilListener;
import com.vulcan.gotzoom.util.FileUtil;

import java.util.ArrayList;
import java.util.HashMap;

public class QuestionPlanDetailsActivity extends AppCompatActivity {

    Intent intent;

    String _plan_name;

    double _value_107;
    double _value_108;
    double _loan_period;
    double _initial_monthly_payment;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_plan_details);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onResume() {
        super.onResume();

        intent = getIntent();

        //String.format("%d months (%.1f years)", _calc_months, ((float) _calc_months / 12));

        _plan_name = intent.getStringExtra("plan_name");

        _value_107 = intent.getDoubleExtra("value_107", 0);
        _value_108 = intent.getDoubleExtra("value_108", 0);
        _loan_period = intent.getDoubleExtra("loan_period", 0);
        _initial_monthly_payment = intent.getDoubleExtra("initial_monthly_payment", 0);

        double total_saving = ((_value_107 * _value_108) - (_loan_period * _initial_monthly_payment));

        findViewById(R.id.errorLabel).setVisibility(View.INVISIBLE);

        ((TextView) findViewById(R.id.plan_name_Title)).setText(_plan_name);

        ((WebView) findViewById(R.id.webView)).loadData(intent.getStringExtra("plan_description"), null, null);

        ((TextView) findViewById(R.id.current_1_Label)).setText(String.format("$%.2f", _value_107));
        ((TextView) findViewById(R.id.current_2_Label)).setText(String.format("%.0f months (%.2f years)", _value_108, (_value_108 / 12)));
        ((TextView) findViewById(R.id.current_3_Label)).setText(String.format("$%.2f", (_value_107 * _value_108)));

        ((TextView) findViewById(R.id.plan_name_second_Title)).setText(String.format("Your New Repayment Trajectory under %s Repayment plan:", _plan_name));

        ((TextView) findViewById(R.id.new_repayment_1_Label)).setText(String.format("$%.2f", _initial_monthly_payment));
        ((TextView) findViewById(R.id.new_repayment_2_Label)).setText(String.format("%.0f months (%.2f years)", _loan_period, (_loan_period / 12)));
        ((TextView) findViewById(R.id.new_repayment_3_Label)).setText(String.format("$%.2f", (_loan_period * _initial_monthly_payment)));
        ((TextView) findViewById(R.id.new_repayment_4_Label)).setText(String.format("$%.2f", total_saving));

        if (total_saving < 0) {
            findViewById(R.id.errorLabel).setVisibility(View.VISIBLE);
        }

        ScrollView v = (ScrollView) findViewById(R.id.scrollLayout);
       // v.fullScroll(View.FOCUS_UP);
        v.pageScroll(View.FOCUS_UP);
    }

    public void onClickApplyButton(View id) {
//        Intent intent = new Intent(this, ProfileDataActivity.class);
//
//        intent.putExtra("isSendingMode", true);
//        intent.putExtra("plan_name", _plan_name);
//        intent.putExtra("initial_monthly_payment", _initial_monthly_payment);
//        intent.putExtra("loan_period", _loan_period);
//        intent.putExtra("current_payment", _value_107);
//        intent.putExtra("currrent_term", _value_108);
//
//        this.startActivity(intent);

        Intent intent = new Intent(this, ServicesActivity.class);

//        intent.putExtra("isSendingMode", true);
//        intent.putExtra("plan_name", _plan_name);
//        intent.putExtra("initial_monthly_payment", _initial_monthly_payment);
//        intent.putExtra("loan_period", _loan_period);
//        intent.putExtra("current_payment", _value_107);
//        intent.putExtra("currrent_term", _value_108);

        this.startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "QuestionPlanDetails Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.vulcan.gotzoom/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "QuestionPlanDetails Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.vulcan.gotzoom/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}