package com.vulcan.gotzoom;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.ConnectUtilListener;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.util.NavigationUtil;
import com.vulcan.gotzoom.util.PayUtil;
import com.vulcan.gotzoom.volley.VolleyError;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected void onResume() {
        super.onResume();

        PayUtil.payAlert(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        Object o = FileUtil.readTokenFromFile(this);

        if(FileUtil.readTokenFromFile(this).equals("")){
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_login_drawer);
            ((TextView)  navigationView.getHeaderView(0).findViewById(R.id.textView)).setText("Please login or register.");
        } else {

            ((TextView)  navigationView.getHeaderView(0).findViewById(R.id.textView)).setText(FileUtil.readDictFromTokenFile(this).get("email").toString());
        }


        navigationView.setNavigationItemSelectedListener(this);

        Log.i("MainActivity", "JSON  " + "Start");

        final Context context = this;

        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {

                //FileUtil.writeTestFile(context, ParseConnectionMessages.parseQuestionFirst(str));
                //HashMap map = FileUtil.readTestFile(context);

                Log.i("MainActivity", "JSON  " + str);
            }

            @Override
            public void failResponse(VolleyError error) {

            }
        };

        ConnectUtil.loadQuestionFirst(this, listener);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profiledata) {
            Intent intent = new Intent(this, ProfileDataActivity.class);
            intent.putExtra("isSendingMode", false);
            startActivity(intent);

        } else if (id == R.id.nav_questionlist) {
            Intent intent = new Intent(this, QuestionListActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            Intent intent = new Intent(this, LogoutActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_login) {
            FileUtil.writeAuthQuestionToFile(this, false);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_downloadablelist) {
            Intent intent = new Intent(this, DownloadableFormsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void onClickStartButton(View id){
       // _page_loaderActivityIndicatorView.hidden = NO;
       // _nextButton.hidden = YES;

        findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
        findViewById(R.id.buttonNext).setVisibility(View.GONE);

        NavigationUtil.navigateToQuestion(this,"-1",NavigationUtil.getViewControlsQuestionIdentiesDict(),findViewById(R.id.buttonNext), findViewById(R.id.container_progress_view));

//        Intent intent = new Intent(this, QuestionListActivity.class);
//        startActivity(intent);
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}
