package com.vulcan.gotzoom;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.vulcan.gotzoom.adapters.DownloadableFormsListViewAdapter;
import com.vulcan.gotzoom.adapters.ForbearanceListViewAdapter;
import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.ConnectUtilListener;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.volley.VolleyError;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class DownloadableFormsActivity extends AppCompatActivity {

    private DownloadableFormsListViewAdapter mListAdapterS;

    private ListView listView;

    private ArrayList contentArray;

    private HashMap download_status_Dictionary;

    final String title_cell       = "title_cell";
    final String description_cell = "description_cell";
    final String button_cell      = "button_cell";

    // Progress Dialog
    private ProgressDialog pDialog;

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    // File url to download
    private static String file_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloadable_forms);

        listView = (ListView) findViewById(R.id.listView);

        contentArray = new ArrayList();

        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {
                HashMap responseDict = ParseConnectionMessages.parseQuestionFirst(str);
                findViewById(R.id.container_progress_view).setVisibility(View.GONE);

                if ((Boolean) responseDict.get("success")){
                    ArrayList dataArray = (ArrayList) responseDict.get("data");

                    ArrayList description_api_Array;
                    HashMap content_item;

                    download_status_Dictionary = new HashMap();

                    for (int i = 0; i < dataArray.size(); i++){
                        download_status_Dictionary.put(((HashMap) dataArray.get(i)).get("slug"),0);
                        description_api_Array = (ArrayList) ((HashMap) dataArray.get(i)).get("description_api");

                        content_item = new HashMap();
                        content_item.put("type", title_cell);
                        content_item.put("content", ((HashMap) dataArray.get(i)).get("name"));

                        contentArray.add(content_item);

                        if (description_api_Array != null) {
                            for (int j = 0; j < description_api_Array.size(); j++) {
                                content_item = new HashMap();
                                content_item.put("type", description_cell);
                                content_item.put("content", description_api_Array.get(j));

                                contentArray.add(content_item);
                            }
                        }

                        content_item = new HashMap();
                        content_item.put("type", button_cell);
                        content_item.put("slug", ((HashMap) dataArray.get(i)).get("slug"));

                        contentArray.add(content_item);
                    }

                    download_status_Dictionary = checkFileStatus(download_status_Dictionary);

                    mListAdapterS = new DownloadableFormsListViewAdapter(
                            DownloadableFormsActivity.this,
                            contentArray,
                            download_status_Dictionary,
                            new int[]{
                                    R.layout.item_form_downloadable_button_download,
                                    R.layout.item_form_downloadable_button_open,
                                    R.layout.item_form_downloadable_description,
                                    R.layout.item_form_downloadable_title
                            },
                            new int[]{
                                    R.id.openButton,
                                    R.id.downloadButton,
                                    R.id.text
                            },
                            new int[]{},
                            listView);

                    listView = (ListView) DownloadableFormsActivity.this.findViewById(R.id.listView);
                    listView.setAdapter(mListAdapterS);
                    listView.setLongClickable(false);
                    listView.setClickable(false);

                    findViewById(R.id.listView).setVisibility(View.VISIBLE);
                } else {
                    String error = "You are not authorized to access that location.";

                    AlertDialog.Builder builder = new AlertDialog.Builder(DownloadableFormsActivity.this);
                    builder.setTitle("Alert")
                            .setMessage(error)
                            .setCancelable(false)
                            .setNegativeButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

                findViewById(R.id.container_progress_view).setVisibility(View.GONE);

            }

            @Override
            public void failResponse(VolleyError error) {

            }
        };

        ConnectUtil.getDownloadableForms(this, listener);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (contentArray.size() > 0) {

        } else {
            findViewById(R.id.listView).setVisibility(View.INVISIBLE);
            findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
        }
    }

    public void onClickDownloadButton(View id){
        String token = FileUtil.readTokenFromFile(this);

        int position = ((View) id.getParent().getParent()).getId();

        String slug = ((HashMap) contentArray.get(position)).get("slug").toString();

        file_url = ConnectUtil.getURLDownloableFullUrl(token, slug);
        String path = String.format("%s/gotzoom/frames/%s.pdf", "sdcard", slug);


        new DownloadFileFromURL().execute(file_url, path);
    }

    public void onClickOpenFileButton(View id){
        int position = ((View) id.getParent().getParent()).getId();

        String slug = ((HashMap) contentArray.get(position)).get("slug").toString();

        String file_path = String.format("%s/gotzoom/frames/%s.pdf", "sdcard", slug);
        File file = new File(file_path);
        Uri path = Uri.fromFile(file);

        Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
        pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfOpenintent.setDataAndType(path, "application/pdf");

        try {
            startActivity(pdfOpenintent);
        }
        catch (ActivityNotFoundException e) {
            String error = "No application found for open PDF";

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Alert")
                    .setMessage(error)
                    .setCancelable(false)
                    .setNegativeButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }

    public HashMap checkFileStatus(HashMap download_status_Dictionary){
        ArrayList slug_key_Array = new ArrayList();
        slug_key_Array.addAll(download_status_Dictionary.keySet());

        for (int i = 0; i < slug_key_Array.size(); i++){
            String path = String.format("%s/gotzoom/frames/%s.pdf", Environment.getExternalStorageDirectory().toString(), slug_key_Array.get(i));
            //String path = Environment.getExternalStorageDirectory().toString() + "/1/downloadedfile.jpg";


            File file = new File(path);

            if(file.exists()){
                download_status_Dictionary.put(slug_key_Array.get(i), 2);
            }else{
                download_status_Dictionary.put(slug_key_Array.get(i), 0);
            }
        }

        return download_status_Dictionary;
    }

    public void checkButtonFileStatus(HashMap download_status_Dictionary){
        download_status_Dictionary = this.checkFileStatus(download_status_Dictionary);

        DownloadableFormsListViewAdapter parent_adapter = ((DownloadableFormsListViewAdapter)listView.getAdapter());
        parent_adapter.setDownloadStatusDictionary(download_status_Dictionary);
        parent_adapter.notifyDataSetChanged();
    }

    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();

                return pDialog;
            default:
                return null;
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);

            pDialog.setProgress(0);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                //OutputStream output = new FileOutputStream(f_url[1]);
                //OutputStream output = new FileOutputStream("sdcard/forbearance-form.pdf");
                OutputStream output = new FileOutputStream(f_url[1]);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

            checkButtonFileStatus(download_status_Dictionary);

            // Displaying downloaded image into image view
            // Reading image path from sdcard
            // String path = Environment.getExternalStorageDirectory().toString() + "/gotzoom/forms/forbearance-form.pdf";
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
