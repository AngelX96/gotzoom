package com.vulcan.gotzoom;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.vulcan.gotzoom.adapters.ForbearanceListViewAdapter;
import com.vulcan.gotzoom.adapters.ServicesListViewAdapter;
import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.FileUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class ForbearanceActivity extends AppCompatActivity {
    private int state;



    private HashMap download_status_Dictionary;

    private ForbearanceListViewAdapter mListAdapterS;

    private  ListView listView;

    // Progress Dialog
    private ProgressDialog pDialog;

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    // File url to download
    private static String file_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forbearance);

        state = 1;
        ArrayList state_1_array;
        ArrayList state_2_array;
        ArrayList state_3_array;

        Button buttonContinue = (Button) findViewById(R.id.buttonContinue);

        download_status_Dictionary = new HashMap();
        download_status_Dictionary.put("forbearance-form", 0);

        switch (state) {
            case 1:
            case 2:
                buttonContinue.setVisibility(View.GONE);
                break;
            case 3:
                buttonContinue.setVisibility(View.VISIBLE);
                break;

            default:
                break;
        }

//        NSFileManager *fileManager = [NSFileManager defaultManager];
//
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//
//        downloded_list_Array = [fileManager contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/forms", [paths objectAtIndex:0]] error:Nil];

        HashMap item;

        state_1_array = new ArrayList();

        item = new HashMap();
        item.put("type","forbearance_big_description");
        item.put("content", "Now that you have begun to process your consolidation, a forbearance request is something you may want to consider. What is it? It’s a temporary pause of payments, gives you a time extension to make your payments, or temporarily accepts smaller payments that were previously scheduled. If you are experiencing financial difficulties, currently unemployed, on partial disability, or any other documented hardship then a forbearance request may be for you!");
        state_1_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_button_item");
        item.put("content", "Would you like to submit a forbearance request?");
        state_1_array.add(item);

        state_2_array = new ArrayList();

        item = new HashMap();
        item.put("type", "forbearance_big_description");
        item.put("content", "Now that you have begun to process your consolidation, a forbearance request is something you may want to consider. What is it? It’s a temporary pause of payments, gives you a time extension to make your payments, or temporarily accepts smaller payments that were previously scheduled. If you are experiencing financial difficulties, currently unemployed, on partial disability, or any other documented hardship then a forbearance request may be for you!");
        state_2_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_small_description");
        item.put("content", "Some important things to know about forbearance:");
        state_2_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_item");
        item.put("content", "Will only be granted for up to 12 months at a time and cannot exceed a total of 36 months for the entire term of the loan");
        state_2_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_item");
        item.put("content", "Any loans that are in default cannot be placed into forbearance");
        state_2_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_item");
        item.put("content", "Interest will continue to accrue");
        state_2_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_button_item");
        item.put("content", "Would you like to submit a forbearance request?");
        state_2_array.add(item);

        state_3_array = new ArrayList();

        item = new HashMap();
        item.put("type", "forbearance_big_description");
        item.put("content", "Now that you have begun to process your consolidation, a forbearance request is something you may want to consider. What is it? It’s a temporary pause of payments, gives you a time extension to make your payments, or temporarily accepts smaller payments that were previously scheduled. If you are experiencing financial difficulties, currently unemployed, on partial disability, or any other documented hardship then a forbearance request may be for you!");
        state_3_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_small_description");
        item.put("content", "Some important things to know about forbearance:");
        state_3_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_item");
        item.put("content", "Will only be granted for up to 12 months at a time and cannot exceed a total of 36 months for the entire term of the loan");
        state_3_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_item");
        item.put("content", "Any loans that are in default cannot be placed into forbearance");
        state_3_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_item");
        item.put("content", "Interest will continue to accrue");
        state_3_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_button_item");
        item.put("content", "Would you like to submit a forbearance request?");
        state_3_array.add(item);

        item = new HashMap();
        item.put("type", "forbearance_download");
        item.put("content", "Please download the form below. You will need to fill it out with proper dates, sign and fax or mail to your current loan servicer.");
        state_3_array.add(item);

        download_status_Dictionary = new HashMap();

        File wallpaperDirectory = new File("/sdcard/gotzoom/frames");
// have the object build the directory structure, if needed.
        wallpaperDirectory.mkdirs();

        //[9/1/16, 8:39:34 PM] Alexandr  Frankovskiy: http://gz.test.digitalepoch.com/downloadable-forms/get/forbearance-form
        //[9/1/16, 8:40:12 PM] Alexandr  Frankovskiy: slug = forbearance-form
        download_status_Dictionary.put("forbearance-form", 0);

//        NSMutableArray* temp_downlodable_list_Array = [NSMutableArray array];
//        NSMutableDictionary* temp_downlodable_Dictionary = [NSMutableDictionary dictionary];
//        [temp_downlodable_Dictionary setObject:@"forbearance-form" forKey:@"slug"];
//        [temp_downlodable_list_Array insertObject:temp_downlodable_Dictionary atIndex:0];
//        downlodable_list_Array = [NSArray arrayWithArray:temp_downlodable_list_Array];

        View.OnClickListener buttonYesListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        };

        View.OnClickListener buttonNoListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        };

        download_status_Dictionary = this.checkFileStatus(download_status_Dictionary);

        mListAdapterS = new ForbearanceListViewAdapter(
                this,
                state,
                download_status_Dictionary,
                state_1_array,
                state_2_array,
                state_3_array,
                new int[]{
                        R.layout.item_forbearance_big_description,
                        R.layout.item_forbearance_button,
                        R.layout.item_forbearance_download,
                        R.layout.item_forbearance_item,
                        R.layout.item_forbearance_small_description,
                        R.layout.item_forbearance_open
                },
                new int[]{
                        R.id.text,
                        R.id.openButton,
                        R.id.downloadButton,
                        R.id.open_container,
                        R.id.download_container
                },
                new int[]{},
                new View.OnClickListener[]{buttonYesListner, buttonNoListner},
                listView);

        listView = (ListView) this.findViewById(R.id.listView);
        listView.setAdapter(mListAdapterS);
        listView.setLongClickable(false);
        listView.setClickable(false);
    }


    public void onClickYesButton(View id){
        View view = (View) id.getParent().getParent().getParent();

        state++;

        if(state > 2){
            findViewById(R.id.buttonContinue).setVisibility(View.VISIBLE);
        }

        ListView parent_ListView = (ListView) view;
        ForbearanceListViewAdapter parent_adapter = ((ForbearanceListViewAdapter)parent_ListView.getAdapter());
        parent_adapter.setState(state);
        parent_adapter.notifyDataSetChanged();

        listView.setSelection(listView.getCount() - 1);
    }

    public void onClickNoButton(View id){
        Intent intent = new Intent(getApplicationContext(), DownloadableFormsActivity.class);
        startActivity(intent);
    }

    public void onClickDownloadButton(View id){
        String token = FileUtil.readTokenFromFile(this);
        file_url = ConnectUtil.getURLDownloableFullUrl(token, "forbearance-form");
        String path = String.format("%s/gotzoom/frames/%s.pdf", "sdcard", "forbearance-form");


        new DownloadFileFromURL().execute(file_url, path);
    }

    public void onClickOpenFileButton(View id){
        String file_path = String.format("%s/gotzoom/frames/%s.pdf", "sdcard", "forbearance-form");
        File file = new File(file_path);
        Uri path = Uri.fromFile(file);

        Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
        pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfOpenintent.setDataAndType(path, "application/pdf");

        try {
            startActivity(pdfOpenintent);
        }
        catch (ActivityNotFoundException e) {
            String error = "No application found for open PDF";

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Alert")
                    .setMessage(error)
                    .setCancelable(false)
                    .setNegativeButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void onClickContinueButton(View id){
        Intent intent = new Intent(getApplicationContext(), DownloadableFormsActivity.class);
        startActivity(intent);
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }

    public HashMap checkFileStatus(HashMap download_status_Dictionary){
        ArrayList slug_key_Array = new ArrayList();
        slug_key_Array.addAll(download_status_Dictionary.keySet());

        for (int i = 0; i < slug_key_Array.size(); i++){
            String path = String.format("%s/gotzoom/frames/%s.pdf", Environment.getExternalStorageDirectory().toString(), slug_key_Array.get(i));
            //String path = Environment.getExternalStorageDirectory().toString() + "/1/downloadedfile.jpg";


            File file = new File(path);

            if(file.exists()){
                download_status_Dictionary.put(slug_key_Array.get(i), 2);
            }else{
                download_status_Dictionary.put(slug_key_Array.get(i), 0);
            }
        }

        return download_status_Dictionary;
    }

    public void checkButtonFileStatus(HashMap download_status_Dictionary){
        download_status_Dictionary = this.checkFileStatus(download_status_Dictionary);

        ForbearanceListViewAdapter parent_adapter = ((ForbearanceListViewAdapter)listView.getAdapter());
        parent_adapter.setDownloadStatusDictionary(download_status_Dictionary);
        parent_adapter.notifyDataSetChanged();
    }

    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();

                return pDialog;
            default:
                return null;
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                //OutputStream output = new FileOutputStream(f_url[1]);
                //OutputStream output = new FileOutputStream("sdcard/forbearance-form.pdf");
                OutputStream output = new FileOutputStream(f_url[1]);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

            checkButtonFileStatus(download_status_Dictionary);

            // Displaying downloaded image into image view
            // Reading image path from sdcard
            // String path = Environment.getExternalStorageDirectory().toString() + "/gotzoom/forms/forbearance-form.pdf";
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
