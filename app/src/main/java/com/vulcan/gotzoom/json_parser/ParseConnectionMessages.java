package com.vulcan.gotzoom.json_parser;

import android.support.annotation.Nullable;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 4/20/16.
 */
public class ParseConnectionMessages {


    public enum ConnectionKeys {
        success, question, next, q, group, available_update, validation, answer, q_type, id, no_text, check_type;
    }

    public enum JsonTokenKeys {
        BEGIN_ARRAY, BEGIN_OBJECT, BOOLEAN, END_ARRAY, END_DOCUMENT, END_OBJECT, NAME, NULL, NUMBER, STRING;
    }

    public enum ParentTypeKeys {
        ARRAY, OBJECT;
    }

    public static HashMap parseJSONStringToHashMap(String str) {
        ArrayList<HashMap> array = new ArrayList<HashMap>();

        HashMap map = new HashMap();

        Boolean success = false;

        JsonReader reader = new JsonReader(new StringReader(str));

        return (HashMap) ParseConnectionMessages.parseJson(reader, null, null);
    }

    public static HashMap parseQuestionFirst(String str) {
        ArrayList<HashMap> array = new ArrayList<HashMap>();

        HashMap map = new HashMap();

        Boolean success = false;

        JsonReader reader = new JsonReader(new StringReader(str));

        return (HashMap) ParseConnectionMessages.parseJson(reader, null, null);
    }

    public static Object parseJson(JsonReader reader, Object parentObj, ParentTypeKeys parentType) {
        String t_name = "";

        Object object = new Object();

        IOException t_e = null;

        //ParseConnectionMessages.testparseJson(reader);

        try {
            while (reader.hasNext()) {
                JsonToken jsonToken = reader.peek();
                switch (JsonTokenKeys.valueOf(jsonToken.name())) {
                    case BEGIN_ARRAY: {
                        reader.beginArray();

                        if (parentObj == null) {
                            object = new ArrayList();
                            ParseConnectionMessages.parseJson(reader, object, ParentTypeKeys.ARRAY);
                        } else {
                            switch (parentType) {
                                case ARRAY: {
                                    object = new ArrayList();
                                    ((ArrayList) parentObj).add(ParseConnectionMessages.parseJson(reader, object, ParentTypeKeys.ARRAY));
                                    break;
                                }
                                case OBJECT: {
                                    object = new ArrayList();
                                    ((HashMap) parentObj).put(t_name, ParseConnectionMessages.parseJson(reader, object, ParentTypeKeys.ARRAY));
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case BEGIN_OBJECT: {
                        reader.beginObject();

                        if (parentObj == null) {
                            object = new HashMap();
                            ParseConnectionMessages.parseJson(reader, object, ParentTypeKeys.OBJECT);
                        } else {
                            switch (parentType) {
                                case ARRAY: {
                                    object = new HashMap();
                                    ((ArrayList) parentObj).add(ParseConnectionMessages.parseJson(reader, object, ParentTypeKeys.OBJECT));
                                    break;
                                }
                                case OBJECT: {
                                    object = new HashMap();
                                    ((HashMap) parentObj).put(t_name, ParseConnectionMessages.parseJson(reader, object, ParentTypeKeys.OBJECT));
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case BOOLEAN: {
                        switch (parentType) {
                            case ARRAY: {
                                ((ArrayList) parentObj).add(reader.nextBoolean());

                                break;
                            }
                            case OBJECT: {
                                ((HashMap) parentObj).put(t_name, reader.nextBoolean());
                                break;
                            }
                        }
                        break;
                    }
                    case END_ARRAY: {
                        reader.endArray();
                        return parentObj;
                    }
                    case END_DOCUMENT: {
                        return object;
                    }
                    case END_OBJECT: {
                        reader.endObject();
                        return parentObj;
                    }
                    case NAME: {
                        t_name = reader.nextName();
                        if (t_name.equals("max")) {
                            String s = "";
                        }
                        break;
                    }
                    case NULL: {
                        switch (parentType) {
                            case ARRAY: {
                                ((ArrayList) parentObj).add(null);

                                break;
                            }
                            case OBJECT: {
                                ((HashMap) parentObj).put(t_name, null);
                                break;
                            }
                        }
                        reader.nextNull();
                        break;
                    }
                    case NUMBER: {
                        switch (parentType) {
                            case ARRAY: {
                                ((ArrayList) parentObj).add(reader.nextDouble());

                                break;
                            }
                            case OBJECT: {
                                ((HashMap) parentObj).put(t_name, reader.nextDouble());
                                break;
                            }
                        }
                        break;
                    }
                    case STRING: {
                        switch (parentType) {
                            case ARRAY: {
                                ((ArrayList) parentObj).add(reader.nextString());

                                break;
                            }
                            case OBJECT: {
                                ((HashMap) parentObj).put(t_name, reader.nextString());
                                break;
                            }
                        }
                        break;
                    }
                    default: {
                        reader.skipValue();
                        break;
                    }
                }
            }
            switch (parentType) {
                case ARRAY: {
                    reader.endArray();
                    break;
                }
                case OBJECT: {
                    reader.endObject();
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            t_e = e;

        }

        Log.i("Parse", "%s", t_e);

        if (parentObj == null) {
            return object;
        } else {
            return parentObj;
        }
    }
}

//// Question 1
////        {
////            "success":true,
////                "question":{
////                    "next":102,
////                    "q":"What is your Ultimate goal?",
////                    "group":1,
////                    "available_update":1,
////                    "validation":[],
////                    "answer":{
////                        "1":"Lower your payment",
////                        "2":"Shorter your repayment term",
////                        "3":"Make your loans into one loan - one payment",
////                        "4":"Find forgiveness options",
////                        "5":"Make payments based on your income and family size"},
////                    "q_type":1,
////                    "id":101
////        }}
//// Question 1 q_type 1
////        {
////            "success":true,
////                "question":{
////                        "next":{
////                            "yes":104,
////                            "no":105
////                            },
////                "q":"Do you know your FSA ID and Password?",
////                    "no_text":"You need to create your FSA ID in order to process any applications through the Department of Education system. Click here to create FSA ID. This step is not required to find your options through GotZoom.",
////                    "group":1,
////                    "available_update":0,
////                    "validation":[],
////            "q_type":3,
////                    "id":"103"
////        }}
//
//try {
//        reader.beginObject();
//
//        map = new HashMap();
//
//        while (reader.hasNext()) {
//        String name = reader.nextName();
//
//        switch (ConnectionKeys.valueOf(name)) {
//        case success: {
//        success = reader.nextBoolean();
//        map.put("success", success);
//        break;
//        }
//
//        case question: {
//
//        reader.beginObject();
//
//        HashMap question_map = new HashMap();
//
//        if (success) {
//
//        while (reader.hasNext()) {
//        name = reader.nextName();
//
//        switch (ConnectionKeys.valueOf(name)) {
//        case next: {
//        JsonToken jsonToken = reader.peek();
//
//        switch (JsonTokenKeys.valueOf(jsonToken.name())) {
//        case BEGIN_OBJECT: {
//        reader.beginObject();
//
//        HashMap next_map = new HashMap();
//        while (reader.hasNext()) {
//        next_map.put(reader.nextName(), reader.nextInt());
//        }
//
//        reader.endObject();
//
//        question_map.put("next", next_map);
//        break;
//        }
//
//        case NUMBER: {
//        question_map.put("next", reader.nextInt());
//        break;
//        }
//        }
//
//        break;
//        }
//
//        case q: {
//        question_map.put("q", reader.nextString());
//        break;
//        }
//
//        case no_text: {
//        question_map.put(name, reader.nextString());
//        break;
//        }
//
//        case group: {
//        question_map.put("group", reader.nextInt());
//        break;
//        }
//
//        case check_type: {
//        question_map.put("check_type", reader.nextInt());
//        break;
//        }
//
//        case available_update: {
//        question_map.put("available_update", reader.nextInt());
//        break;
//        }
//
//        case validation: {
//
//        HashMap validation_map = new HashMap();
//
//        JsonToken validToken = reader.peek();
//        switch (JsonTokenKeys.valueOf(validToken.name())) {
//        case BEGIN_OBJECT: {
//        reader.beginObject();
//        validation_map = new HashMap();
//        while (reader.hasNext()) {
//        validation_map.put(reader.nextName(), reader.nextString());
//        }
//        reader.endObject();
//
//        question_map.put("validation", validation_map);
//        break;
//        }
//        case BEGIN_ARRAY: {
//        ArrayList<HashMap> validation_array = new ArrayList<HashMap>();
//        reader.beginArray();
//        validation_map = new HashMap();
//        while (reader.hasNext()) {
//        validation_map.put(reader.nextName(), reader.nextInt());
//        }
//        validation_array.add(validation_map);
//        reader.endArray();
//
//        question_map.put("validation", validation_array);
//        break;
//        }
//
//        case NUMBER: {
//        question_map.put("validation", reader.nextInt());
//        break;
//        }
//        }
//        //question_map.put("validation",validation_array);
//        break;
//        }
//
//        case q_type: {
//        question_map.put("q_type", reader.nextInt());
//        break;
//        }
//
//        case id: {
//        question_map.put("id", reader.nextInt());
//        break;
//        }
//
//        case answer: {
//        reader.beginObject();
//
//        HashMap answer_map = new HashMap();
//
//        while (reader.hasNext()) {
//
//        answer_map.put(reader.nextName(), reader.nextString());
//
//        }
//        reader.endObject();
//
//        question_map.put("answer", answer_map);
//        break;
//        }
//
//default: {
//        reader.skipValue();
//        break;
//        }
//        }
//        }
//
//        }
//
//        reader.endObject();
//
//        map.put("question", question_map);
//        break;
//        }
//
//default: {
//        reader.skipValue();
//        break;
//        }
//        }
//        }
//
//        //array.add(map);
//
//        reader.endObject();
//
//
//        } catch (IOException e) {
//        e.printStackTrace();
//        }