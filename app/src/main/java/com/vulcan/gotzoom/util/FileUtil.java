package com.vulcan.gotzoom.util;

import android.content.Context;
import android.content.Intent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;

/**
 * Created by andiil on 4/21/16.
 */
public class FileUtil {

    private static String PROFILE_DATA = "gz_profile_data";
    private static String DATA = "gz_data";
    private static String TOKEN = "gz_token";
    private static String CALC_CALENDAR = "gz_calccalendar";

    private static void writeFile(String file_name, Context context, Object obj) {
        try {
            FileOutputStream fileOut = context.openFileOutput(file_name, Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(obj);
            out.close();
            fileOut.close();

        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    private static Object readFile(String file_name, Context context) {
        Object e = null;
        try {
            FileInputStream fileIn = context.openFileInput(file_name);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }

        return e;
    }

    public static void writeProfileData(Context context, Object obj) {
        FileUtil.writeFile(PROFILE_DATA, context, obj);
    }

    public static ArrayList readProfileData(Context context) {


        return (ArrayList) FileUtil.readFile(PROFILE_DATA, context);
    }

    public static void writeJsonToFileDataJson(Context context, Object obj) {
        FileUtil.writeFile(DATA, context, obj);
    }

    public static HashMap readJsonFromFileDataJson(Context context) {
        HashMap map = (HashMap) FileUtil.readFile(DATA, context);

        return map==null?new HashMap():map;
    }

    public static void addAnswerById(Context context, String answ_id, String answ_next_id, HashMap answ_map) {
        ArrayList answers, list;

        HashMap read_dictionary = FileUtil.readJsonFromFileDataJson(context);

        if (!read_dictionary.isEmpty()) {

            list = (ArrayList) read_dictionary.get("list");
            answers = (ArrayList) read_dictionary.get("answers");

            HashSet<Integer> indexes = new HashSet<Integer>();
            Boolean isChange = false;
            Boolean isFind = false;
            int index = -1;

            for (int i = 0; i < list.size(); i++) {
                if (!isFind && Integer.parseInt((String)list.get(i)) == Integer.parseInt(answ_id)) {
                    isFind = true;
                    index = i;
                    if (!isChange && i < (list.size() - 1)) {

                        if (Integer.parseInt((String) list.get(i + 1)) != Integer.parseInt(answ_next_id)) {
                            isChange = true;
                            indexes.add(i);
                        }
                    }
                } else if (isChange) {
                    indexes.add(i);
                }
            }

            if (isChange) {
                FileUtil.removeByIndexSet(list, indexes);
                FileUtil.removeByIndexSet(answers, indexes);

                list.add(answ_id);

                HashMap temp_map = new HashMap();
                temp_map.put("id", answ_id);
                temp_map.put("answer", answ_map.get("answer"));

                answers.add(temp_map);
            } else if (isFind) {
                list.set(index, answ_id);

                HashMap temp_map = new HashMap();
                temp_map.put("id", answ_id);
                temp_map.put("answer", answ_map.get("answer"));

                answers.set(index, temp_map);
            } else if (!isFind) {
                list.add(answ_id);

                HashMap temp_map = new HashMap();
                temp_map.put("id", answ_id);
                temp_map.put("answer", answ_map.get("answer"));

                answers.add(temp_map);
            }
        } else {
            answers = new ArrayList();
            list = new ArrayList();
            list.add(answ_id);

            HashMap temp_map = new HashMap();

            temp_map.put("id", answ_id);
            temp_map.put("answer", answ_map.get("answer"));

            answers.add(temp_map);
        }

        if (Integer.parseInt(answ_next_id)==999){
            if (!FileUtil.isQuestionListFin(context)){
                list.add("999");

                HashMap temp_map = new HashMap();

                temp_map.put("id", "999");
                temp_map.put("answer", "");

                answers.add(temp_map);
            }
        }

        HashMap temp_map = new HashMap();

        temp_map.put("list", list);
        temp_map.put("answers", answers);

        FileUtil.writeJsonToFileDataJson(context, temp_map);
    }

    private static ArrayList removeByIndexSet(ArrayList arr, HashSet<Integer> indexes){
        ArrayList temp_arr = new ArrayList(arr);

        Iterator iterator = indexes.iterator();

        while(iterator.hasNext()){
            temp_arr.remove(iterator.next());
        }

        return  new ArrayList(temp_arr);
    }


    public static boolean isQuestionListFin(Context context) {
        HashMap read_dictionary = new HashMap(FileUtil.readJsonFromFileDataJson(context));

        ArrayList list;

        if (!read_dictionary.isEmpty()){
            list = new ArrayList((ArrayList)read_dictionary.get("list"));

            ListIterator iterator = list.listIterator();
            while (iterator.hasNext()){
                if (iterator.next().equals("999"))
                {
                    return  true;
                }
            }
        }
        return false;
    }

    public static HashMap getAnswerById(Context context, String answ_id) {
        ArrayList answers;

        HashMap read_dictionary = new HashMap(FileUtil.readJsonFromFileDataJson(context));

        if(!read_dictionary.isEmpty()){
        answers =  new ArrayList((ArrayList)read_dictionary.get("answers"));
            ListIterator iterator = answers.listIterator();
            while (iterator.hasNext()){
                HashMap map = ((HashMap)iterator.next());
                if (map.get("id").equals(answ_id))
                {
                    return  map;
                }
            }
        }

        return new HashMap();
    }

    public static ArrayList getQuestionListArray(Context context) {
        HashMap read_dictionary = new HashMap(FileUtil.readJsonFromFileDataJson(context));

        ArrayList sections_array = new ArrayList();

        if(read_dictionary.size()>0) {
            ArrayList answers_array = new ArrayList((ArrayList) read_dictionary.get("answers"));
            int group = -1;


            ArrayList items_array;
            HashMap section_item;

            sections_array = new ArrayList();
            items_array = new ArrayList();

            int section_index = 0;
            int item_index = 0;

            ListIterator iterator = answers_array.listIterator();

            while (iterator.hasNext()) {
                HashMap answer_item = new HashMap((HashMap) iterator.next());

                if (!answer_item.get("id").equals("999")) {
//                if (group != ((HashMap) ((HashMap) ((HashMap) answer_item.get("answer")).get("response_dict")).get("question")).get("group")) {
//                    group = (int) ((HashMap) ((HashMap) ((HashMap) answer_item.get("answer")).get("response_dict")).get("question")).get("group");
//
//                    String group_name = "";
//
//                    switch (group) {
//                        case 1:
//                            group_name = "Your Current Loans Recap";
//                            break;
//
//                        case 2:
//                            group_name = "Employment Information";
//                            break;
//
//                        case 3:
//                            group_name = "Income Information";
//                            break;
//
//                        default:
//                            break;
//                    }
//
//                    items_array = new ArrayList();
//
//                    HashMap temp_map = new HashMap();
//
//                    temp_map.put("title", group_name);
//                    temp_map.put("items", items_array);
//
//                    sections_array.add(temp_map);
//                }
//
//                items_array.add(new HashMap(answer_item));

                    sections_array.add(answer_item);
                }
            }
        }
        return sections_array;
    }


    public static HashMap getFinalAnswerDictionary(Context context) {
        HashMap read_dictionary = new HashMap(FileUtil.readJsonFromFileDataJson(context));
        HashMap fin_dictionary  = new HashMap();

        if(!read_dictionary.isEmpty()){
            ArrayList list_dictoinary = new ArrayList((ArrayList)read_dictionary.get("list"));
            //[list_dictoinary addObject:@"999"];
            fin_dictionary.put("list", list_dictoinary);

            ArrayList answers = new ArrayList((ArrayList)read_dictionary.get("answers"));
            HashMap fin_answers = new HashMap();

            for (int i = 0; i < answers.size(); i++) {
                if(((HashMap)answers.get(i)).get("id").equals("999") ){
                    fin_answers.put("999","");
                    fin_dictionary.put("answers",fin_answers);
                }else{
                    String key = ((HashMap)answers.get(i)).get("id").toString();

                    HashMap temp_map = new HashMap();

                    temp_map.put("answer", ((HashMap)((HashMap) answers.get(i)).get("answer")).get("answer"));

                    fin_answers.put(key, temp_map);

                    temp_map = new HashMap();

                    temp_map.put("answer", ((HashMap) answers.get(i)).get("answer"));

                    fin_dictionary.put(key, temp_map);
                }
            }
        }

        return fin_dictionary;
    }

    public static void writeTokenToFile(Context context, String token, String email, String password) {
        HashMap map = new HashMap(FileUtil.readDictFromTokenFile(context));
        map.put("token", token);
        map.put("email", email);
        map.put("password", password);

        FileUtil.writeFile(TOKEN, context, map);
    }

    public static void writeRegisterMessageToFile(Context context, String message) {
        HashMap map = new HashMap(FileUtil.readDictFromTokenFile(context));
        map.put("register_message_key", message);

        FileUtil.writeFile(TOKEN, context, map);
    }

    public static String readRegisterMessageFromTokenFile(Context context) {
        HashMap map = new HashMap(FileUtil.readDictFromTokenFile(context));

        return (String) map.get("register_message_key");
    }

    public static void writeAuthQuestionToFile(Context context, Boolean value) {
        Object obj = FileUtil.readDictFromTokenFile(context);
        HashMap map = new HashMap();

        if (obj != null){
            map =  FileUtil.readDictFromTokenFile(context);
        }

        map.put("auth_question_key", (value ? "YES" : "NO"));

        FileUtil.writeFile(TOKEN, context, map);
    }

    public static Boolean readAuthQuestionFromFile(Context context) {
        Object o = FileUtil.readDictFromTokenFile(context);

        HashMap map = new HashMap();

        if(o != null){
            map = (HashMap) o;
            return map.get("auth_question_key").equals("YES") ? true : false;
        }

        return false;
    }

    public static void clearTokenToFile(Context context) {

        HashMap map = new HashMap(FileUtil.readDictFromTokenFile(context));
        map.put("token", "");
//        HashMap map = new HashMap();

        FileUtil.writeFile(TOKEN, context, map);
    }

    public static HashMap readDictFromTokenFile(Context context) {


        return (HashMap) FileUtil.readFile(TOKEN, context);
    }

    public static String readTokenFromFile(Context context) {

        Object token = FileUtil.readFile(TOKEN, context);
        HashMap map = (token == null)? new HashMap() : (HashMap) token;

        //            return [NSString stringWithString:[(NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:tokenData] valueForKey:item]];
        return map.keySet().contains("token") ? (String) map.get("token") : "";
    }

////amount = < question 105 or 104>,
////interest_rate = <question 122 if yes, else method 6>,
////payment =  <question 107>

    public static void writeAmountJsonToFileCalcCalendarJson(Context context, String amount) {
        HashMap map = new HashMap(FileUtil.readDictFromTokenFile(context));
        map.put("amount", amount);

        FileUtil.writeFile(CALC_CALENDAR, context, map);
    }


    public static void writeInterestRateJsonToFileCalcCalendarJson(Context context, HashMap interest_rate_map) {
        HashMap map = new HashMap(FileUtil.readJsonFromFileCalcCalendarJson(context));
//        HashMap interest_rate_read_map = new HashMap((HashMap) map.get("interest_rate"));
//
//        interest_rate_read_map.putAll(interest_rate_map);
//
//        map.put("interest_rate", interest_rate_read_map);

        map.put("interest_rate", interest_rate_map);

        FileUtil.writeFile(CALC_CALENDAR, context, map);
    }

//    + (void) writeInterestRateJsonToFileCalcCalendarJson:(NSDictionary *) interest_rate_dict
//    {
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[self readJsonFromFileCalcCalendarJson]];
//        NSMutableDictionary *interest_rate_read_dict = [[NSMutableDictionary alloc] initWithDictionary:[dict valueForKey:@"interest_rate"]];
//        NSArray *interest_rate_keys_arr = [interest_rate_dict allKeys];
//        NSLog(@"writeInterestRateJsonToFileCalcCalendarJson: %@", dict);
//
//        for (NSString* item_key in interest_rate_keys_arr) {
//        [interest_rate_read_dict setValue:[interest_rate_dict valueForKey:item_key] forKey:item_key];
//    }
//
//        [dict setValue:interest_rate_read_dict forKey:@"interest_rate"];
//
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//
//        NSData *urlData = [NSKeyedArchiver archivedDataWithRootObject:dict];
//
//        if (urlData) {
//            NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"calccalendar.json"];
//            [urlData writeToFile:filePath atomically:YES];
//        }
//    }


    public static void writePaymentJsonToFileCalcCalendarJson(Context context, String payment) {
        HashMap map = new HashMap(FileUtil.readJsonFromFileCalcCalendarJson(context));

        map.put("payment", payment);

        FileUtil.writeFile(CALC_CALENDAR, context, map);
    }

    public static HashMap readJsonFromFileCalcCalendarJson(Context context) {

        HashMap map = new HashMap();
        Object obj = FileUtil.readFile(CALC_CALENDAR, context);
        if(obj != null){
            map =new HashMap((HashMap)obj);
        }

        return map;
    }

    public static void clearJsonToFileCalcCalendarJson(Context context) {

        FileUtil.writeFile(CALC_CALENDAR, context, new HashMap());
    }

    public static void writePotentialForgivenessProgramJsonToFileCalcCalendarJson(Context context, String potential_forgiveness_program) {
        HashMap dict = new HashMap(FileUtil.readJsonFromFileCalcCalendarJson(context));
        dict.put("potential_forgiveness_program", potential_forgiveness_program);

        FileUtil.writeFile(CALC_CALENDAR, context, dict);
    }

    public static void writeActiveServiceFileCalcCalendarJson(Context context, HashMap active_service) {
        HashMap dict = new HashMap(FileUtil.readJsonFromFileCalcCalendarJson(context));
        dict.put("active_service", active_service);

        FileUtil.writeFile(CALC_CALENDAR, context, dict);
    }

    public static Boolean isMarried(Context context) {
        ArrayList total_arr = FileUtil.getQuestionListArray(context);

        for (int i = 0; i < total_arr.size(); i++) {
            HashMap dict = (HashMap) total_arr.get(i);
            if (Integer.parseInt((String) dict.get("id")) == 301) {
                if (((HashMap) dict.get("answer")).get("answer").equals("yes")) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        return false;
    }
}
