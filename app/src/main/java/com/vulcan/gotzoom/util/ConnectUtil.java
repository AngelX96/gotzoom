package com.vulcan.gotzoom.util;

import android.content.Context;

import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.volley.Request;
import com.vulcan.gotzoom.volley.Response;
import com.vulcan.gotzoom.volley.VolleyError;
import com.vulcan.gotzoom.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 4/20/16.
 */
public class ConnectUtil {
    public static String url = "";

//    public static String URL_FIRST_QUESTION              = "http://gz.test.digitalepoch.com/api/questionnaires/get-question.json?api-key=%s";
//    public static String URL_ID_QUESTION                 = "http://gz.test.digitalepoch.com/api/questionnaires/get-question/%s.json?api-key=%s";
//    public static String URL_CALC_MONTH                  = "http://gz.test.digitalepoch.com/api/questionnaires/calculate-months.json?api-key=%s";
//    public static String URL_INTEREST_RATE               = "http://gz.test.digitalepoch.com/api/questionnaires/get-defaultInterest-rate/%s.json?api-key=%s";
//    public static String URL_REGISTER                    = "http://gz.test.digitalepoch.com/api/users/register.json?api-key=%s";
//    public static String URL_LOGIN                       = "http://gz.test.digitalepoch.com/api/users/token.json?api-key=%s";
//    public static String URL_FSA_LOANS                   = "http://gz.test.digitalepoch.com/api/questionnaires/get-loans.json?api-key=%s";
//    public static String URL_CALCULATE_RESULTS           = "http://gz.test.digitalepoch.com/api/questionnaires/calculate-results.json?api-key=%s";
//    public static String URL_UPDATE_PERSONAL_INFORMATION = "http://gz.test.digitalepoch.com/api/users/update-personal-information.json?api-key=%s&token=%s";
//    public static String URL_PERSONAL_INFORMATION        = "http://gz.test.digitalepoch.com/api/users/get-data/personal-information.json?api-key=%s&token=%s";
//    public static String URL_USERS_GET_ACTIVE_SERVICE    = "http://gz.test.digitalepoch.com/api/users/get-active-service.json?api-key=%s&token=%s";
//    public static String URL_GET_SERVICES                = "http://gz.test.digitalepoch.com/api/services/get.json?api-key=%s";
//    public static String URL_PURCHASE_SERVICES           = "http://gz.test.digitalepoch.com/api/services/purchase.json?api-key=%s&token=%s";
//    public static String URL_GET_DOWNLOBLE_FORMS         = "http://gz.test.digitalepoch.com/api/downloadable-forms/get.json?api-key=%s&token=%s";
//    public static String URL_DOWNLOAD_DOWNLOBLE_FORMS    = "http://gz.test.digitalepoch.com/api/downloadable-forms/download/%s.json?api-key=%s&token=%s";

    public static String URL_FIRST_QUESTION              = "https://gotzoom.com/api/questionnaires/get-question.json?api-key=%s";
    public static String URL_ID_QUESTION                 = "https://gotzoom.com/api/questionnaires/get-question/%s.json?api-key=%s";
    public static String URL_CALC_MONTH                  = "https://gotzoom.com/api/questionnaires/calculate-months.json?api-key=%s";
    public static String URL_INTEREST_RATE               = "https://gotzoom.com/api/questionnaires/get-defaultInterest-rate/%s.json?api-key=%s";
    public static String URL_REGISTER                    = "https://gotzoom.com/api/users/register.json?api-key=%s";
    public static String URL_LOGIN                       = "https://gotzoom.com/api/users/token.json?api-key=%s";
    public static String URL_FSA_LOANS                   = "https://gotzoom.com/api/questionnaires/get-loans.json?api-key=%s";
    public static String URL_CALCULATE_RESULTS           = "https://gotzoom.com/api/questionnaires/calculate-results.json?api-key=%s";
    public static String URL_UPDATE_PERSONAL_INFORMATION = "https://gotzoom.com/api/users/update-personal-information.json?api-key=%s&token=%s";
    public static String URL_PERSONAL_INFORMATION        = "https://gotzoom.com/api/users/get-data/personal-information.json?api-key=%s&token=%s";
    public static String URL_USERS_GET_ACTIVE_SERVICE    = "https://gotzoom.com/api/users/get-active-service.json?api-key=%s&token=%s";
    public static String URL_GET_SERVICES                = "https://gotzoom.com/api/services/get.json?api-key=%s";
    public static String URL_PURCHASE_SERVICES           = "https://gotzoom.com/api/services/purchase.json?api-key=%s&token=%s";
    public static String URL_GET_DOWNLOBLE_FORMS         = "https://gotzoom.com/api/downloadable-forms/get.json?api-key=%s&token=%s";
    public static String URL_DOWNLOAD_DOWNLOBLE_FORMS    = "https://gotzoom.com/api/downloadable-forms/download/%s.json?api-key=%s&token=%s";

    public static String getApiKey()
    {
        return "0feyEwzRmvl5E35rqBeX";
    }

    public static void loadQuestionFirst(Context c, final ConnectUtilListener listener)
    {
        String url = String.format(URL_FIRST_QUESTION, ConnectUtil.getApiKey());

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    public static void loadQuestionID(Context c, final ConnectUtilListener listener, String id)
    {
        String url = String.format(URL_ID_QUESTION, id, ConnectUtil.getApiKey());

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    public static void loadPersonaByToken(Context c, final ConnectUtilListener listener, String token)
    {
        String url = String.format(URL_PERSONAL_INFORMATION, ConnectUtil.getApiKey(), token);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    public static void loadFinalProgramsByToken(Context c, final ConnectUtilListener listener)
    {
        String url = String.format(URL_CALCULATE_RESULTS, ConnectUtil.getApiKey());

        HashMap dict = FileUtil.getFinalAnswerDictionary(c);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, new JSONObject(dict), new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    ////Calculate months for question 108 | post
    ////url: http://gz.test.digitalepoch.com/api/questionnaires/calculate-results.json
    ////?api-key=<key>
    ////post data:
    ////amount = < question 105 or 104>,
    ////interest_rate = <question 122 if yes, else method 6>,
    ////payment =  <question 107>
    public static void loadCalculateMonth(Context c, final ConnectUtilListener listener, String id)
    {
        String url = String.format(URL_CALC_MONTH, ConnectUtil.getApiKey());

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    ////Get default interest rate | get
    ////url: http://gz.test.digitalepoch.com/api/questionnaires/get-default-interest-rate.json
    ////?api-key=<key>&ppl=<question 106 if yes then 1, default 0>

    public static void loadGetDefaultInterestRate(Context c, final ConnectUtilListener listener, String check, HashMap map)
    {
        String url = String.format(URL_INTEREST_RATE, check, ConnectUtil.getApiKey());

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        //        if ([responseDict objectForKey:@"success"]) {
//            //dispatch_sync(dispatch_get_main_queue(), ^{
//
//            [self sendCalculateMonthsByParams:[NSDictionary dictionaryWithObjectsAndKeys:
//            [dict valueForKey:@"payment"],@"payment",
//            [[responseDict objectForKey:@"result"] stringValue] ,@"interest_rate",
//            [dict valueForKey:@"amount"],@"amount",
//                    nil]
//            completionHandler:completionHandler];
//
//            //});
//        }
                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }
    ////register | post
    //url: http://gz.test.digitalepoch.com/api/users/register.json?api-key=<key>&token<token>
    //post data:
    //email, password
    public static void registerUserEmail(Context c, final ConnectUtilListener listener, String email, String password)
    {
        String url = String.format(URL_REGISTER, ConnectUtil.getApiKey());

        JSONObject json = new JSONObject();

        try {
            json.put("email",email);
            json.put("password",password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        //        if ([[responseDict objectForKey:@"success"] intValue] == 1) {
                        //            [FileUtil writeRegisterMessageToFile:@"Register success complete. Please check your email for confirm registration."];
                        //        }else{
                        //            [FileUtil writeRegisterMessageToFile:[[[responseDict objectForKey:@"errors"]  objectForKey:@"email"] objectForKey:@"_isUnique"]];
                        //        }

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    public static void loginUserEmail(final Context c, final ConnectUtilListener listener, String email, String password) {
        String url = String.format(URL_LOGIN, ConnectUtil.getApiKey());

        JSONObject json = new JSONObject();

        try {
            json.put("email", email);
            json.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Context f_context = c;
        final String f_email = email;
        final String f_password = password;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        HashMap responseDict = ParseConnectionMessages.parseQuestionFirst(response.toString());
                        if ((boolean) responseDict.get("success")) {

                            FileUtil.writeTokenToFile(f_context, ((HashMap) responseDict.get("data")).get("token").toString(), f_email, f_password);
                        }
                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    public static void calculateMonths(Context c, final ConnectUtilListener listener, String token) {
        HashMap dict = FileUtil.readJsonFromFileCalcCalendarJson(c);

        Boolean isFind112 = false;

        HashMap interest_rate_dict = (HashMap) dict.get("interest_rate");
        ArrayList interest_rate_key_arr = new ArrayList();
        interest_rate_key_arr.addAll(interest_rate_dict.keySet());

        String interest_rate = null;
        String item = "";
        for (int i = 0; i < interest_rate_key_arr.size(); i++) {
            item = interest_rate_key_arr.get(i).toString();
            if (item.equals("112")) {
                if (((HashMap) dict.get(item)).get("bool").equals("yes")) {
                    isFind112 = true;
                    interest_rate = ((HashMap) dict.get(item)).get("input").toString();
                }
            }
        }

        if (!isFind112) {
            HashMap map = new HashMap();
            map.put("payment", dict.get("payment"));
            map.put("interest_rate", "");
            map.put("amount", dict.get("amount"));

            //(Context c, final ConnectUtilListener listener, String check, HashMap map)
            String st_check = ((HashMap) ((HashMap) dict.get("interest_rate")).get(106)).get("bool").equals("yes") ? "1" : "0";

            loadGetDefaultInterestRate(c, listener, st_check, map);
        } else {
            HashMap map = new HashMap();
            map.put("payment", dict.get("payment"));
            map.put("interest_rate", interest_rate);
            map.put("amount", dict.get("amount"));

            sendCalculateMonthsByParams(c, listener, map);
        }
    }

    public static void sendCalculateMonthsByParams(Context c, final ConnectUtilListener listener, HashMap map)
    {
        String url = String.format(URL_CALC_MONTH, ConnectUtil.getApiKey());

        JSONObject json = new JSONObject();

    //    try {
    //        json.put("email",username);
    //        json.put("password",password);
    //    } catch (JSONException e) {
    //        e.printStackTrace();
    //    }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    ////get FSA loans | post
    ////url : http://gz.test.digitalepoch.com/api/questionnaires/get-loans.json?api-key=<key>
    ////post data:
    ////username, password
    ////response
    ////ok:{
    ////    "success": true,
    ////    "result": 7436}
    ////wrong:{
    ////    "success": false,
    ////    "result": "<error>"}
    public static void sendLoanFsaByUsername(Context c, final ConnectUtilListener listener, String username, String password)
    {
        String url = String.format(URL_FSA_LOANS, ConnectUtil.getApiKey());

        JSONObject json = new JSONObject();

        try {
            json.put("email",username);
            json.put("password",password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    ////plan_name
    ////initial_monthly_payment
    ////loan_period
    ////current_payment
    ////currrent_term
    public static void sendProfileToken(Context c, final ConnectUtilListener listener, String token, HashMap map)
    {
        String url = String.format(URL_UPDATE_PERSONAL_INFORMATION, ConnectUtil.getApiKey(), token);
    //        NSData *postData = [NSJSONSerialization dataWithJSONObject:map options:0 error:&error];
        try {
            // code that may throw exception

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, url, new JSONObject(map), new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            listener.successResponse(response.toString());

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            listener.failResponse(error);

                        }
                    });

            SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
        }catch (Error error){

        }

    }

    public static void getActiveServiceByToken(Context c, final ConnectUtilListener listener, String token)
    {
        String url = String.format(URL_USERS_GET_ACTIVE_SERVICE, ConnectUtil.getApiKey(), token);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    public static void getServicesCompletionHandler(Context c, final ConnectUtilListener listener)
    {
        String url = String.format(URL_GET_SERVICES, ConnectUtil.getApiKey());

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    /////////
    //purchase | post
    //url: http://gz.test.digitalepoch.com/api/services/purchase.json?api-key=<key>&token<token>
    //post data:
    //service_slug,
    //cardNumber,
    //cardExpiry,
    //cardCVC
    public static void purchaeServiceSlug(Context c, final ConnectUtilListener listener, String token, String service_slug, String card_number, String card_expiry, String card_cvc)
    {
        String url = String.format(URL_PURCHASE_SERVICES, ConnectUtil.getApiKey(), token);

        HashMap map = new HashMap();

        map.put("service_slug", service_slug);
        map.put("cardNumber", card_number);
        map.put("cardExpiry", card_expiry);
        map.put("cardCVC", card_cvc);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, new JSONObject(map), new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }

    public static String getURLDownloableFullUrl(String token, String slug){
        String path = String.format(URL_DOWNLOAD_DOWNLOBLE_FORMS, slug,  ConnectUtil.getApiKey(), token);

        return path;
    }

    public static void getDownloadableForms(Context c, final ConnectUtilListener listener)
    {
        String url = String.format(URL_GET_DOWNLOBLE_FORMS, ConnectUtil.getApiKey(), FileUtil.readTokenFromFile(c));

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        listener.successResponse(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        listener.failResponse(error);

                    }
                });

        SingletoneConnectUtil.getInstance().sendRequest(c, jsObjRequest);
    }
}
