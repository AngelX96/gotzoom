package com.vulcan.gotzoom.util;

import com.vulcan.gotzoom.volley.VolleyError;

/**
 * Created by andiil on 4/20/16.
 */
public interface ConnectUtilListener{
    public void successResponse(String str);

    public void failResponse(VolleyError error);
}
