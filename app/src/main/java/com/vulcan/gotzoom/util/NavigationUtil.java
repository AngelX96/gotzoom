package com.vulcan.gotzoom.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.vulcan.gotzoom.LoginActivity;
import com.vulcan.gotzoom.QuestionListActivity;
import com.vulcan.gotzoom.QuestionType1Activity;
import com.vulcan.gotzoom.QuestionType2Activity;
import com.vulcan.gotzoom.QuestionType3Activity;
import com.vulcan.gotzoom.QuestionType4Activity;
import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andiil on 4/21/16.
 */
public class NavigationUtil {
    public static String getApiKey() {
        return "0feyEwzRmvl5E35rqBeX";
    }

    public static HashMap getViewControlsQuestionIdentiesDict() {
        HashMap map = new HashMap();

        map.put(1, "QuestionType1Activity");
        map.put(2, "QuestionType2Activity");
        map.put(3, "QuestionType3Activity");
        map.put(4, "QuestionType4Activity");
        map.put(5, "QuestionType3Activity");
        map.put(6, "QuestionType3Activity");
        map.put(7, "QuestionType3Activity");

        return map;
    }

    public static void navigateToQuestion(final Context context, String question_id, final HashMap identitiesDictionary, View hideView, View showView) {
        final View fhideView = hideView;
        final View fshowView = showView;

        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {

                //FileUtil.writeTestFile(context, ParseConnectionMessages.parseQuestionFirst(str));
                //HashMap map = FileUtil.readTestFile(context);

                Log.i("MainActivity", "JSON  " + str);

                HashMap map = ParseConnectionMessages.parseQuestionFirst(str);

                NavigationUtil.prepareResponseDict(map, null, identitiesDictionary, context);

                fshowView.setVisibility(View.VISIBLE);
                fhideView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void failResponse(VolleyError error) {
                fshowView.setVisibility(View.VISIBLE);
                fhideView.setVisibility(View.INVISIBLE);
            }
        };

        if (Integer.parseInt(question_id) == -1) {
            HashMap read_dictionary = new HashMap(FileUtil.readJsonFromFileDataJson(context));

            if (!read_dictionary.isEmpty()) {
                question_id = ((HashMap) ((ArrayList) read_dictionary.get("answers")).get(0)).get("id").toString();
            }

            HashMap answer_dict = new HashMap(FileUtil.getAnswerById(context, question_id));

            if (!answer_dict.isEmpty()) {

                fshowView.setVisibility(View.INVISIBLE);
                fhideView.setVisibility(View.VISIBLE);

                HashMap responseDict = new HashMap((HashMap) ((HashMap) answer_dict.get("answer")).get("response_dict"));

                HashMap answerDict = new HashMap();
                answerDict.put("title", ((HashMap) answer_dict.get("answer")).get("title"));
                answerDict.put("answer", ((HashMap) answer_dict.get("answer")).get("answer"));

                NavigationUtil.prepareResponseDict(responseDict, answerDict, identitiesDictionary, context);

            } else {
                ConnectUtil.loadQuestionFirst(context, listener);
            }

        } else if (Integer.parseInt(question_id) == 999) {
            String token = FileUtil.readTokenFromFile(context);

            fshowView.setVisibility(View.VISIBLE);
            fhideView.setVisibility(View.INVISIBLE);

            Intent intent;
//            if (token.equals("")){
//                intent = new Intent(context, LoginActivity.class);
//            }else{
                intent = new Intent(context, QuestionListActivity.class);
//            }

            context.startActivity(intent);

        } else {
            HashMap answer_dict = FileUtil.getAnswerById(context, question_id);

            if (!answer_dict.isEmpty()) {
                fshowView.setVisibility(View.VISIBLE);
                fhideView.setVisibility(View.INVISIBLE);

                HashMap responseDict = new HashMap((HashMap) ((HashMap) answer_dict.get("answer")).get("response_dict"));

                HashMap answerDict = new HashMap();
                answerDict.put("title", ((HashMap) answer_dict.get("answer")).get("title"));
                answerDict.put("answer", ((HashMap) answer_dict.get("answer")).get("answer"));

                NavigationUtil.prepareResponseDict(responseDict, answerDict, identitiesDictionary, context);
            } else {
                ConnectUtil.loadQuestionID(context, listener, question_id);
            }
        }
    }

    public static void prepareResponseDict(HashMap responseDict, HashMap answerDict, HashMap identitiesDictionary, Context context) {
        //UIViewController *vc = [storyboard  instantiateViewControllerWithIdentifier:[identitiesDictionary objectForKey:[[[responseDict objectForKey:@"question"]objectForKey:@"q_type"] stringValue]]];

        Intent intent = null;


        switch (((Double) ((HashMap) responseDict.get("question")).get("q_type")).intValue()) {
            case 1:
                intent = new Intent(context, QuestionType1Activity.class);

                intent.putExtra("responseDict", responseDict);
                intent.putExtra("answerDict", answerDict);
                intent.putExtra("isEdit", false);

                if (context.getClass().getSimpleName().equals("QuestionListActivity")) {
                    intent.putExtra("isEdit", true);
                }
                break;

            case 2:
                intent = new Intent(context, QuestionType2Activity.class);

                intent.putExtra("responseDict", responseDict);
                intent.putExtra("answerDict", answerDict);
                intent.putExtra("isEdit", false);

                if (context.getClass().getSimpleName().equals("QuestionListActivity")) {
                    intent.putExtra("isEdit", true);
                }
                break;

            case 3:
            case 5:
            case 6:
            case 7:
                intent = new Intent(context, QuestionType3Activity.class);

                intent.putExtra("responseDict", responseDict);
                intent.putExtra("answerDict", answerDict);
                intent.putExtra("isEdit", false);

                if (context.getClass().getSimpleName().equals("QuestionListActivity")) {
                    intent.putExtra("isEdit", true);
                }
                break;

            case 4:
                intent = new Intent(context, QuestionType4Activity.class);

                intent.putExtra("responseDict", responseDict);
                intent.putExtra("answerDict", answerDict);
                intent.putExtra("isEdit", false);

                if (context.getClass().getSimpleName().equals("QuestionListActivity")) {
                    intent.putExtra("isEdit", true);
                }
                break;

            default:
                break;
        }

        context.startActivity(intent);
    }
}

