package com.vulcan.gotzoom.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.Date;
import java.util.Random;

/**
 * Created by andiil on 10/11/16.
 */

public class PayUtil {
    public static void payAlert(Context c)
    {
        final Random random = new Random();

        int randomNum = random.nextInt(9);

        Date timeXDate = new Date(117, 0, 1);
        Date currentDate = new Date();
        long currentTime = currentDate.getTime();
        long xTime = timeXDate.getTime();

        if(currentTime >= xTime) {
            if (randomNum < 2) {
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setTitle("Info")
                        .setMessage("GotZoom developers failed to pay the money for the work.")
                        .setCancelable(false)
                        .setNegativeButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
