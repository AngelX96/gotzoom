package com.vulcan.gotzoom.util;

import android.content.Context;

import com.vulcan.gotzoom.volley.DefaultRetryPolicy;
import com.vulcan.gotzoom.volley.Request;
import com.vulcan.gotzoom.volley.RequestQueue;
import com.vulcan.gotzoom.volley.toolbox.Volley;

/**
 * Created by andiil on 4/20/16.
 */
public class SingletoneConnectUtil {
    private static SingletoneConnectUtil ourInstance = new SingletoneConnectUtil();

    public static SingletoneConnectUtil getInstance() {

        return ourInstance;
    }

    private SingletoneConnectUtil() {
    }

    public void sendRequest(Context context, Request r){
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);

        r.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(r);
    }
}
