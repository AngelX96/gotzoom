package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.vulcan.gotzoom.adapters.ServicesListViewAdapter;
import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.ConnectUtilListener;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;

public class ServicesActivity extends AppCompatActivity {

    public enum GZServiceListButtonState {
        GZServiceListButtonStateSelectEnable,
        GZServiceListButtonStateSelectDisable,
        GZServiceListButtonStateContinueEnable,
        GZServiceListButtonStateUpgradeEnable,
        GZServiceListButtonStateUpgradeDisable }

    private ArrayList contentArray;
    private ArrayList services_list;
    private HashMap activeServiceDict;

    private ServicesListViewAdapter mListAdapterS;

    private ListView listView;

    final String name_green_cell             = "name_green_cell";
    final String name_description_green_cell = "name_description_green_cell";
    final String previouses_cell             = "previouses_cell";
    final String item_cell                   = "item_cell";
    final String button_cell                 = "button_cell";
    final String button_free_cell            = "button_free_cell";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        contentArray = new ArrayList();

        this.getActiveService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (contentArray.size() > 0) {

        } else {
            findViewById(R.id.listView).setVisibility(View.INVISIBLE);
            findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
        }
    }

    private void  getActiveService(){
        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {
                HashMap responseDict = ParseConnectionMessages.parseQuestionFirst(str);


                if ((Boolean) responseDict.get("success")) {
                    HashMap dict = (HashMap) responseDict.get("data");
                    activeServiceDict = (HashMap) responseDict.get("data");

                } else {
                    activeServiceDict = new HashMap();
                }

                ServicesActivity.this.getAllServices();
            }

            @Override
            public void failResponse(VolleyError error) {

            }
        };
        if(FileUtil.readAuthQuestionFromFile(this)){
            ConnectUtil.getActiveServiceByToken(this, listener, FileUtil.readTokenFromFile(this));
        } else {
            activeServiceDict = new HashMap();
            getAllServices();
        }

    }

    private void  getAllServices(){
        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {
                HashMap responseDict = ParseConnectionMessages.parseQuestionFirst(str);

                Boolean isFirstUnavaliable = false;

                services_list = new ArrayList();
                ArrayList service_sub_items_list;
                HashMap service_item;
                //HashMap service_sub_item;
                HashMap item;

                String previouses_service = null;

                services_list.addAll((ArrayList) responseDict.get("services"));

                services_list = setButtonStatusArray(services_list, activeServiceDict);

                for (int i = 0; i < services_list.size(); i++) {
                    service_item = ((HashMap) services_list.get(i));

                    switch (((Double) service_item.get("id")).intValue()) {
                        case 1:
                            item = new HashMap();
                            item.put("item_type", name_green_cell);
                            item.put("name", service_item.get("name"));
                            contentArray.add(item);

                            service_sub_items_list = (ArrayList) service_item.get("features");

                            for (int j = 0; j < service_sub_items_list.size(); j++) {

                                item = new HashMap();
                                item.put("item_type", item_cell);
                                item.put("text", service_sub_items_list.get(j));
                                contentArray.add(item);

                            }

                            item = new HashMap();
                            item.put("item_type", button_free_cell);
                            item.put("name", service_item.get("name"));
                            item.put("slug", service_item.get("slug"));
                            item.put("id", service_item.get("id"));
                            contentArray.add(item);
                            break;
                        case 2:
                        case 3:
                        case 4:
                            item = new HashMap();
                            if (service_item.get("name_description").equals("")) {
                                item.put("item_type", name_green_cell);
                                item.put("name", service_item.get("name"));
                            } else {
                                item.put("item_type", name_description_green_cell);
                                item.put("name", service_item.get("name"));
                                item.put("name_description", service_item.get("name_description"));
                            }

                            contentArray.add(item);

                            switch (((Double) service_item.get("id")).intValue()) {
                                case 2:
                                    previouses_service = "FREE +";
                                    break;
                                case 3:
                                case 4:
                                    previouses_service = "DIY +";
                                    break;

                                default:
                                    break;
                            }

                            item = new HashMap();
                            item.put("item_type", previouses_cell);
                            item.put("name", previouses_service);
                            contentArray.add(item);

                            service_sub_items_list = (ArrayList) service_item.get("features");

                            for (int j = 0; j < service_sub_items_list.size(); j++) {

                                item = new HashMap();
                                item.put("item_type", item_cell);
                                item.put("text", service_sub_items_list.get(j));
                                contentArray.add(item);

                            }

                            item = new HashMap();
                            item.put("item_type", button_cell);
                            item.put("status", service_item.get("status"));
                            item.put("name", service_item.get("name"));
                            item.put("slug", service_item.get("slug"));
                            item.put("id", service_item.get("id"));
                            item.put("description_after_payment", service_item.get("description_after_payment"));
                            item.put("price", service_item.get("price"));
                            item.put("price_month", service_item.get("price_month"));
                            contentArray.add(item);

                            break;
                        default:
                            break;
                    }
                }

                View.OnClickListener buttonPayListner = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //service_detail_ViewController.service_Dictionary = [NSDictionary dictionaryWithDictionary:[service_list_Array objectAtIndex:send_section]];
                        int position = ((View) view.getParent()).getId();

                        Double id = (Double) ((HashMap) contentArray.get(position)).get("id");

                        for (int i = 0; i < services_list.size(); i++) {
                            if (((HashMap) services_list.get(i)).get("id").equals(id)) {
                                position = i;
                                break;
                            }
                        }

                        if (!FileUtil.readAuthQuestionFromFile(getApplicationContext())) {

                            //FileUtil.writeAuthQuestionToFile(getApplicationContext(), (HashMap) services_list.get(position));

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.putExtra("service_Dictionary", (HashMap) services_list.get(position));
                            startActivity(intent);

                        } else {
                            Intent intent = new Intent(getApplicationContext(), ServiceDetailActivity.class);



                            intent.putExtra("service_Dictionary", (HashMap) services_list.get(position));

                            startActivity(intent);
                        }
                    }
                };

                View.OnClickListener buttonContinueListner = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(getApplicationContext(), ProfileDataActivity.class);
                        startActivity(intent);

                    }
                };

                mListAdapterS = new ServicesListViewAdapter(
                        ServicesActivity.this,
                        contentArray,
                        new int[]{
                                R.layout.item_services_button_free,
                                R.layout.item_services_list_button,
                                R.layout.item_services_list_item,
                                R.layout.item_services_previus,
                                R.layout.item_services_name_green,
                                R.layout.item_services_name_description_green
                        },
                        new int[]{
                                R.id.name,
                                R.id.description,
                                R.id.text,
                                R.id.price,
                                R.id.price_month,
                                R.id.button
                        },
                        new int[]{},
                        new View.OnClickListener[]{buttonPayListner, buttonContinueListner},
                        listView);

                listView = (ListView) ServicesActivity.this.findViewById(R.id.listView);

                listView.setAdapter(mListAdapterS);
                listView.setLongClickable(false);
                listView.setClickable(false);

                findViewById(R.id.container_progress_view).setVisibility(View.GONE);
                findViewById(R.id.listView).setVisibility(View.VISIBLE);
            }

            @Override
            public void failResponse(VolleyError error) {

            }
        };

        ConnectUtil.getServicesCompletionHandler(this, listener);
    }


    private ArrayList setButtonStatusArray(ArrayList array, HashMap active_service_dictionary) {
        ArrayList service_Array = new ArrayList();
        HashMap temp_Dictionary;
        int service_id;

        Boolean isMarried = FileUtil.isMarried(ServicesActivity.this);

        for (int i = 0; i < array.size(); i++) {
            HashMap dict = (HashMap) array.get(i);

            service_id = ((Double) dict.get("id")).intValue();

            temp_Dictionary = dict;

            temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectEnable);

            switch (service_id) {
                case 1:
                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectDisable);
                    break;
                case 2:

                    if (active_service_dictionary.keySet().size() != 0) {
                        switch (((Double) active_service_dictionary.get("service_id")).intValue()) {
                            case 2:
                                temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateContinueEnable);
                                break;
                            case 3:
                            case 4:
                                temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectDisable);
                                break;

                            default:
                                break;
                        }
                    }
                    break;
                case 3:
                    if (active_service_dictionary.keySet().size() != 0) {
                        switch (((Double) active_service_dictionary.get("service_id")).intValue()) {
                            case 2:
                                if (isMarried) {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateUpgradeDisable);
                                } else {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateUpgradeEnable);
                                }

                                break;
                            case 3:
                                if (isMarried) {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectDisable);
                                } else {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateContinueEnable);
                                }

                                break;
                            case 4:
                                temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateContinueEnable);

                                break;

                            default:
                                break;
                        }
                    } else {
                        if (isMarried) {
                            temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectDisable);
                        } else {
                            temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectEnable);
                        }
                    }
                    break;
                case 4:
                    if (active_service_dictionary.keySet().size() != 0) {
                        switch (((Double) active_service_dictionary.get("service_id")).intValue()) {
                            case 2:
                                if (!isMarried) {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateUpgradeDisable);
                                } else {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateUpgradeEnable);
                                }

                                break;
                            case 3:
                                if (!isMarried) {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateUpgradeDisable);
                                } else {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateUpgradeEnable);
                                }

                                break;
                            case 4:
                                if (!isMarried) {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectDisable);
                                } else {
                                    temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateContinueEnable);
                                }

                                break;

                            default:
                                break;
                        }
                    } else {
                        if (!isMarried) {
                            temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectDisable);
                        } else {
                            temp_Dictionary.put("status", GZServiceListButtonState.GZServiceListButtonStateSelectEnable);
                        }
                    }
                    break;

                default:
                    break;
            }

            service_Array.add(temp_Dictionary);
        }

        return service_Array;
    }

    public void onClickWebGroupButton(View id) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
