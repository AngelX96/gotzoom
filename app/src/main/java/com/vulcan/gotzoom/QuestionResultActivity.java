package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.vulcan.gotzoom.adapters.QuestionResultViewAdapter;
import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.ConnectUtilListener;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.util.PayUtil;
import com.vulcan.gotzoom.volley.VolleyError;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class QuestionResultActivity extends AppCompatActivity {

    private ArrayList contentArray;

    private QuestionResultViewAdapter mListAdapterS;

    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_result);

        PayUtil.payAlert(this);

        contentArray = new ArrayList();

        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {
                HashMap responseDict = ParseConnectionMessages.parseQuestionFirst(str);
                findViewById(R.id.container_progress_view).setVisibility(View.GONE);

                if ((Boolean) responseDict.get("success")) {
                    //////////////////////

                    FileUtil.writePotentialForgivenessProgramJsonToFileCalcCalendarJson(QuestionResultActivity.this, ((HashMap)responseDict.get("result")).get("potential_forgiveness_program").toString());

                    double count_calculator = (double) ((HashMap) responseDict.get("result")).get("count_calculator");

                    FileUtil.writePotentialForgivenessProgramJsonToFileCalcCalendarJson(QuestionResultActivity.this, (String) ((HashMap) responseDict.get("result")).get("potential_forgiveness_program"));

                    ArrayList available_array;
                    ArrayList unavailable_array;

                    HashMap programs_dict = (HashMap) ((HashMap) responseDict.get("result")).get("programs");

                    ArrayList programs_keys = new ArrayList();
                    programs_keys.addAll(programs_dict.keySet());

                    String programs_dictionary_key = "";

                    HashMap dict;

                    for (int i = 0; i < programs_keys.size(); i++) {
                        programs_dictionary_key = (String) programs_keys.get(i);
                        HashMap programs_dictionary = (HashMap) programs_dict.get(programs_dictionary_key);
                        if(programs_keys.size() > 1) {
                            String description_text = null;

                            if (programs_dictionary_key.equals("joint")) {
                                description_text = "If you continue filing a Joint Tax Return:";
                            } else if(programs_dictionary_key.equals("separetly")) {
                                description_text = "If you will file a Your Tax Return Separtely in the future:";
                            }

                            dict = new HashMap();
                            dict.put("type", "description");
                            dict.put("text", description_text);

                            contentArray.add(dict);
                        }

                        available_array = (ArrayList) programs_dictionary.get("available");

                        if(programs_dictionary.get("unavailable") == null){
                            unavailable_array = null;
                        } else {
                            unavailable_array = (ArrayList) programs_dictionary.get("unavailable");
                        }

                        for (int j = 0; j < available_array.size(); j++) {
                            dict = (HashMap) available_array.get(j);
                            dict.put("type", "available");
                            contentArray.add(dict);
                        }

                        if (unavailable_array != null) {
                            dict = new HashMap();
                            dict.put("type", "description");
                            dict.put("text", ((HashMap) responseDict.get("result")).get("unavailable_message"));

                            contentArray.add(dict);

                            for (int j = 0; j < unavailable_array.size(); j++) {
                                dict = (HashMap) unavailable_array.get(j);
                                dict.put("type", "unavailable");
                                contentArray.add(dict);
                            }
                        }
                    }

                    View.OnClickListener mLibdocListner = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Integer position = view.getId();

                            String content = ((HashMap) ((HashMap) FileUtil.getAnswerById(QuestionResultActivity.this, "108").get("answer")).get("answer")).get("content").toString();
                            String default_content = ((HashMap) ((HashMap) FileUtil.getAnswerById(QuestionResultActivity.this, "108").get("answer")).get("answer")).get("default_content").toString();

                            String value = (content.equals("") || content.equals("0") ? default_content : content);

                            Intent intent = new Intent(QuestionResultActivity.this, QuestionPlanDetailsActivity.class);

                            intent.putExtra("plan_description", ((HashMap) contentArray.get(position)).get("description").toString());
                            intent.putExtra("plan_id", ((HashMap) contentArray.get(position)).get("id").toString());
                            intent.putExtra("plan_name", ((HashMap) contentArray.get(position)).get("name").toString());
                            intent.putExtra("initial_monthly_payment", ((HashMap) contentArray.get(position)).get("initialMonthlyPayment").toString());
                            intent.putExtra("loan_period", ((HashMap) contentArray.get(position)).get("loanPeriod").toString());
                            intent.putExtra("value_107", Double.parseDouble(((HashMap) FileUtil.getAnswerById(QuestionResultActivity.this, "107").get("answer")).get("answer").toString()));
                            intent.putExtra("value_108", Double.parseDouble(value));

                            startActivity(intent);
                        }
                    };

                    mListAdapterS = new QuestionResultViewAdapter(
                            QuestionResultActivity.this,
                            contentArray,
                            new int[]{R.layout.item_question_result_enable, R.layout.item_question_result_disable, R.layout.item_question_result_description},
                            new int[]{R.id.repayment_plan_Label, R.id.f_monthly_payment_Label, R.id.repayment_period_Label, R.id.description_Label},
                            new int[]{},
                            new View.OnClickListener[]{mLibdocListner});

                    listView = (ListView) QuestionResultActivity.this.findViewById(R.id.listView);
                    listView.setAdapter(mListAdapterS);
                    listView.setLongClickable(false);
                    listView.setClickable(true);

                    findViewById(R.id.listView).setVisibility(View.VISIBLE);
                }

                //[_plansTableView reloadData];
            }

            @Override
            public void failResponse(VolleyError error) {

            }
        };

        ConnectUtil.loadFinalProgramsByToken(this, listener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (contentArray.size() > 0) {

        } else {
            findViewById(R.id.listView).setVisibility(View.INVISIBLE);
            findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}
