package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.vulcan.gotzoom.adapters.QuestionType2ListViewAdapter;
import com.vulcan.gotzoom.adapters.QuestionType3ListViewAdapter;
import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.ConnectUtilListener;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.util.NavigationUtil;
import com.vulcan.gotzoom.util.PayUtil;
import com.vulcan.gotzoom.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuestionType3Activity extends AppCompatActivity {
    static String TITLE_KEY = "title";
    static String TYPE_KEY = "type";
    static String VALUE_KEY = "value";
    static String NEXT_ID_KEY = "next_id";
    static String CHILD_KEY = "child";
    static String VALIDATION_KEY = "validation";
    static String CHECK_KEY = "check_key";
    static String CONTENT_KEY = "content";

    static String DESCRIPTION_CHILD_TYPE = "description";
    static String DESCRIPTION_WEB_CHILD_TYPE = "description_web";
    static String INPUT_CHILD_TYPE = "input";
    static String INPUT_EMPTY_CHILD_TYPE = "input_empty";
    static String INPUT_FSA_CHILD_TYPE = "input_fsa";
    static String EMPTY_CHILD_TYPE = "empty";

    static String CHECK_TYPE_CELL = "CheckItemCell";
    static String INPUT_FSA_TYPE_CELL = "InputFsaIdItemCell";
    static String INPUT_TYPE_CELL = "InputItemCell";
    static String DESCRIPTION_TYPE_CELL = "DescriptionItemCell";
    static String DESCRIPTION_WEB_TYPE_CELL = "DescriptionWebItemCell";

    static int VALUE_NO = 0;
    static int VALUE_YES = 1;
    static int VALUE_IDN = 2;

    private String input_type;
    private Intent intent;
    private Boolean _isEdit;
    private Boolean isSuccess;
    private HashMap _answerDict;
    private HashMap responseDict;
    private Integer _Id;
    private Integer _nextId;
    private Integer nextId;
    private int input_cell_index;
    private String answer_value;
    private HashMap response_dict;

    private Double _calc_months;

    private ArrayList contentListArray;

    private ListView listView;

    private QuestionType3ListViewAdapter mListAdapterS;

    private String check_yes_no_idn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_type3);

        PayUtil.payAlert(this);

        input_type = "";
        answer_value = "";

        response_dict = new HashMap();

        isSuccess = false;

        intent = getIntent();

        _isEdit = intent.getBooleanExtra("isEdit", false);

        if (_isEdit) {
            ((Button) findViewById(R.id.buttonNext)).setText("Save");
        }

        contentListArray = new ArrayList();

        _answerDict = (HashMap) intent.getSerializableExtra("answerDict");
        responseDict = (HashMap) intent.getSerializableExtra("responseDict");

        _nextId = -1;
        _Id = Integer.parseInt(((HashMap) responseDict.get("question")).get("id").toString());

        _calc_months = 0.0;

        if (_Id==108) {
            ((TextView) findViewById(R.id.questionLabel)).setText("");

            findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);

            ConnectUtilListener listener = new ConnectUtilListener() {
                @Override
                public void successResponse(String str) {
                    HashMap response = ParseConnectionMessages.parseQuestionFirst(str);

                    if ((Boolean) response.get("success")) {
                        _calc_months = (Double) response.get("result");
                        String calendar_subsstring = String.format("%d months (%.1f years)", ((Double)_calc_months).intValue(), (_calc_months / 12));

                        findViewById(R.id.container_progress_view).setVisibility(View.GONE);

                        String questoin = (String) ((HashMap) responseDict.get("question")).get("q");

                        questoin = questoin.replace("%@", "%s");

                        ((TextView) findViewById(R.id.questionLabel)).setText(String.format(questoin, calendar_subsstring));
                    }
                }

                @Override
                public void failResponse(VolleyError error) {

                }
            };
            ConnectUtil.calculateMonths(this, listener, FileUtil.readTokenFromFile(this));
        } else {
            ((TextView) findViewById(R.id.questionLabel)).setText((String) ((HashMap) responseDict.get("question")).get("q"));
        }

        //[NavigateUtil loadGetDefaultInterestRate:_Id textView:Nil];

        HashMap hashMap = new HashMap();
        HashMap subhashMap = new HashMap();

        if (((HashMap) responseDict.get("question")).get("next").getClass() == HashMap.class) {
            switch (((Double) ((HashMap) responseDict.get("question")).get("q_type")).intValue()) {
                case 3:
                    contentListArray = new ArrayList();

                    hashMap = new HashMap();
                    subhashMap = new HashMap();

                    subhashMap.put(TYPE_KEY, INPUT_FSA_CHILD_TYPE);

                    hashMap.put(TITLE_KEY, "YES");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_YES);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("next")).get("yes"));
                    hashMap.put(CHILD_KEY, subhashMap);

                    contentListArray.add(hashMap);

                    hashMap = new HashMap();
                    subhashMap = new HashMap();

                    subhashMap.put(TYPE_KEY, DESCRIPTION_WEB_CHILD_TYPE);
                    subhashMap.put(DESCRIPTION_WEB_CHILD_TYPE, ((HashMap) responseDict.get("question")).get("no_text"));

                    hashMap.put(TITLE_KEY, "NO");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_NO);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("next")).get("no"));
                    hashMap.put(CHILD_KEY, subhashMap);

                    contentListArray.add(hashMap);

                    break;
                case 5:
                case 7:

                    contentListArray = new ArrayList();

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "YES");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_YES);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("next")).get("yes"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("yes"));

                    contentListArray.add(hashMap);

                    hashMap = new HashMap();


                    hashMap.put(TITLE_KEY, "NO");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_NO);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("next")).get("no"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("no"));

                    contentListArray.add(hashMap);
                    break;
                case 6:

                    contentListArray = new ArrayList();

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "YES");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_YES);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("next")).get("yes"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("yes"));

                    contentListArray.add(hashMap);

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "NO");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_NO);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("next")).get("no"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("no"));

                    contentListArray.add(hashMap);

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "I don't now");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_IDN);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("next")).get("idn"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("idn"));

                    contentListArray.add(hashMap);
                    break;

                default:
                    break;
            }
        } else {

            nextId = ((Double) ((HashMap) responseDict.get("question")).get("next")).intValue();

            switch (((Double) ((HashMap) responseDict.get("question")).get("q_type")).intValue()) {
                case 5:
                case 7:
                    contentListArray = new ArrayList();

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "YES");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_YES);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) responseDict.get("question")).get("next"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("yes"));

                    contentListArray.add(hashMap);

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "NO");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_NO);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) responseDict.get("question")).get("next"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("no"));

                    contentListArray.add(hashMap);
                    break;
                case 6:

                    contentListArray = new ArrayList();

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "YES");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_YES);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) responseDict.get("question")).get("next"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("yes"));

                    contentListArray.add(hashMap);

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "NO");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_NO);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) responseDict.get("question")).get("next"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("no"));

                    contentListArray.add(hashMap);

                    hashMap = new HashMap();

                    hashMap.put(TITLE_KEY, "I don't now");
                    hashMap.put(TYPE_KEY, CHECK_TYPE_CELL);
                    hashMap.put(CHECK_KEY, 0);
                    hashMap.put(VALUE_KEY, VALUE_IDN);
                    hashMap.put(NEXT_ID_KEY, ((HashMap) responseDict.get("question")).get("next"));
                    hashMap.put(CHILD_KEY, ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get("idn"));

                    contentListArray.add(hashMap);
                    break;

                default:
                    break;
            }
        }

        addChildItems();

        if (_answerDict != null) {
            int check_value = -1;
            int index = -1;

            String answer_text = null;
            String content_text = "";

            switch (((Double) ((HashMap) responseDict.get("question")).get("q_type")).intValue()) {
                case 3:
                case 6:
                case 7:
                    answer_text = (String) _answerDict.get("answer");
                    break;
                case 5:
                    answer_text = (String) ((HashMap) _answerDict.get("answer")).get("value");
                    content_text = (String) ((HashMap) _answerDict.get("answer")).get("content");
                    break;
                default:
                    break;
            }

            if(answer_text != null) {
                if (answer_text.equals("yes")) {
                    check_value = VALUE_YES;
                } else if (answer_text.equals("no")) {
                    check_value = VALUE_NO;
                } else if (answer_text.equals("idn")) {
                    check_value = VALUE_IDN;
                }
            }

            ArrayList t_contentListArray = new ArrayList();

            HashMap map = new HashMap();

            for (int i = 0; i < contentListArray.size(); i++) {
                index++;
                map = (HashMap) contentListArray.get(index);
                HashMap t_dict = (HashMap) contentListArray.get(index);
                if (map.get(TYPE_KEY).equals(CHECK_TYPE_CELL)) {
                    if (map.get(VALUE_KEY).equals(check_value)) {
                        t_dict.put(CHECK_KEY, 1);
                    }
                } else if (map.get(TYPE_KEY).equals(INPUT_TYPE_CELL)) {
                    t_dict.put(CONTENT_KEY, content_text);
                }
                t_contentListArray.add(t_dict);
            }

            contentListArray = t_contentListArray;

            ((TextView) findViewById(R.id.questionLabel)).setText((String) _answerDict.get("title"));
        }

        View.OnClickListener mLibdocListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer position = view.getId();

                checkingItemInPath(position);
            }
        };

        mListAdapterS = new QuestionType3ListViewAdapter(
                this,
                contentListArray,
                new int[]{
                        R.layout.item_question_yes_no_idn,
                        R.layout.item_question_fsa,
                        R.layout.item_question_input,
                        R.layout.item_question_description,
                        R.layout.item_question_description_web
                },
                new int[]{
                        R.id.radioButton,
                        R.id.inputTypeLabel,
                        R.id.textView,
                        R.id.editText,
                        R.id.webView
                },
                new int[]{},
                new View.OnClickListener[]{mLibdocListner});

        listView = (ListView) this.findViewById(R.id.listView);
        listView.setAdapter(mListAdapterS);
        listView.setLongClickable(false);
        listView.setClickable(true);
    }

    private void addChildItems() {
        ArrayList t_arr = contentListArray;
        for (int i = 0; i < contentListArray.size(); i++) {
            if (((HashMap) contentListArray.get(i)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)) {

                HashMap dict = null;

                //NSLog(@"TYPE_KEY: %@", [contentListArray objectAtIndex:[indexPath item]]);

                input_cell_index = -1;

                if (((HashMap) ((HashMap) contentListArray.get(i)).get(CHILD_KEY)).get(TYPE_KEY).equals(INPUT_FSA_CHILD_TYPE)) {
                    dict = new HashMap();
                    dict.put(TYPE_KEY, INPUT_FSA_TYPE_CELL);
                } else if (((HashMap) ((HashMap) contentListArray.get(i)).get(CHILD_KEY)).get(TYPE_KEY).equals(DESCRIPTION_WEB_CHILD_TYPE)) {
                    dict = new HashMap();
                    dict.put(TYPE_KEY, DESCRIPTION_WEB_TYPE_CELL);
                    dict.put(DESCRIPTION_WEB_CHILD_TYPE, ((HashMap) ((HashMap) contentListArray.get(i)).get(CHILD_KEY)).get(DESCRIPTION_WEB_CHILD_TYPE));
                } else if (((HashMap) ((HashMap) contentListArray.get(i)).get(CHILD_KEY)).get(TYPE_KEY).equals(DESCRIPTION_CHILD_TYPE)) {
                    dict = new HashMap();
                    dict.put(TYPE_KEY, DESCRIPTION_TYPE_CELL);
                    dict.put(DESCRIPTION_CHILD_TYPE, ((HashMap) ((HashMap) contentListArray.get(i)).get(CHILD_KEY)).get(DESCRIPTION_CHILD_TYPE));
                } else if (((HashMap) ((HashMap) contentListArray.get(i)).get(CHILD_KEY)).get(TYPE_KEY).equals(INPUT_CHILD_TYPE)) {
                    dict = new HashMap();
                    dict.put(TYPE_KEY, INPUT_TYPE_CELL);
                    dict.put(CONTENT_KEY, "");
                    dict.put(VALIDATION_KEY, ((HashMap) ((HashMap) contentListArray.get(i)).get(CHILD_KEY)).get(VALIDATION_KEY));
                }

                if (dict != null) {
                    t_arr.add(i + 1, dict);
                }
            }
        }
        contentListArray = t_arr;
    }

    private void checkingItemInPath(Integer position) {
        if (!((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)) {
            position = position - 1;
        }

        if (((HashMap) contentListArray.get(position)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)) {
            int value_check = (Integer) ((HashMap) contentListArray.get(position)).get(VALUE_KEY);

            if (value_check == VALUE_YES) {
                check_yes_no_idn = "yes";
            } else if (value_check == VALUE_NO) {
                check_yes_no_idn = "no";
            } else if (value_check == VALUE_IDN) {
                check_yes_no_idn = "idn";
            }
        }

        int first_position = listView.getFirstVisiblePosition();
        int last_position = listView.getLastVisiblePosition();

        for (int i = 0; i <= (last_position - first_position); i++) {
            if ((position - first_position) != i) {
                if (((HashMap) contentListArray.get(first_position + i)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)) {
                    HashMap map = (HashMap) contentListArray.get(first_position + i);
                    map.put(CHECK_KEY, 0);
                    contentListArray.set(first_position + i, map);

                    ((RadioButton) listView.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                }
            } else {
                if (((HashMap) contentListArray.get(first_position + i)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)) {
                    HashMap map = (HashMap) contentListArray.get(first_position + i);
                    map.put(CHECK_KEY, 1);
                    contentListArray.set(first_position + i, map);

                    ((RadioButton) listView.getChildAt(i).findViewById(R.id.radioButton)).setChecked(true);
                }
            }
        }

        mListAdapterS.updateContentList(contentListArray);
    }

    private String validateString(String checkString, String type) {

//        String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
//        CharSequence inputStr = phoneNumber;
//        Pattern pattern = Pattern.compile(expression);
//        Matcher matcher = pattern.matcher(inputStr);
//        if(matcher.matches()){
//            isValid = true;
//        }

        if (type.equals("ssn")) { // SSN is an outlet
            String expression = "^\\d{3}[- ]?\\d{2}[- ]?\\d{4}$";
            CharSequence inputStr = checkString;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if (!matcher.matches()) {
                return "Enter social security number in '000-00-0000' format";
            }

        } else if (type.equals("email")) {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(checkString).matches()) {
                return "Enter email 'email@example.com' format";
            }
        } else if (type.equals("number") ||
                type.equals("money") ||
                type.equals("months")) {
            String expression = "^[0-9]*\\.?[0-9]+$";
            CharSequence inputStr = checkString;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if (!matcher.matches()) {
                return "Enter correct number";
            }
        } else if (type.equals("percent")) {
            String expression = "^[0-9]*\\.?[0-9]+$";
            CharSequence inputStr = checkString;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if (!matcher.matches()) {
                return "Enter correct number";
            }
        }

        return null;
    }

    private Boolean isValidating(String check_type, int select_row) {
        if (check_type.equals(INPUT_CHILD_TYPE)) {
            View input_cell = null;

            int first_position = listView.getFirstVisiblePosition();
            int last_position = listView.getLastVisiblePosition();

            for (int i = 0; i <= (last_position - first_position); i++) {
                if ((select_row + 1 - first_position) == i) {
                    input_cell = listView.getChildAt(i);
                }
            }

            int max, min;

            max = -1;
            min = -1;

            String item = null;
            Object[] key_arr = ((HashMap) ((HashMap) ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get(check_yes_no_idn)).get("validation")).keySet().toArray();

            for (int i = 0; i < key_arr.length; i++) {
                item = key_arr[i].toString();

                if (item.equals("max")) {
                    max = ((Double) ((HashMap) ((HashMap) ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get(check_yes_no_idn)).get("validation")).get(item)).intValue();
                } else if (item.equals("min")) {
                    min = ((Double) ((HashMap) ((HashMap) ((HashMap) ((HashMap) responseDict.get("question")).get("check_type")).get(check_yes_no_idn)).get("validation")).get(item)).intValue();
                }
            }

            ((TextView) input_cell.findViewById(R.id.errortextView)).setText("");
            input_cell.findViewById(R.id.errortextView).setVisibility(View.INVISIBLE);

            String validate_content = validateString(((EditText) input_cell.findViewById(R.id.editText)).getText().toString(),
                    ((String) ((HashMap) ((HashMap) ((HashMap) contentListArray.get(select_row)).get(CHILD_KEY)).get("validation")).get("type")));

            String editText = ((EditText) input_cell.findViewById(R.id.editText)).getText().toString();

            if (validate_content == null) {
                Integer editint = Integer.parseInt(editText);

                if (min > -1) {
                    Integer val = Integer.parseInt(((EditText) input_cell.findViewById(R.id.editText)).getText().toString());
                    if (min <= val && max >= val) {

                    } else {
                        ((EditText) input_cell.findViewById(R.id.editText)).setText("");
                        ((TextView) input_cell.findViewById(R.id.errortextView)).setText((String.format("%d - %d", min, max)));
                        input_cell.findViewById(R.id.errortextView).setVisibility(View.VISIBLE);
                        return false;
                    }
                } else if (editText.equals("")) {
                    ((EditText) input_cell.findViewById(R.id.editText)).setText("");
                    ((TextView) input_cell.findViewById(R.id.errortextView)).setText("Field cannot be empty.");
                    input_cell.findViewById(R.id.errortextView).setVisibility(View.VISIBLE);

                    return false;
                } else if (((String) ((HashMap) ((HashMap) ((HashMap) contentListArray.get(select_row)).get(CHILD_KEY)).get("validation")).get("type")).equals("percent") &&
                        (editint < 0 || editint > 100)) {
                    ((EditText) input_cell.findViewById(R.id.editText)).setText("");
                    ((TextView) input_cell.findViewById(R.id.errortextView)).setText("1 - 100");
                    input_cell.findViewById(R.id.errortextView).setVisibility(View.VISIBLE);

                    return false;
                }
            } else {
                ((EditText) input_cell.findViewById(R.id.editText)).setText("");
                ((TextView) input_cell.findViewById(R.id.errortextView)).setText(validate_content);
                input_cell.findViewById(R.id.errortextView).setVisibility(View.VISIBLE);

                return false;
            }
        }
        return true;
    }

    public void onClickNextButton(View id) {
        //NavigationUtil.navigateToQuestion(this,"-1",NavigationUtil.getViewControlsQuestionIdentiesDict(),findViewById(R.id.container_progress_view), findViewById(R.id.buttonNext));

        int select_item_index = -1;

        for (int i = 0; i < contentListArray.size(); i++) {
            if ((int)((HashMap) contentListArray.get(i)).get(CHECK_KEY) == 1) {
                select_item_index = i;
                break;
            }
        }

        if (select_item_index > -1) {
            findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
            findViewById(R.id.buttonNext).setVisibility(View.GONE);

            String input_string = "";

            String check_type = (String) ((HashMap) ((HashMap) contentListArray.get(select_item_index)).get(CHILD_KEY)).get(TYPE_KEY);

            if (check_type.equals(INPUT_CHILD_TYPE)) {

                int first_position = listView.getFirstVisiblePosition();
                int last_position = listView.getLastVisiblePosition();

                for (int i = 0; i <= (last_position - first_position); i++) {
                    if ((select_item_index + 1 - first_position) == i) {
                        input_string = ((EditText) listView.getChildAt(i).findViewById(R.id.editText)).getText().toString();
                    }
                }
            }

            for (int i = 0; i < contentListArray.size(); i++) {
                if (((HashMap) contentListArray.get(i)).get(TYPE_KEY).equals(CHECK_TYPE_CELL)) {
                    if (((HashMap) ((HashMap) contentListArray.get(i)).get(CHILD_KEY)).get(TYPE_KEY).equals(INPUT_CHILD_TYPE)) {
                        check_type = INPUT_CHILD_TYPE;
                    }
                }
            }

            _nextId = ((Double) ((HashMap) contentListArray.get(select_item_index)).get(NEXT_ID_KEY)).intValue();

            switch ((Integer) ((HashMap) contentListArray.get(select_item_index)).get(VALUE_KEY)) {
                case 0:
                    answer_value = "no";
                    break;
                case 1:
                    answer_value = "yes";
                    break;
                case 2:
                    answer_value = "idn";
                    break;

                default:
                    break;
            }

            check_yes_no_idn = answer_value;

            if (check_type.equals(INPUT_FSA_CHILD_TYPE)) {
                //{"answer":{"value":"no","fsa-username":"abuse@lifecoachhub.com","fsa-password":"11111111"}}

                final View fsa_cell = listView.getChildAt(1);

                ((TextView) fsa_cell.findViewById(R.id.errortextView)).setText("");
                fsa_cell.findViewById(R.id.errortextView).setVisibility(View.INVISIBLE);

                ConnectUtilListener listener = new ConnectUtilListener() {
                    @Override
                    public void successResponse(String str) {

                        findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
                        findViewById(R.id.buttonNext).setVisibility(View.GONE);

                        HashMap response = ParseConnectionMessages.parseQuestionFirst(str);

                        if ((Boolean) responseDict.get("success")) {

                            View fsa_cell = listView.getChildAt(1);

                            HashMap answer_dict = new HashMap();
                            answer_dict.put("value", answer_value);
                            answer_dict.put("fsa-username", ((EditText) fsa_cell.findViewById(R.id.emaileditText)).getText());
                            answer_dict.put("fsa-password", ((EditText) fsa_cell.findViewById(R.id.passwordeditText)).getText());
                            answer_dict.put("result", responseDict.get("result"));

                            HashMap t_map = new HashMap();

                            t_map.put("response_dict", responseDict);
                            t_map.put("title", ((TextView) findViewById(R.id.questionLabel)).getText());
                            t_map.put("answer", "yes");

                            HashMap t_dictionary = new HashMap();
                            t_dictionary.put("answer", t_map);

                            FileUtil.addAnswerById(QuestionType3Activity.this, _Id.toString(), _nextId.toString(), t_dictionary);

                            if (_isEdit) {
                                onBackPressed();
                            } else {
                                NavigationUtil.navigateToQuestion(
                                        QuestionType3Activity.this,
                                        _nextId.toString(),
                                        NavigationUtil.getViewControlsQuestionIdentiesDict(),
                                        findViewById(R.id.container_progress_view),
                                        findViewById(R.id.buttonNext));
                            }

                        } else {

                            View fsa_cell = listView.getChildAt(1);

                            ((TextView) fsa_cell.findViewById(R.id.errortextView)).setText(responseDict.get("result").toString());
                            fsa_cell.findViewById(R.id.errortextView).setVisibility(View.VISIBLE);

                            //[fsa_cell.errorPasswordLabel sizeToFit];
                        }
                    }

                    @Override
                    public void failResponse(VolleyError error) {

                    }
                };

                isSuccess = false;

                ConnectUtil.sendLoanFsaByUsername(
                        QuestionType3Activity.this,
                        listener,
                        ((EditText) fsa_cell.findViewById(R.id.emaileditText)).getText().toString(),
                        ((EditText) fsa_cell.findViewById(R.id.passwordeditText)).getText().toString());

            } else if (check_type.equals(INPUT_CHILD_TYPE)) {
                if (((HashMap) ((HashMap) contentListArray.get(select_item_index)).get(CHILD_KEY)).get(TYPE_KEY).equals(INPUT_CHILD_TYPE)) {
                    isSuccess = isValidating(check_type, select_item_index);
                } else {
                    isSuccess = true;
                }

                // {"answer":{"":"0","value":"no","count":"12"}}
                HashMap answer_dict = new HashMap();
                answer_dict.put("value", answer_value);
                answer_dict.put("default_content", 0);
                answer_dict.put("content", input_string == null ? "0" : input_string);

                response_dict = new HashMap();
                response_dict.put("answer", answer_dict);

            } else {
                //{"answer":"yes|no|idn"}
                isSuccess = true;

                response_dict = new HashMap();
                response_dict.put("answer", answer_value);
            }

            if (_Id == 105 || _Id == 104) {
                FileUtil.writeAmountJsonToFileCalcCalendarJson(this, input_string);
            } else if (_Id == 107) {
                FileUtil.writePaymentJsonToFileCalcCalendarJson(this, input_string);
            } else if (_Id == 112 || _Id == 106) {
                HashMap answer_dict = new HashMap();
                answer_dict.put("bool", check_yes_no_idn.toLowerCase());
                answer_dict.put("input", input_string);

                HashMap id_dict = new HashMap();
                id_dict.put(_Id, answer_dict);

                FileUtil.writeInterestRateJsonToFileCalcCalendarJson(this, id_dict);

            } else if (_Id == 108) {
                HashMap answer_dict = new HashMap();
                answer_dict.put("value", check_yes_no_idn.toLowerCase());
                answer_dict.put("default_content", _calc_months);
                answer_dict.put("content", input_string == null ? "0" : input_string);

                response_dict = new HashMap();
                response_dict.put("answer", answer_dict);
            }

            if (isSuccess) {
                //self.responseDict, @"response_dict",

                HashMap t_dictionary = new HashMap();

                t_dictionary.put("response_dict", responseDict);
                t_dictionary.put("title", ((TextView) findViewById(R.id.questionLabel)).getText());
                t_dictionary.put("answer", response_dict.get("answer"));

                HashMap answ_dictionary = new HashMap();

                answ_dictionary.put("answer", t_dictionary);

                FileUtil.addAnswerById(this, _Id.toString(), _nextId.toString(), answ_dictionary);

                if (_isEdit) {
                    onBackPressed();
                } else {
                    NavigationUtil.navigateToQuestion(
                            this,
                            _nextId.toString(),
                            NavigationUtil.getViewControlsQuestionIdentiesDict(),
                            findViewById(R.id.container_progress_view),
                            findViewById(R.id.buttonNext));
                }
            } else {
                findViewById(R.id.container_progress_view).setVisibility(View.INVISIBLE);
                findViewById(R.id.buttonNext).setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}