package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.vulcan.gotzoom.adapters.QuestionListViewAdapter;
import com.vulcan.gotzoom.adapters.QuestionType3ListViewAdapter;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.util.NavigationUtil;

import java.util.ArrayList;
import java.util.HashMap;

public class QuestionListActivity extends AppCompatActivity {

    private ArrayList question_list_Array;

    private ListView listView;

    private QuestionListViewAdapter mListAdapterS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_list);


    }

    @Override
    protected void onResume() {
        super.onResume();

        //        self.page_loaderActivityIndicatorView.hidden = YES;
//        self.nextButton.hidden = NO;

        question_list_Array = FileUtil.getQuestionListArray(this);

        // _container_buttonView.hidden = !FileUtil.isQuestionListFin(this);

        View.OnClickListener mLibdocListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View v = (View) view.getParent().getParent();
                Integer position = view.getId();
                String select_id = ((HashMap) question_list_Array.get(position)).get("id").toString();

                NavigationUtil.navigateToQuestion(
                        QuestionListActivity.this,
                        select_id,
                        NavigationUtil.getViewControlsQuestionIdentiesDict(),
                        findViewById(R.id.container_progress_view),
                        findViewById(R.id.buttonNext));
            }
        };

        mListAdapterS = new QuestionListViewAdapter(
                this,
                question_list_Array,
                new int[]{R.layout.item_question_list},
                new int[]{R.id.questionTitle, R.id.questionContent},
                new int[]{},
                new View.OnClickListener[]{mLibdocListner});

        listView = (ListView) this.findViewById(R.id.listView);
        listView.setAdapter(mListAdapterS);
        listView.setLongClickable(false);
        listView.setClickable(true);

//        self.page_loaderActivityIndicatorView.hidden = YES;
//        self.nextButton.hidden = NO;

         findViewById(R.id.buttonLayout).setVisibility(FileUtil.isQuestionListFin(this)?View.VISIBLE:View.GONE);
    }

    public void onClickNextButton(View id){
        //NavigationUtil.navigateToQuestion(this,"-1",NavigationUtil.getViewControlsQuestionIdentiesDict(),findViewById(R.id.container_progress_view), findViewById(R.id.buttonNext));

        String token = FileUtil.readTokenFromFile(this);

        Intent intent = new Intent(this, QuestionResultActivity.class);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}
