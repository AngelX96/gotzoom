package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.vulcan.gotzoom.adapters.QuestionType2ListViewAdapter;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.util.NavigationUtil;
import com.vulcan.gotzoom.util.PayUtil;

import java.util.HashMap;

public class QuestionType2Activity extends AppCompatActivity {

    private String[] statelistArray;
    private String[] statelist_abbr_Array;
    private String _state;
    private String _state_full;

    private Intent intent;
    private Boolean _isEdit;
    private HashMap _answerDict;
    private HashMap responseDict;
    private Integer _Id;
    private Integer _nextId;

    private ListView listView;
    private QuestionType2ListViewAdapter mListAdapterS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_type2);

        PayUtil.payAlert(this);

        ////////////////////////////////////////////////////////////////////////////////////////////

        _state = "";
        _state_full = "";

        intent = getIntent();

        _isEdit = intent.getBooleanExtra("isEdit", false);

        if (_isEdit) {
            ((Button) findViewById(R.id.buttonNext)).setText("Save");
        }

        statelistArray = new String[]{
                "Alabama",
                "Alaska",
                "Arizona",
                "Arkansas",
                "California",
                "Colorado",
                "Connecticut",
                "Delaware",
                "Florida",
                "Georgia",
                "Hawaii",
                "Idaho",
                "Illinois Indiana",
                "Iowa",
                "Kansas",
                "Kentucky",
                "Louisiana",
                "Maine",
                "Maryland",
                "Massachusetts",
                "Michigan",
                "Minnesota",
                "Mississippi",
                "Missouri",
                "Montana Nebraska",
                "Nevada",
                "New Hampshire",
                "New Jersey",
                "New Mexico",
                "New York",
                "North Carolina",
                "North Dakota",
                "Ohio",
                "Oklahoma",
                "Oregon",
                "Pennsylvania Rhode Island",
                "South Carolina",
                "South Dakota",
                "Tennessee",
                "Texas",
                "Utah",
                "Vermont",
                "Virginia",
                "Washington",
                "West Virginia",
                "Wisconsin",
                "Wyoming"};

        statelist_abbr_Array = new String[]{
                "AL",
                "AK",
                "AZ",
                "AR",
                "CA",
                "CO",
                "CT",
                "DE",
                "FL",
                "GA",
                "HI",
                "ID",
                "IL",
                "IN",
                "IA",
                "KS",
                "KY",
                "LA",
                "ME",
                "MD",
                "MA",
                "MI",
                "MN",
                "MS",
                "MO",
                "MT",
                "NE",
                "NV",
                "NH",
                "NJ",
                "NM",
                "NY",
                "NC",
                "ND",
                "OH",
                "OK",
                "OR",
                "PA",
                "RI",
                "SC",
                "SD",
                "TN",
                "TX",
                "UT",
                "VT",
                "VA",
                "WA",
                "WV",
                "WI",
                "WY"};


        _answerDict = (HashMap) intent.getSerializableExtra("answerDict");
        responseDict = (HashMap) intent.getSerializableExtra("responseDict");

        _nextId = ((Double) ((HashMap) responseDict.get("question")).get("next")).intValue();
        _Id = Integer.parseInt(((HashMap) responseDict.get("question")).get("id").toString());

        _state = "";
        if (_answerDict != null) {
            _state = (String) _answerDict.get("answer");
            for (int i = 0 ; i < statelist_abbr_Array.length ; i++) {
                if (_state.equals(statelist_abbr_Array)){
                    _state_full = statelistArray[i];
                    break;
                }
            }
            //_state_full = (String) _answerDict.get("title");
        }

        View.OnClickListener mLibdocListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    Intent intent = new Intent(LibdocListActivity.this, FileListActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                    String libdoc_id = ((SectionListAdapter) ((ListView) view.getParent().getParent()).getAdapter()).getIdByPosition(((View) view.getParent()).getId());
//                    intent.putExtra(DBAssist.DB_COLUMN_NAME_LIBDOC_ID, libdoc_id);
//                    startActivity(intent);

                //String libdoc_id = ((QuestionType1ListViewAdapter) ((ListView) view.getParent().getParent()).getAdapter()).getIdByPosition(((View) view.getParent()).getId());
                Integer position = ((View) view.getParent()).getId();

                _state = statelist_abbr_Array[position];
                _state_full = statelistArray[position];

                mListAdapterS.updateState(_state);

                int first_position = listView.getFirstVisiblePosition();
                int last_position = listView.getLastVisiblePosition();

                for (int i = 0; i<(last_position - first_position); i++){
                    if ((position - first_position) != i){
                        ((RadioButton) listView.getChildAt(i).findViewById(R.id.radioButton)).setChecked(false);
                    }
                }
               // checkArray.set(i, !checkArray.get(i));
            }
        };
        mListAdapterS = new QuestionType2ListViewAdapter(
                this,
                statelistArray,
                statelist_abbr_Array,
                _state,
                new int[]{R.layout.item_check_single},
                new int[]{R.id.radioButton},
                new int[]{R.id.radioButton},
                new View.OnClickListener[]{mLibdocListner});

        listView = (ListView) this.findViewById(R.id.listView);
        listView.setAdapter(mListAdapterS);
        listView.setLongClickable(false);
        listView.setClickable(true);
    }


    public void onClickNextButton(View view){
     //   NavigationUtil.navigateToQuestion(this,"-1",NavigationUtil.getViewControlsQuestionIdentiesDict(),findViewById(R.id.container_progress_view), findViewById(R.id.buttonNext));

        if (!_state.equals("")) {
            findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
            findViewById(R.id.buttonNext).setVisibility(View.GONE);

            HashMap t_map = new HashMap();

            t_map.put("answer", _state);
            t_map.put("response_dict", responseDict);
            t_map.put("title", "What is your state of Residence?");
            t_map.put("answer_name", _state_full);

            HashMap t_dictionary = new HashMap();
            t_dictionary.put("answer", t_map);

            FileUtil.addAnswerById(this, _Id.toString(), _nextId.toString(), t_dictionary);

            if(_isEdit){
                onBackPressed();
            } else{
                NavigationUtil.navigateToQuestion(
                        this,
                        _nextId.toString(),
                        NavigationUtil.getViewControlsQuestionIdentiesDict(),
                        findViewById(R.id.container_progress_view),
                        findViewById(R.id.buttonNext));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClickWebGroupButton(View id){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}
