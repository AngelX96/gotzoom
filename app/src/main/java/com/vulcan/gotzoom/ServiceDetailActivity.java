package com.vulcan.gotzoom;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.vulcan.gotzoom.adapters.ServiceDetailListViewAdapter;
import com.vulcan.gotzoom.adapters.ServicesListViewAdapter;
import com.vulcan.gotzoom.json_parser.ParseConnectionMessages;
import com.vulcan.gotzoom.util.ConnectUtil;
import com.vulcan.gotzoom.util.ConnectUtilListener;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.volley.VolleyError;

import java.util.HashMap;

public class ServiceDetailActivity extends AppCompatActivity {
    private Intent intent;

    private ServiceDetailListViewAdapter mListAdapterS;

    private ListView listView;

    private HashMap content_map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_service_detail);

        findViewById(R.id.listView).setVisibility(View.VISIBLE);
        findViewById(R.id.container_progress_view).setVisibility(View.INVISIBLE);

        intent = getIntent();

        content_map = (HashMap) intent.getSerializableExtra("service_Dictionary");

        View.OnClickListener buttonPayListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View parent_view = (View) view.getParent();

                String slug_service = content_map.get("slug").toString();
                String card_number  = ((EditText) parent_view.findViewById(R.id.card_number)).getText().toString();
                String card_expiry  = String.format("%s/%s", ((EditText) parent_view.findViewById(R.id.mounth)).getText().toString(), ((EditText) parent_view.findViewById(R.id.year)).getText().toString());
                String card_cvc     = ((EditText) parent_view.findViewById(R.id.cv_code)).getText().toString();

                ServiceDetailActivity.this.purchaseService(slug_service, card_number, card_expiry, card_cvc);
            }
        };

        mListAdapterS = new ServiceDetailListViewAdapter(
                ServiceDetailActivity.this,
                content_map,
                new int[]{
                        R.layout.item_service_card,
                        R.layout.item_service_detail_name,
                        R.layout.item_service_detail_price,
                        R.layout.item_service_item
                },
                new int[]{
                        R.id.button,
                        R.id.name,
                        R.id.price,
                        R.id.price_month,
                        R.id.text
                },
                new int[]{},
                new View.OnClickListener[]{buttonPayListner},
                listView);

        listView = (ListView) ServiceDetailActivity.this.findViewById(R.id.listView);
        listView.setAdapter(mListAdapterS);
        listView.setLongClickable(false);
        listView.setClickable(false);
    }

    private void  purchaseService(String service_slug, String card_number, String card_expiry, String card_cvc){
        ConnectUtilListener listener = new ConnectUtilListener() {
            @Override
            public void successResponse(String str) {
                HashMap responseDict = ParseConnectionMessages.parseQuestionFirst(str);
                findViewById(R.id.container_progress_view).setVisibility(View.GONE);

                if ((Boolean) responseDict.get("success")) {
                    Intent intent = new Intent(getApplicationContext(), ServiceThankYouActivity.class);
                    intent.putExtra("thank_you_String", (String) content_map.get("description_after_payment"));
                    startActivity(intent);
                } else {
                    findViewById(R.id.listView).setVisibility(View.VISIBLE);
                    findViewById(R.id.container_progress_view).setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void failResponse(VolleyError error) {
                findViewById(R.id.listView).setVisibility(View.VISIBLE);
                findViewById(R.id.container_progress_view).setVisibility(View.INVISIBLE);
            }
        };

        findViewById(R.id.listView).setVisibility(View.INVISIBLE);
        findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);

        ConnectUtil.purchaeServiceSlug(this, listener, FileUtil.readTokenFromFile(this), service_slug, card_number, card_expiry, card_cvc);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
