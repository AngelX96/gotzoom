package com.vulcan.gotzoom;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.vulcan.gotzoom.adapters.QuestionType1ListViewAdapter;
import com.vulcan.gotzoom.util.FileUtil;
import com.vulcan.gotzoom.util.NavigationUtil;
import com.vulcan.gotzoom.util.PayUtil;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class QuestionType1Activity extends AppCompatActivity {
    private Intent intent;
    private ArrayList answers_request_arr;
    private ArrayList answers_st_request_arr;
    private HashMap responseDict;
    private Integer _Id;
    private Integer _nextId;
    private HashMap answerDict;
    private HashMap _answerDict;
    private ArrayList answerArray;
    private ArrayList<Boolean> checkArray;
    private ListView listView;
    private QuestionType1ListViewAdapter mListAdapterS;
    private Boolean _isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_type1);

        PayUtil.payAlert(this);

        ////////////////////////////////////////////////////////////////////////////////////////////
        intent = getIntent();

        _isEdit = intent.getBooleanExtra("isEdit", false);
        if (_isEdit) {

            ((Button) findViewById(R.id.buttonNext)).setText("Save");
        }

        answers_request_arr = new ArrayList();
        answers_st_request_arr = new ArrayList();
        checkArray = new ArrayList();
        responseDict = (HashMap) intent.getSerializableExtra("responseDict");
        _answerDict = (HashMap) intent.getSerializableExtra("answerDict");

        if ((Boolean) responseDict.get("success")) {

            _Id = ((Double) ((HashMap) responseDict.get("question")).get("id")).intValue();
            _nextId = ((Double) ((HashMap) responseDict.get("question")).get("next")).intValue();

            ((TextView) findViewById(R.id.questionLabel)).setText((String) ((HashMap) responseDict.get("question")).get("q"));

            FileUtil.getAnswerById(this, _Id.toString());

            answerDict = (HashMap) ((HashMap) responseDict.get("question")).get("answer");

            answerArray = new ArrayList();

            HashMap answer = (HashMap) ((HashMap) responseDict.get("question")).get("answer");

            Iterator iterator = answer.entrySet().iterator();

//            while (iterator.hasNext()){
//                HashMap o =  iterator.next();
//                answerArray.add(o);
//            }

            Set answer_keys = answer.keySet();
            HashMap[] obj_arr = new HashMap[answer.size()];
            for (int i = 0; i < answer.size(); i++) {
                HashMap hm = new HashMap();
                hm.put(answer_keys.toArray()[i], answer.get(answer_keys.toArray()[i]));
                obj_arr[i] = hm;
            }

            // HashMap[] obj_arr = (HashMap[]) answerArray.toArray();

            Arrays.sort(obj_arr, new Comparator<HashMap>() {
                @Override
                public int compare(HashMap player1, HashMap player2) {
                    int int_p1 = Integer.parseInt((String) player1.keySet().toArray()[0]);
                    int int_p2 = Integer.parseInt((String) player2.keySet().toArray()[0]);

                    if (int_p1 > int_p2) return 1;
                    else if (int_p1 < int_p2) return -1;
                    else return 0;
                }
            });

            answerArray = new ArrayList();

            for (int i = 0; i < obj_arr.length; i++) {
                answerArray.add(obj_arr[i]);
            }

//            [self.answersTableView reloadData];
        }

        checkArray = new ArrayList<Boolean>(answerArray.size());
        for (int i = 0; i < answerArray.size(); i++) {
            checkArray.add(false);
        }

        if (_answerDict != null) {
            ((TextView) findViewById(R.id.questionLabel)).setText((String) _answerDict.get("title"));

            answers_request_arr = new ArrayList((ArrayList) _answerDict.get("answer"));

            for (int i = 0; i < answers_request_arr.size(); i++) {
                Object s = ((HashMap) answers_request_arr.get(i)).keySet().toArray()[0];
                checkArray.add((Integer.parseInt((String) s) - 1), true);
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        if (getIntent().getExtras() != null) {


            View.OnClickListener mLibdocListner = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(LibdocListActivity.this, FileListActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                    String libdoc_id = ((SectionListAdapter) ((ListView) view.getParent().getParent()).getAdapter()).getIdByPosition(((View) view.getParent()).getId());
//                    intent.putExtra(DBAssist.DB_COLUMN_NAME_LIBDOC_ID, libdoc_id);
//                    startActivity(intent);

                    //String libdoc_id = ((QuestionType1ListViewAdapter) ((ListView) view.getParent().getParent()).getAdapter()).getIdByPosition(((View) view.getParent()).getId());
                    Integer i = ((View) view.getParent()).getId();
                    checkArray.set(i, !checkArray.get(i));
                }
            };
            mListAdapterS = new QuestionType1ListViewAdapter(
                    this,
                    answerArray,
                    checkArray,
                    new int[]{R.layout.item_check_multy},
                    new int[]{R.id.checkBox},
                    new int[]{R.id.checkBox},
                    new View.OnClickListener[]{mLibdocListner});

            listView = (ListView) this.findViewById(R.id.listView);
            listView.setAdapter(mListAdapterS);
            listView.setLongClickable(false);
            listView.setClickable(true);
        }
    }

    public void onClickNextButton(View id) {
        //NavigationUtil.navigateToQuestion(this,"-1",NavigationUtil.getViewControlsQuestionIdentiesDict(),findViewById(R.id.container_progress_view), findViewById(R.id.buttonNext));

        answers_request_arr = new ArrayList();
        answers_st_request_arr = new ArrayList();

        for (int i = 0; i < checkArray.size(); i++) {
            if (checkArray.get(i)) {
                String st = ((HashMap) answerArray.get(i)).get(Integer.toString(i + 1)).toString();
                answers_st_request_arr.add(st);
                Object o = answerArray.get(i);
                answers_request_arr.add(answerArray.get(i));
            }
        }

        if (answers_request_arr.size() > 0) {
            findViewById(R.id.container_progress_view).setVisibility(View.VISIBLE);
            findViewById(R.id.buttonNext).setVisibility(View.GONE);

            HashMap t_map = new HashMap();

            t_map.put("answer", answers_request_arr);
            t_map.put("response_dict", responseDict);
            t_map.put("title", ((TextView) findViewById(R.id.questionLabel)).getText());
            t_map.put("answer_names", answers_st_request_arr);

            HashMap t_dictionary = new HashMap();
            t_dictionary.put("answer", t_map);

            FileUtil.addAnswerById(this, _Id.toString(), _nextId.toString(), t_dictionary);

            if (_isEdit) {
                onBackPressed();
            } else {
                NavigationUtil.navigateToQuestion(
                        this,
                        _nextId.toString(),
                        NavigationUtil.getViewControlsQuestionIdentiesDict(),
                        findViewById(R.id.container_progress_view),
                        findViewById(R.id.buttonNext));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClickWebGroupButton(View id) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://webgroupclub.com"));
        startActivity(browserIntent);
    }
}
